set -e -x 

## Variables

## Setup
yarn install

## Build
yarn run build

## Deploy
aws s3 sync ./build s3://flowmate-frontend
aws cloudfront create-invalidation --distribution-id EVZO300E4HD5N --paths '/*'


