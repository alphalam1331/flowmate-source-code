import "bootstrap/dist/css/bootstrap-grid.min.css";
import "./App.css";
import { Route, Routes } from "react-router";
import Trail from "./Trail";
import MainContainer from "./MainContainer";


function App() {
  
  return (
    <>
      <Routes>
        <Route path="/" element={<Trail />} />
        <Route path="/main/*" element={<MainContainer/>}/>
      </Routes>
    </>
  );
}

export default App;
