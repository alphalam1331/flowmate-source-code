import { APIResultType, AuthUser, UserInfo } from './state'

export function logoutAction() {
  return { type: '@@Auth/logout' as const }
}

export function setProfileAction(userInfo:UserInfo) {
  return { type: '@@Auth/setProfile' as const, userInfo}
}

export function loginAction(user:AuthUser) {
  return { type: '@@Auth/login' as const, user }
}

export function setLoginResultAction(result: APIResultType) {
  return { type: '@@Auth/setLoginResult' as const, result }
}
export function setRegisterResultAction(result: APIResultType) {
  return { type: '@@Auth/setRegisterResult' as const, result }
}

export type AuthAction =
  | ReturnType<typeof setLoginResultAction>
  | ReturnType<typeof setRegisterResultAction>
  | ReturnType<typeof logoutAction>
  | ReturnType<typeof loginAction>
  | ReturnType<typeof setProfileAction>