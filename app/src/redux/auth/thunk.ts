import { getFetchUserInfo, post, postWithToken } from "../../api"
import { AppDispatch } from "../action"
import { RootState } from "../state"
import { setProfileAction, setLoginResultAction, setRegisterResultAction } from "./action"


export function loginWithPasswordThunk(user: {
    username: string
    password: string
  }) {
    return async (dispatch: AppDispatch, getState: () => RootState) => {
      let json = await post('/user/login', user)
      // console.log("loginresult:", {json})
      if (json.message) {
        dispatch(setLoginResultAction({ type: 'fail', message: json.message }))
      } else {
        localStorage.setItem('token', json.token)
        dispatch(
          setLoginResultAction({
            type: 'success',
            token: json.token,
          }),
        )
      }
    }
  }


export function registerThunk(user:any /*{ username: string; password: string }*/) {
  
  return async (dispatch: AppDispatch, getState: () => RootState) => {
    // const navigate = useNavigate();
    let json = await post('/user/register', {
      username: user.username,
      title: user.title,
      name:user.name,
      superAdmin:false, 
      email: user.email,
      password: user.password,
      confirmPassword: user.confirmPassword, 
    })
    if (json.message) {
      dispatch(setRegisterResultAction({ type: 'fail', message: json.message }))
    } else {
      localStorage.setItem('token', json.token)
      dispatch(
        setRegisterResultAction({
          type: 'success',
          token: json.token,
        }),
      )
    
      // navigate("/builder");

    }
  }
}

export function setLoadProfileThunk() {
  return async (dispatch: AppDispatch, getState: () => RootState) => {
    let json = await getFetchUserInfo('/user')
    // console.log(json[0])
    // if (json.error) {
      // dispatch(setLoadProfileAction({ type: 'fail', message: json.error }))
    // } else {
      dispatch(
        setProfileAction(json[0])
      )
      // console.log("dispatch"+json[0])
    }
  }

  export function setEditProfileThunk(updateInfo:any) {
    return async (dispatch: AppDispatch, getState: () => RootState) => {
      let json = await postWithToken('/user/edit',{
        username: updateInfo.username,
        title: updateInfo.title,
        name:updateInfo.name,
        email: updateInfo.email,
        password: updateInfo.password,
      })
      console.log(json[0])
      // if (json.error) {
        // dispatch(setLoadProfileAction({ type: 'fail', message: json.error }))
      // } else {
        dispatch(
          setProfileAction(json)
        )
        console.log({json})
      }
    }
// }
// export function logoutThunk() {
//   return async (dispatch: AppDispatch, getState: () => RootState) => {
//     localStorage.removeItem('token')
//     dispatch(logoutAction())
//   }
// }