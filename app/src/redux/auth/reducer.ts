import { AuthAction } from "./action";
import { AuthState, AuthUser, JWTPayload, UserInfo } from "./state";
import jwtDecode from "jwt-decode";
import { useReducer } from "react";

function loadUserFromToken(token: string | null): AuthUser | null {
  if (!token) {
    return null;
  }
  try {
    let payload: JWTPayload = jwtDecode(token);
    // console.log("token payload:", payload);
    return { payload, token };
  } catch (error) {
    // console.error("Failed to code JWT:", error);
    return null;
  }
}

function initialState(): AuthState {
  let token = localStorage.getItem("token");
  const user = loadUserFromToken(token);

  return {
    user: user,
    registerResult: { type: "idle" },
    loginResult: { type: "idle" },
    userInfo:user?
    {
    id: user.payload.id,
    username: user.payload.username,
    superAdmin:user.payload.superAdmin,
    title: user.payload.title,
    name:user.payload.name, 
    email:user.payload.email,
    hashed_password: user.payload.hashed_password
    }:null
  }}

export function authReducer(
  state: AuthState = initialState(),
  action: AuthAction
): AuthState {
  switch (action.type) {
    case "@@Auth/login":
      return { ...state, user: action.user };

    case "@@Auth/logout":
      localStorage.removeItem('token')

      return {
        ...state,
        user: null,
        loginResult: {
          type: "idle",
        },
        registerResult:{
          type: "idle"
        }
      };

    case "@@Auth/setRegisterResult":
      return {
        ...state,
        registerResult: action.result,
        user: loadUserFromToken(
          action.result.type === "success" ? action.result.token : null
        ),
      };

    case "@@Auth/setLoginResult":
      console.log({
        ...state,
        loginResult: action.result,
        user: loadUserFromToken(
          action.result.type === "success" ? action.result.token : null
        ),
      });
      return {
        ...state,
        loginResult: action.result,
        user: loadUserFromToken(
          action.result.type === "success" ? action.result.token : null
        ),
      }
      case '@@Auth/setProfile':
        // console.log("Action User",action.userInfo);
        // const {userInfo}=action;
      return {...state, 
        userInfo:action.userInfo}
     
      
      // case '@@Auth/setEditProfileAction':
      //     console.log("Action User",action.userInfo);
      //     // const {userInfo}=action;
      //   return {...state, 
      //     userInfo:action.userInfo}
        
        
    default:
      return state;
  }
}
