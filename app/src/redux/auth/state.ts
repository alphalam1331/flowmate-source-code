export type AuthState = {
  user: AuthUser | null;
  registerResult: APIResultType;
  loginResult: APIResultType;
  userInfo:UserInfo | null;


};

export type AuthUser = {
  token: string;
  payload: JWTPayload 
};

export type APIResultType =
  | { type: "idle" }
  | { type: "success"; token: string }
  | { type: "fail"; message: string };

export type JWTPayload = {
  id: number; // user_id
  exp: number; // expire_at (ms)
  username: string;
  superAdmin: boolean;
  title: string;
	name: string;
  email: string;
  hashed_password:string;
};
export type UserInfo = {
  id: number;
  username: string;
  title: string;
  name: string;
  email: string;
  hashed_password: string;
  superAdmin: boolean;
};
// export type EditUserInfo = {
//   id: number;
//   username: string;
//   title: string;
//   name: string;
//   email: string;
//   password: string;
//   superAdmin: boolean;
// };