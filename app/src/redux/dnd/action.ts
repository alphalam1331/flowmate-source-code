import { ChangeEventHandler, MouseEventHandler } from "react";
import {
  Connection,
  Edge,
  Node,
  FlowElement,
  XYPosition,
  Elements,
} from "react-flow-renderer";
import { panel } from "../builderPanal/state";
import { WorkflowData, Option } from "./state";

export function updateAllElementAction(allElements: any) {
  return {
    type: "updateAllElements" as const,
    allElements: allElements,
  };
}

export function dropElement(
  event: React.DragEvent,
  position: XYPosition,
  id: string,
  // inputData: ChangeEventHandler,
  removeEvent: MouseEventHandler,
  panelData: panel,
  inputChange: ChangeEventHandler
) {
  let type: string = event.dataTransfer.getData("application/reactflow");
  return {
    type: `${type}` as const,
    position: position,
    id: id,
    // inputData: inputData,
    removeEvent: removeEvent,
    panelData: panelData,
    inputChange: inputChange,
  };
}

export function removeElement(cardName: string) {
  return {
    type: "remove" as const,
    cardName: cardName,
  };
}

export function addEdgeAction(
  params: Edge | Connection,
  id: string,
  edgeType: string
) {

  return {
    type: "addEdge" as const,
    edge: params,
    id: id,
    edgeType: edgeType,
  };
}

export function inputDataAction(
  inputTarget: string,
  input: string | boolean,
  targetElement: Node | Edge,
  removeEvent: MouseEventHandler,
  inputChange: ChangeEventHandler,
  panelData: panel
) {

  return {
    type: "inputData" as const,
    inputTarget: inputTarget,
    input: input,
    targetElement: targetElement as any,
    removeEvent: removeEvent,
    inputChange: inputChange,
    panelData: panelData,
  };
}
export function dropTemplateAction(
  template: any,
  removeEvent: MouseEventHandler
) {

  return {
    type: `init` as const,
    element: template,
    removeEvent: removeEvent,
  };
}

export function clearAllElements() {
  return {
    type: "clearAllElements" as const,
  };
}

export function savedWorkflow() {
  return {
    type: "SAVED_WORKFLOW" as const,
  };
}

export function updateNode(
  inputName: string,
  inputValue: string | boolean | Option[],
  nodeId: string
) {
  return {
    type: "updateNode" as const,
    inputName: inputName,
    inputValue: inputValue,
    nodeId: nodeId,
  };
}

export function updateEdge(
  inputName: string,
  inputValue: string | boolean,
  edgeId: string
) {
  return {
    type: "updateEdge" as const,
    inputName: inputName,
    inputValue: inputValue,
    edgeId: edgeId,
  };
}

//ToDo wait for the server connected with frontend and do the error handling
type FAILED_INTENT =
  | "SAVE_WORKFLOW_FAILED"
  | "PLACE_NEXT_STEP_FAILED"
  | "RESET_BOARD_FAILED"
  | "GET_BOARD_FAILED"
  | "GET_ALL_BOARD_FAILED";

export function failed(type: FAILED_INTENT, msg: string) {
  return {
    type,
    msg,
  };
}

export type elementAction =
  | ReturnType<typeof removeElement>
  | ReturnType<typeof dropElement>
  | ReturnType<typeof addEdgeAction>
  | ReturnType<typeof inputDataAction>
  | ReturnType<typeof dropTemplateAction>
  | ReturnType<typeof updateAllElementAction>
  | ReturnType<typeof failed>
  | ReturnType<typeof clearAllElements>
  | ReturnType<typeof updateNode>
  | ReturnType<typeof updateEdge>;
