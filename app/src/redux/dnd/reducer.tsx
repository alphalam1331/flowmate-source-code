import { ArrowHeadType, Edge, Elements, Node } from "react-flow-renderer";
import initialElement from "../../Component/initialElements";
import {
  elementAction,
  inputDataAction,
  updateEdge,
  updateNode,
} from "./action";
import { edgeLabel, edgeStyle, WorkflowData, Option } from "./state";
import styles from "./reducer.module.css";
// {  …., [target]: input, …}
// JSON, { key:value }
// { success: true}
// { [<variable>] : value }
// { [ variable name] : value }
// const template =`<> ..${item.data.inputData.title}… </>`
// const template =(title)=>`<> ..${title}… </>`
// template(item.data.inputData.title)

//const template =(item,action,input)=>(<> ..{item.title}…, onClick={action} </>)

// let item = _item as NodeLabel;
// If typeof item === “NodeLabe” { changeNodeLabel(…)}

function changeNodeLabel(
  action: ReturnType<typeof inputDataAction>,
  item: any
) {
  //

  return (
    <div className={styles.yellow_container}>
      <button
        className={styles.delete_button}
        name={item.id}
        onClick={action.removeEvent}
      >
        x
      </button>
      <table className={styles.table}>
        <tbody className={styles.yellow_tbody}>
          {/* <caption>Alien football stars</caption> */}
          <tr className={styles.yellow_head_tr}>
            <th scope="col" className={styles.th}>
              Task
            </th>
            <td className={styles.td}>Inspection</td>
          </tr>
          <tr className={styles.yellow_tr}>
            <th scope="row" className={styles.th}>
              Title:
            </th>
            <td className={styles.td}>
              <input
                value={
                  action.inputTarget === "title"
                    ? action.input
                    : item.data.inputData.title
                }
                onChange={action.inputChange}
                name="title"
                style={{ width: "100%", padding: "0px" }}
                type="text"
              />
            </td>
          </tr>
          <tr className={styles.yellow_tr}>
            <th scope="row" className={styles.th}>
              Description:
            </th>
            <td className={styles.td}></td>
          </tr>
          <tr className={styles.yellow_tr}>
            <th scope="row" className={styles.th}>
              Assign to:
            </th>
            <td className={styles.td}></td>
          </tr>
          <tr className={styles.yellow_tr}>
            <th scope="row" className={styles.th}>
              Status:
            </th>
            <td className={styles.td}></td>
          </tr>
          <tr className={styles.yellow_tr}>
            <th scope="row" className={styles.th}>
              Deadline:
            </th>
            <td className={styles.td}></td>
          </tr>
        </tbody>
      </table>
    </div>

    // <div className={styles.container}>
    //   <button name={item.id} onClick={action.removeEvent}>
    //     x
    //   </button>
    //   <table style={{ textAlign: "center" }}>
    //     <tbody>
    //       {/* <caption>Alien football stars</caption> */}
    //       <tr>
    //         <th scope="col">Task</th>
    //         <th scope="col">Inspection</th>
    //       </tr>
    //       <tr>
    //         <th scope="row">Title:</th>
    //         <td>
    //           {action.inputTarget === "title"
    //             ? action.input
    //             : item.data.inputData.title}
    //         </td>
    //       </tr>
    //       <tr>
    //         <th scope="row">Description:</th>
    //         <td>
    //           {action.inputTarget === "description"
    //             ? action.input
    //             : item.data.inputData.description}
    //         </td>
    //       </tr>
    //       <tr>
    //         <th scope="row">Assign to:</th>
    //         <td>
    //           {action.inputTarget === "assign_to"
    //             ? action.input
    //             : item.data.inputData.assign_to}
    //         </td>
    //       </tr>
    //       <tr>
    //         <th scope="row">Status:</th>
    //         <td>
    //           {action.inputTarget === "status"
    //             ? action.input
    //             : item.data.inputData.status}
    //         </td>
    //       </tr>
    //       <tr>
    //         <th scope="row">Deadline:</th>
    //         <td>
    //           {action.inputTarget === "deadline"
    //             ? action.input
    //             : item.data.inputData.deadline}
    //         </td>
    //       </tr>
    //     </tbody>
    //   </table>
    // </div>
  );
}

function changeEdgeLabel(
  action: ReturnType<typeof inputDataAction>,
  item: edgeLabel
) {
  return (
    <>
      <p>
        Review:
        {action.inputTarget === "review"
          ? action.input === "approve"
            ? "Approve"
            : action.input === "reject"
            ? "Reject"
            : action.input === "blank"
            ? "N/A"
            : "Error"
          : item.data.review === "approve"
          ? "Approve"
          : item.data.review === "reject"
          ? "Reject"
          : item.data.review === "blank"
          ? "N/A"
          : "----Error----"}
      </p>
      <p>
        Upload file:
        {action.inputTarget === "upload"
          ? action.input === "true"
            ? "Yes"
            : action.input === "false"
            ? "No"
            : "Error"
          : item.data.upload === "true"
          ? "Yes"
          : item.data.upload === "false"
          ? "No"
          : "Error"}
      </p>
    </>
  );
}

function changeEdgeStyle(
  action: ReturnType<typeof inputDataAction>,
  item: edgeStyle
) {
  return {
    ...item.style,
    stroke:
      action.inputTarget === "stroke_color" ? action.input : item.style.stroke,
  };
}

// const changeEvent = (action:any,item: any) => {
//   if ("position" in action.targetElement) {
//     return {
//       ...item,
//       data: {
//         ...item.data,
//         inputData: {
//           ...item.data.inputData,
//           [action.inputTarget]: action.input,
//         },
//         label: changeNodeLabel(action, item),
//       },
//     };
//   } else if ("source" in action.targetElement) {
//     return {
//       ...item,
//       data: {
//         ...item.data,
//         review:
//           action.inputTarget === "review"
//             ? action.input
//             : item.data.review,
//         upload:
//           action.inputTarget === "upload"
//             ? action.input
//             : item.data.upload,

//         label: changeEdgeLabel(action, item),
//       },
//       style: changeEdgeStyle(action, item),
//     };
//   }
//   console.log("return should not happen");
// };

function updateParentAndChild<
  T extends { data: { inputData: { dependencies: {}[]; nextStage: {}[] } } }
>(action: any, e: T): T {
  if (!("position" in e)) {
    // console.log("item is not a card element", e);
    return e;
  }

  // console.log(`this is a card element`, e);
  if (
    e.data.inputData.dependencies.some(
      (e: any) => e.connectedEdgeId === action.targetElement.id
    )
  ) {
    // console.log("before creating dependenciesList");
    const dependenciesList = [...e.data.inputData.dependencies].map(
      (item: any) => {
        // console.log(
        //   "in dependenciesList, start to check e.connectedEdgeId === action.targetElement.id"
        // );
        if (item.connectedEdgeId === action.targetElement.id) {
          // console.log(
          //   "item.connectedEdgeId === action.targetElement.id, return dependenciesList",
          //   {
          //     ...item,
          //     triggerEvent: {
          //       ...item.triggerEvent,
          //       [action.inputTarget]: action.input,
          //     },
          //   }
          // );
          return {
            ...item,
            triggerEvent: {
              ...item.triggerEvent,
              [action.inputTarget]: action.input,
            },
          };
        }
        // console.log(
        //   "although it has a connection id with targeted edge, but not related dependenciesList, return unchange",
        //   item
        // );
        return item;
      }
    );
    return {
      ...e,
      data: {
        ...e.data,
        inputData: {
          ...e.data.inputData,
          dependencies: dependenciesList,
        },
      },
    };
  } else if (
    e.data.inputData.nextStage.some(
      (e: any) => e.connectedEdgeId === action.targetElement.id
    )
  ) {
    // console.log("before creating nextStageList");
    const nextStageList = [...e.data.inputData.nextStage].map((e: any) => {
      // console.log(
      //   "in nextStageList, start to check e.connectedEdgeId === action.targetElement.id"
      // );
      if (e.connectedEdgeId === action.targetElement.id) {
        // console.log(
        //   "e.connectedEdgeId === action.targetElement.id, return nextStageList",
        //   {
        //     ...e,
        //     triggerEvent: {
        //       ...e.triggerEvent,
        //       [action.inputTarget]: action.input,
        //     },
        //   }
        // );
        return {
          ...e,
          triggerEvent: {
            ...e.triggerEvent,
            [action.inputTarget]: action.input,
          },
        };
      }
      // console.log(
      //   "although it has a connection id with targeted edge, but not related nextStageList, return unchange",
      //   e
      // );

      return e;
    });

    return {
      ...e,
      data: {
        ...e.data,
        inputData: {
          ...e.data.inputData,
          nextStage: nextStageList,
        },
      },
    };
  } else {
    return e;
  }
}

export const elementReducer = (
  state: Elements<WorkflowData> = initialElement,
  action: elementAction
): Elements<WorkflowData> => {
  let waitingList;
  switch (action.type) {
    case "custom1":
      waitingList = {
        id: action.id,
        type: action.type,
        position: action.position,
        data: {
          inputData: {
            title: "",
            description: "",
            deadline: "",
            status: "",
            assign_to: [],
            background_color: "#e4e81e",
            dependencies: [],
            nextStage: [],
            action: {
              email: {
                to: [],
                subject: "",
                content: "",
              },
            },
          },
        },
      };
      return [...state, waitingList];

    case "remove":
      let newState = [...state];
      console.log("Before remove: ", newState);
      if ("cardName" in action) {
        const targetElement = newState.find((e) => e.id === action.cardName);
        //----------confirm it is a edge to be removed-----------------
        if (targetElement && "source" in targetElement) {
          newState = newState.map((e: any) => {
            if (
              "position" in e &&
              e.data.inputData.dependencies.some(
                (e: any) => e.connectedEdgeId === action.cardName
              )
            ) {
              const newDependencies = [...e.data.inputData.dependencies].filter(
                (e: any) => e.connectedEdgeId !== action.cardName
              );

              return {
                ...e,
                data: {
                  ...e.data,
                  inputData: {
                    ...e.data.inputData,
                    dependencies: newDependencies,
                  },
                },
              };
            } else if (
              "position" in e &&
              e.data.inputData.nextStage.some(
                (e: any) => e.connectedEdgeId === action.cardName
              )
            ) {
              const newNextStage = [...e.data.inputData.nextStage].filter(
                (e: any) => e.connectedEdgeId !== action.cardName
              );
              return {
                ...e,
                data: {
                  ...e.data,
                  inputData: {
                    ...e.data.inputData,
                    nextStage: newNextStage,
                  },
                },
              };
            }
            return e;
          });
        }
        //----------confirm it is a node /card to be removed-----------------
        if (targetElement && "position" in targetElement) {
          //----------remove related edge-----------------
          newState = newState.filter((e: any) =>
            (e.source && e.source === action.cardName) ||
            (e.target && e.target === action.cardName)
              ? false
              : true
          );
          //----------remove related node-----------------
          newState = newState.map((e: any) => {
            if (
              "position" in e &&
              e.data.inputData.dependencies.some(
                (e: any) => e.sourceId === action.cardName
              )
            ) {
              // console.log(
              //   "-----------------before newDependencies-----------------",
              //   [...e.data.inputData.dependencies]
              // );
              const newDependencies = [...e.data.inputData.dependencies].filter(
                (e: any) => e.sourceId !== action.cardName
              );
              // console.log(
              //   "-----------------after newDependencies-----------------",
              //   newDependencies
              // );
              return {
                ...e,
                data: {
                  ...e.data,
                  inputData: {
                    ...e.data.inputData,
                    dependencies: newDependencies,
                  },
                },
              };
            } else if (
              "position" in e &&
              e.data.inputData.nextStage.some(
                (e: any) => e.targetId === action.cardName
              )
            ) {
              const newNextStage = [...e.data.inputData.nextStage].filter(
                (e: any) => e.targetId !== action.cardName
              );
              console.log("newNextStage",newNextStage)
              return {
                ...e,
                data: {
                  ...e.data,
                  inputData: {
                    ...e.data.inputData,
                    nextStage: newNextStage,
                  },
                },
              };
            }
            return e;
          });
        }
        if (targetElement) {
          let index = newState.indexOf(targetElement);
          newState.splice(index, 1);
        }
      }
      console.log("After remove: ", newState);
      return newState;

    case "addEdge":
      let addEdgeState = [...state];
      // console.log(
      //   "-----------------------before addEdge--------------------------",
      //   addEdgeState
      // );
      let newEdge;
      if ("edge" in action && action.edge.source && action.edge.target) {
        const randomColor = Math.floor(Math.random() * 16777215).toString(16);
        newEdge = {
          id: action.id,
          type: action.edgeType,
          source: action.edge.source,
          target: action.edge.target,
          sourceHandle: action.edge.sourceHandle,
          labelShowBg: true,
          animated: true,
          inputData: {},
          arrowHeadType: ArrowHeadType.ArrowClosed,
          style: { strokeWidth: 5 },
          data: {
            stroke_color: `#${randomColor}`,
            review: "approve",
            upload: "true",
            label: (
              <>
                <p>
                  Review:
                  {"N/A"}
                </p>
                <p>Upload file:{"No"}</p>
              </>
            ),
          },
        };

        addEdgeState = addEdgeState.concat(newEdge as any);

        addEdgeState = addEdgeState.map((e: any) => {
          if (e.id === action.edge.source) {
            return {
              ...e,
              data: {
                ...e.data,
                inputData: {
                  ...e.data.inputData,
                  nextStage: [
                    ...e.data.inputData.nextStage,
                    {
                      connectedEdgeId: action.id,
                      targetId: action.edge.target,
                      triggerEvent: {
                        review: "approve",
                        // upload: false,
                      },
                    },
                  ],
                },
              },
            };
          }
          if (e.id === action.edge.target) {
            return {
              ...e,
              data: {
                ...e.data,
                inputData: {
                  ...e.data.inputData,
                  dependencies: [
                    ...e.data.inputData.dependencies,
                    {
                      connectedEdgeId: action.id,
                      sourceId: action.edge.source,
                      triggerEvent: {
                        review: "approve",
                        // upload: false,
                      },
                    },
                  ],
                },
              },
            };
          }
          return e;
        });
      }
      // console.log(
      //   "-----------------------after addEdge--------------------------",
      //   addEdgeState
      // );
      // console.log("addEdge success");
      return addEdgeState;

    case "inputData":
      console.log("testtest")
      let inputDataList = [...state];

      // console.log(
      //   "------------------inputDataList before------------------------: ",
      //   inputDataList
      // );

      if ("targetElement" in action) {
        // console.log(
        //   "==============debug================",
        //   action.targetElement
        // );
        inputDataList = [...state].map((item: any) => {
          const changeEvent = (target: string) => {
            // console.log("111111111111111111");
            if ("position" in action.targetElement) {
              // console.log(`"position" in action.targetElement`);

              return {
                ...item,
                data: {
                  ...item.data,
                  inputData: {
                    ...item.data.inputData,
                    [target]: action.input,
                  },
                  label: changeNodeLabel(action, item),
                },
              };
            } else if ("source" in action.targetElement) {
              // console.log(`"source" in action.targetElement`);

              return {
                ...item,
                data: {
                  ...item.data,
                  review:
                    action.inputTarget === "review"
                      ? action.input
                      : item.data.review,
                  upload:
                    action.inputTarget === "upload"
                      ? action.input
                      : item.data.upload,

                  label: changeEdgeLabel(action, item),
                },
                style: changeEdgeStyle(action, item),
              };
            }
            // console.log("return should not happen");
          };

          if (item.id === action.targetElement.id) {
            return changeEvent(action.inputTarget);
          } else {
            // console.log("function error, return last default");
            return item;
          }
        });
      }

      //-------------change card's child and parent--------------------
      if ("targetElement" in action) {
        if (
          action.inputTarget === "review" ||
          action.inputTarget === "upload"
        ) {
          inputDataList = inputDataList.map((e: Node | Edge) => {
            return updateParentAndChild(action, e as any);
          });
        }
      }
  
      return inputDataList;

    case "init":
      waitingList = [...state];

      if ("element" in action) {
        // console.log("action.element in init", action.element);
        let incomingElement = action.element;
        incomingElement = incomingElement.map((e: any) => {
          if (e.position) {
            return {
              ...e,
              data: {
                ...e.data,
                label: (
                  <div>
                    <button name={e.id} onClick={action.removeEvent}>
                      x
                    </button>
                    <table>
                      <tbody>
                        <tr>
                          <th scope="col">Task</th>
                          <th scope="col">Inspection</th>
                        </tr>
                        <tr>
                          <th scope="row">Title:</th>
                          <td>{e.data.inputData.title}</td>
                        </tr>
                        <tr>
                          <th scope="row">Description:</th>
                          <td>{e.data.inputData.description}</td>
                        </tr>
                        <tr>
                          <th scope="row">Assign to:</th>
                          <td>{e.data.inputData.assign_to}</td>
                        </tr>
                        <tr>
                          <th scope="row">Status:</th>
                          <td>{e.data.inputData.status}</td>
                        </tr>
                        <tr>
                          <th scope="row">Deadline:</th>
                          <td>{e.data.inputData.deadline}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                ),
              },
            };
          } else if (e.source) {
            return {
              ...e,
              data: {
                ...e.data,
                label: (
                  <>
                    <p>
                      Review:
                      {e.data.review}
                    </p>
                    <p>Upload file:{e.data.upload}</p>
                  </>
                ),
              },
            };
          }
          return e;
        });
        // console.log("action.element", action.element);
        // console.log("waitingList", waitingList);

        waitingList = incomingElement;
      }
      // console.log("init success");
      return waitingList;

    case "updateAllElements":
      if ("allElements" in action) {
        waitingList = action.allElements;
      }
      return waitingList;

    case "clearAllElements":
      waitingList = [...state];
      waitingList.splice(0, [...state].length);
      // console.log("======clearAllElements======", waitingList);
      return waitingList;

    case "updateNode":
      // console.log("Before updateNode action: ", [...state]);
      waitingList = [...state];
      if ("nodeId" in action) {
        waitingList = updateNodeData(state, action);
      }
      // console.log("after updateNode action: ", waitingList);

      return waitingList;

    case "updateEdge":
      // console.log("Before updateEdge action: ", [...state]);
      waitingList = [...state];
      // console.log(`"edgeId" in action`, "edgeId" in action);

      if ("edgeId" in action) {
        // console.log(`"edgeId" in action`);
        waitingList = updateEdgeData(state, action);
        waitingList = waitingList.map((e: any) => updateNodeFamily(action, e));
      }
      // console.log("after updateEdge action: ", waitingList);

      return waitingList;
    default:
      // console.log('dnd error state', state)
      // console.log(' dnd error action',action)
      return state;
  }
};

function updateNodeData(
  state: Elements<WorkflowData>,
  action: ReturnType<typeof updateNode>
) {
  let stateList = state.map((e: Node<WorkflowData> | Edge<WorkflowData>) => {
    if (!("position" in e && e.id === action.nodeId && e.data)) {
      return e;
    } else {
      let assign_to_list;
      let emailList;
      if (
        action.inputName === "assign_to" &&
        Array.isArray(action.inputValue)
      ) {
        assign_to_list = action.inputValue.map((e: Option) => {
          return e.value;
        });
      }
      if (action.inputName === "to" && Array.isArray(action.inputValue)) {
        emailList = action.inputValue.map((e: Option) => {
          return e.value;
        });
        return {
          ...e,
          data: {
            ...e.data,
            inputData: {
              ...e.data.inputData,
              action: {
                ...e.data.inputData.action,
                email: {
                  ...e.data.inputData.action.email,
                  to:
                    action.inputName === "to"
                      ? emailList
                      : e.data.inputData.action.email.to,
                },
              },
            },
          },
        };
      }
      if (action.inputName === "content" || action.inputName === "subject") {
        return {
          ...e,
          data: {
            ...e.data,
            inputData: {
              ...e.data.inputData,
              action: {
                ...e.data.inputData.action,
                email: {
                  ...e.data.inputData.action.email,
                  [action.inputName]: action.inputValue,
                },
              },
            },
          },
        };
      }
      // console.log("yes");
      return {
        ...e,
        data: {
          ...e.data,
          inputData: {
            ...e.data.inputData,
            [action.inputName]: action.inputValue,
            assign_to:
              action.inputName === "assign_to"
                ? assign_to_list
                : e.data.inputData.assign_to,
          },
        },
      };
    }
  });
  return stateList;
}

function updateEdgeData(
  state: Elements<WorkflowData>,
  action: ReturnType<typeof updateEdge>
) {
  let stateList = state.map((e: Node<WorkflowData> | Edge<WorkflowData>) => {
    if (!("source" in e && e.id === action.edgeId && e.data)) {
      return e;
    } else {
      return {
        ...e,
        data: {
          ...e.data,
          [action.inputName]: action.inputValue,
        },
      };
    }
  });

  return stateList;
}

function updateNodeFamily<
  T extends { data: { inputData: { dependencies: {}[]; nextStage: {}[] } } }
>(action: ReturnType<typeof updateEdge>, e: T): T {
  if (!("position" in e) || action.inputName === "stroke_color") {
    return e;
  }

  // console.log(`this is a card element`, e);
  if (
    e.data.inputData.dependencies.some(
      (e: any) => e.connectedEdgeId === action.edgeId
    )
  ) {
    // console.log("before creating dependenciesList");
    const dependenciesList = [...e.data.inputData.dependencies].map(
      (item: any) => {
        if (item.connectedEdgeId === action.edgeId) {
          return {
            ...item,
            triggerEvent: {
              // ...item.triggerEvent,
              [action.inputName]: action.inputValue,
            },
          };
        }
        return item;
      }
    );
    return {
      ...e,
      data: {
        ...e.data,
        inputData: {
          ...e.data.inputData,
          dependencies: dependenciesList,
        },
      },
    };
  } else if (
    e.data.inputData.nextStage.some(
      (e: any) => e.connectedEdgeId === action.edgeId
    )
  ) {
    const nextStageList = [...e.data.inputData.nextStage].map((e: any) => {
      
      if (e.connectedEdgeId === action.edgeId) {
      
        return {
          ...e,
          triggerEvent: {
            // ...e.triggerEvent,
            [action.inputName]: action.inputValue,
          },
        };
      }
      // console.log(
      //   "although it has a connection id with targeted edge, but not related nextStageList, return unchange",
      //   e
      // );

      return e;
    });

    return {
      ...e,
      data: {
        ...e.data,
        inputData: {
          ...e.data.inputData,
          nextStage: nextStageList,
        },
      },
    };
  } else {
    return e;
  }
}