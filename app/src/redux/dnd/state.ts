import { ReactNode } from "react";
import { ArrowHeadType, Edge, Node } from "react-flow-renderer";

export interface Option {
  value: any;
  label: string;
  key?: string;
  disabled?: boolean;
}

export type WorkflowData = {
  label?: string | ReactNode;
  arrowHeadType?: ArrowHeadType;
  inputData: any;
  data?: { inputData?: { title: string,description:string,assign_to:[],status:string,deadline:string} };
  edgeType?: string;
  text?: string;
  edgeCondition?: string;
  triggerEvent?: {};
  review?: string;
  input?: boolean;
  targetId?: string;
  sourceId?: string;
  connectedEdgeId?: string;
  upload?: string;
  stroke?: string;
  style?: {};
  allElements?: any;
  id?: any;
};

export interface test {
  label: string | ReactNode;
  arrowHeadType: ArrowHeadType;
  inputData: {};
  data: {
    edgeCondition?: string;
    upload?: string;
    inputData: {
      title: string;
      description: string;
      assign_to: string;
      status: string;
      input: string;
      deadline: string;
    };
  };
  edgeType: string;
  text: string;
  edgeCondition: string;
  triggerEvent: {};
  review: string;
  input: boolean;
  targetId: string;
  sourceId: string;
  connectedEdgeId?: string;
  upload: boolean;
  stroke: string;
  style: { stroke?: string };
  id: string;
  source: string;
  target: string;
}

export type inputData = Pick<
  test,
  | "style"
  | "stroke"
  | "data"
  | "inputData"
  | "id"
  | "source"
  | "target"
  | "inputData"
>;

export type NodeLabel = {
  id: string;
  data: {
    inputData: {
      title: string;
      description: string;
      assign_to: string;
      status: string;
      deadline: string;
    };
  };
};

export type edgeLabel = {
  id: string;
  data: {
    review: string;
    upload: string;
  };
};
export type edgeStyle = {
  style: {
    stroke: string;
  };
};

export type NodeDep_Review = {
  connectedEdgeId: string;
  triggerEvent: { review: string };
};

export type NodeDep_Upload = {
  connectedEdgeId: string;
  triggerEvent: { upload: string };
};

export type Item = NodeLabel | edgeLabel | edgeStyle;
