import { Dispatch } from "react";
import { AuthAction } from "./auth/action";
import { panelAction } from "./builderPanal/action";
import { elementAction } from "./dnd/action";
import { UsersInfoAction } from "./usersInfo/action";

export type AppAction = elementAction|panelAction|AuthAction|UsersInfoAction

export type AppDispatch = Dispatch<AppAction>