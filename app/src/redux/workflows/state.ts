import { Elements } from "react-flow-renderer";
import { WorkflowData } from "../dnd/state";

export const initialState: WorkflowState = {
  workflowList: [],
  workflowBeingEdited: [],
  instanceList: [],
  isUpdated:false
};

export type WorkflowState = {
  workflowList: Workflows;
  workflowBeingEdited: Elements<WorkflowData>;
  instanceList: Instances;
  isUpdated:boolean
};

export type Workflows = WorkflowItem[];

export type WorkflowItem = {
  id: number;
  name: string;
  creator: string;
  created_at: Date;
  updater: string | null;
  updated_at: Date | null;
  running_instances: number | null;
};

export type Instances = InstanceItem[];

export type InstanceItem = {
  id: number;
  name: string;
  created_at: Date;
  updated_at: Date;
  initiator: string;
  is_completed: boolean;
  // jsonData:
};


export type InstanceJSONData = Array<TaskJSONData>;

export type TaskJSONData = {
  id: string;
  title: string;
  description: string;
  deadline: Date;
  status: string;
  assign_to: Array<number>;
  nextStage: Array<{ triggerEvent: { upload?: boolean; review?: string } }>;
};

export type Histories = Array<History>;

export type History = {
  id: number;
  workflow_instance_id: number;
  task_id: string;
  task: string;
  description:string;
  status: string;
  start_at: Date;
  action: string | null;
  completed_at: string | null;
  assign_to?:Array<number|string>
};
