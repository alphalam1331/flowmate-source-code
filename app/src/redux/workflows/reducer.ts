import { WorkflowsAction } from "./action";
import { initialState, WorkflowState } from "./state";

export const workflowsReducer = (state: WorkflowState = initialState, action: WorkflowsAction): WorkflowState => {
    switch (action.type) {
        case '@@Workflow.getAllWorkflowsByUserID': {
            const { workflows } = action

            // workflows.forEach((workflow) => { workflow.created_at.})
            return { ...state, workflowList: workflows }
        }
        case '@@Workflow.getWorkflowByID': {
            const { jsonData } = action
            // workflows.forEach((workflow) => { workflow.created_at.})
            return { ...state, workflowBeingEdited: jsonData }
        }
        case '@@Workflow.getAllRunningInstancesByUserID': {
            const {instances} = action
            return {...state, instanceList:instances}
        }

        case '@@Workflow.updateInstanceAction': {
            const {res} = action
            console.log ('Updated Instance: ', res)
            return {...state}
        }
        default:
            return state
    }
}