import { Dispatch } from "redux";
import { getFetchWithToken, postWithToken } from "../../api";
import { dropTemplateAction, elementAction } from "../dnd/action";
import {
  getAllWorkflowsByUserIDAction,
  WorkflowsAction,
  getWorkflowByIDAction,
  getAllRunningInstancesByUserIDAction,
  updateInstanceAction,
} from "./action";

export function getAllWorkflowsByUserIDThunk(userID: number) {
  return async (dispatch: Dispatch<WorkflowsAction>) => {
    const res = await getFetchWithToken(`/workflow/all/${userID}`);
    dispatch(getAllWorkflowsByUserIDAction(res));
  };
}

export function getWorkflowByIDThunk(
  workflowID: number,
  event: React.MouseEventHandler<HTMLButtonElement>
) {
  return async (dispatch: Dispatch<WorkflowsAction | elementAction>) => {
    const res = await getFetchWithToken(`/workflow/json/${workflowID}`);
    dispatch(dropTemplateAction(res, event));
    dispatch(getWorkflowByIDAction(res));
  };
}

export function getAllRunningInstancesByUserIDThunk(userID: number) {
  return async (dispatch: Dispatch<WorkflowsAction>) => {
    const res = await getFetchWithToken(`/instance/${userID}`);
    // console.log(res);
    dispatch(getAllRunningInstancesByUserIDAction(res));
  };
}

export function updateInstanceThunk(
  token:string,
  userID: number,
  workflowInstanceID: number,
  historyID: number,
  msg:string
) {
  return async (dispatch: Dispatch<WorkflowsAction>) => {
    const res:boolean = await postWithToken(`/instance/update`, {userID, workflowInstanceID, historyID, msg});
    dispatch(updateInstanceAction(res))
  };
}
