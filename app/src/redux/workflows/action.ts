import { Elements } from "react-flow-renderer/dist/types";
import { WorkflowData } from "../dnd/state";
import { Instances, Workflows} from "./state";

export type WorkflowsAction =
  | ReturnType<typeof getAllWorkflowsByUserIDAction>
  | ReturnType<typeof getWorkflowByIDAction>
  | ReturnType<typeof getAllRunningInstancesByUserIDAction>
  | ReturnType<typeof updateInstanceAction>

export function getAllWorkflowsByUserIDAction(workflows: Workflows) {
  return {
    type: "@@Workflow.getAllWorkflowsByUserID" as const,
    workflows
  };
}

export function getWorkflowByIDAction(jsonData: Elements<WorkflowData>) {
  return {
    type: "@@Workflow.getWorkflowByID" as const,
    jsonData,
  };
}

export function getAllRunningInstancesByUserIDAction(instances:Instances){
  return {
    type: "@@Workflow.getAllRunningInstancesByUserID" as const,
    instances
  };
}

export function updateInstanceAction(res:boolean){
  return {
    type: "@@Workflow.updateInstanceAction" as const,
    res
  }
}
