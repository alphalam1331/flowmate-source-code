import { panelAction } from "./action";
import { panel } from "./state";

const initialPanel: panel = {
  type: "",
  title: "",
  elementType:"",
  description: "",
  deadline: "",
  selectedElement: null,
  hidden: true,
  upload:false
};

export function panelReducer(
  state: panel = initialPanel,
  action: panelAction
): panel {
  let waitingList = {...state};
  switch (action.type) {
    case "open":
      if('elementType' in action){
        if(action.elementType === 'node'){
          waitingList = {
            ...state,
            type:action.type,
            elementType:action.elementType,
            title: action.title,
            description: action.description,
            deadline: action.deadline,
            selectedElement: action.selectedElement,
            hidden: action.hidden,
            assign_to:action.assign_to,
            status:action.status,
            
          };
        } else if(action.elementType === 'edge' && 'color'in action){
          waitingList = {
            ...state,
            type:action.type,
            elementType:action.elementType,
            title: action.title,
            description: action.description,
            deadline: action.deadline,
            selectedElement: action.selectedElement,
            hidden: action.hidden,
            strokeColor:action.color,
            review:action.review,
            upload:action.upload
          };
        }
        
      }
      // console.log("check edge: ",waitingList)
      return waitingList;

    case "close":
      // console.log("reducer", action.type);
      return state;

    case "clear":
        waitingList = {
            ...state,
            type:action.type,
            title: action.title,
            description: action.description,
            deadline: action.deadline,
            selectedElement: action.selectedElement,
            hidden: action.hidden,
          };
      return waitingList;

    default:
      return state;
  }
}
