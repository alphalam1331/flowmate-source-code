import { Node,Edge } from "react-flow-renderer";
import { WorkflowData } from "../dnd/state";

export function clickNodeAction(element:Node | Edge){
    if("target" in element && element.style ){
        return{
            type:'open',
            elementType:'edge',
            title:'',
            description: '',
            deadline: '',
            color:element.style.stroke,
            selectedElement:element,
            review: element.data.review,
            hidden:false
        }
    } else {
        return {
            type:'open',
            elementType:'node',
            title:element.data.inputData.title,
            description: element.data.inputData.description,
            deadline: element.data.inputData.deadline,
            selectedElement:element,
            hidden:false,
            upload:element.data.inputData.upload,
            status:element.data.inputData.status,
            assign_to:element.data.inputData.assign_to,
        }
    }
    
}

export function closeAction(element:Node | Edge){
    return {
        type:'close',
        title: element.data.inputData.title,
        description: element.data.inputData.description,
        deadline: element.data.inputData.deadline,
        selectedElement:element,
        hidden:true
    }
}

export function clearPanelAction(){
    return {
        type:'clear',
        title: "",
        description: "",
        deadline: "",
        selectedElement:{},
        hidden:true
    }
}

export type panelAction = 
|ReturnType<typeof clickNodeAction>
|ReturnType<typeof closeAction>

