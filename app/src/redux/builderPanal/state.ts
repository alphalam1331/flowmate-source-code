import { Node, Edge } from "react-flow-renderer";
export type panel = {
  type?: string;
  elementType: string;
  title?: string;
  description?: string;
  deadline?: string;
  selectedElement?: Node | Edge | null;
  hidden?: boolean;
  status?: string;
  assign_to?: string;
  review?:string
  strokeColor?:string
  upload?:boolean
};
