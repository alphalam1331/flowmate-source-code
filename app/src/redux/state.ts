
import { Elements } from "react-flow-renderer";
import { AuthState } from "./auth/state";
import { panel } from "./builderPanal/state";
import { WorkflowData } from "./dnd/state";
import { UsersInfoState } from "./usersInfo/state";
import { WorkflowState } from "./workflows/state";

export type RootState = {
    dnd: Elements<WorkflowData>
    buildPanel:panel
    auth: AuthState
    workflows:WorkflowState
    usersInfo:UsersInfoState
}
