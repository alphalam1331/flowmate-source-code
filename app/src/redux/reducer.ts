import { elementReducer } from "./dnd/reducer";
import {combineReducers} from 'redux'
import { panelReducer } from "./builderPanal/reducer";
import { authReducer } from "./auth/reducer";
import { workflowsReducer } from "./workflows/reducer";
import { usersInfoReducer } from "./usersInfo/reducer";






export const rootReducer = combineReducers({
    dnd : elementReducer,
    buildPanel:panelReducer,
    auth: authReducer,
    workflows: workflowsReducer,
    usersInfo:usersInfoReducer
})
