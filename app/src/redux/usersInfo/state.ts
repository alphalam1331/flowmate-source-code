export type UsersInfoState = {
  userList: Users;
};

export type Users = SingleUser[];

export type SingleUser = {
  id:number;
  username: string;
  title: string;
  name: string;
  email: string;
};

export const initialState: UsersInfoState = {
  userList: [],
};
