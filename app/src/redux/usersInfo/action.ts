import {Users} from './state'

export type UsersInfoAction =
  | ReturnType<typeof getAllUsersInfoAction>

export function getAllUsersInfoAction(userList: Users) {
  return {
    type: "@@UsersInfo.getAllUsersInfoAction" as const,
    userList
  };
}
