import { Dispatch } from "redux";
import { getFetch } from "../../api";
import { getAllUsersInfoAction, UsersInfoAction } from "./action";



export function getAllUsersInfoThunk() {
    return async (dispatch: Dispatch<UsersInfoAction>) => {
        const res = await getFetch(`/user?all=true`)
        dispatch(getAllUsersInfoAction(res))
    }
}
