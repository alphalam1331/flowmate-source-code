import { UsersInfoAction } from "./action";
import { initialState, UsersInfoState } from "./state";

export const usersInfoReducer = (
  state: UsersInfoState = initialState,
  action: UsersInfoAction
): UsersInfoState => {
  switch (action.type) {
    case "@@UsersInfo.getAllUsersInfoAction": {
      const { userList } = action;
      return { ...state, userList };
    }
    default:
      return state;
  }
};
