import { applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";


declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
    }
}

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const rootEnhancer = composeEnhancer(
    applyMiddleware(thunk),
);
