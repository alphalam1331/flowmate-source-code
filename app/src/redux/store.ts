import { createStore, compose, applyMiddleware } from "redux";
import { rootReducer } from "./reducer";
import thunk from "redux-thunk";


declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
  }
}

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export let store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));
