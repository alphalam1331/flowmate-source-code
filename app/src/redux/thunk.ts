import { ThunkDispatch } from 'redux-thunk'
import { AppAction } from './action'
import { RootState } from './state'

export type RootThunkDispatch = ThunkDispatch<RootState, null, AppAction>