import "bootstrap/dist/css/bootstrap-grid.min.css";
import "./App.css";
import { useEffect, useState } from "react";
import { Row, Col, Button } from "react-bootstrap";
import {
  RiSideBarFill,
  RiListUnordered,
  RiAddLine,
  RiHistoryFill,
  RiLogoutBoxLine,
} from "react-icons/ri";
import { Route, Routes, useNavigate } from "react-router";
import { Link } from "react-router-dom";
import AllWorkflows from "./Page/AllWorkflows";
import Instances from "./Page/Instances";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "./redux/state";
import Builder from "./Page/Builder";
import Profile from "./Page/Profile";
// import { logoutThunk } from "./redux/auth/thunk";
import { logoutAction } from "./redux/auth/action";
import { setLoadProfileThunk } from "./redux/auth/thunk";

function MainContainer() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  let userData = useSelector((state: RootState) => state.auth.user);
  // console.log({ userData });
  const userInfo = useSelector((state: RootState) => state.auth.userInfo);

  useEffect(() => {
    dispatch(setLoadProfileThunk());
    // console.log("userInfo1",userInfo);
    // dispatch(getAllUsersInfoThunk());
  }, []);
  useEffect(() => {
    console.log("userInfo", userInfo?.title);
    // dispatch(getAllUsersInfoThunk());
  }, [userInfo]);

  useEffect(() => {
    if (!userData) {
      navigate("/");
    }
  }, [userData, navigate]);

  function handleLogout(e: { preventDefault: () => void }) {
    e.preventDefault();
    dispatch(logoutAction());
  }
  
  function getId(){
    dispatch(setLoadProfileThunk())
    return (<></>)
  }
  const [showSideBar, setShowSideBar] = useState(false);
  return (
    <>
      <div className="main-container ">
        <Row className="sidebar">
          <Col>
            <RiSideBarFill
              className="icon"
              onClick={() => {
                setShowSideBar(!showSideBar);
              }}
            />
          </Col>
          {/* <Col onClick={() => navigate("/main/builder")}> */}
          <Col >
            <Link to="/main/builder">
            <RiAddLine className="icon" />
            {showSideBar ? <div>New Workflow</div> : ""}
            </Link>
          </Col>
          <Col>
            <Link to="/main/">
              <RiListUnordered className="icon" />
              {showSideBar ? <div>All Workflows</div> : ""}
            </Link>
          </Col>
          <Col>
            <Link to="/main/instances" >
              <RiHistoryFill className="icon" />
  
              {showSideBar ? <div>Instances</div> : ""}
            </Link>
          </Col>
          {/* <div style={{height:"20%", justifyContent:"spaceBetween",alignContent:"center"}}>
          </div> */}
          <hr></hr>
          <Col>
            <Link
              to="/main/profile"
              style={{
                margin:"auto",
                // maxWidth:" 50vw",
                width: "5.5rem",
                height: "5.5rem",
                backgroundColor: "var(--secondary)",
                borderRadius: " 100%",
                outline: "4px solid white",
              }}
            >
              {userInfo?.title ? (
                userInfo?.title === "Mr." ? (
                  <img
                    style={{justifySelf:"center", width: "auto", height: "100%" }}
                    src={`//joeschmoe.io/api/v1/male/${userInfo.id}`}
                    alt=""
                  />
                ) : (
                  <img
                    src={`//joeschmoe.io/api/v1/female/${userInfo.id}`}
                    alt=""
                  />
                )
              ) :getId()}

              {/* <img src={`//joeschmoe.io/api/v1/${Math.floor(Math.random()*1000)}`} alt="" /> */}
              {/* <FaRegUserCircle className="icon" /> */}
            </Link>
            {showSideBar ? <div>Profile</div> : ""}

          </Col>
          {userData ? (
            <div
              style={{
                margin: "0",
                color: "var(--text)",
                display: "flex",
                justifyContent: "center",
              }}
            >
              <div style={{ textAlign: "center", margin: "10px 0 0 0" }}>
                {userData.payload.username}
              </div>
              
            </div>
          ) : (
            ""
          )}
          <Col>
            <div onClick={handleLogout} style={{ margin: "0" }}>
              <RiLogoutBoxLine className="icon" />
              {showSideBar ? <div>Logout</div> : ""}
            </div>
          </Col>
        </Row>
        <div className="page-container ">
          <Routes>
            <Route path="/" element={<AllWorkflows />} />
            <Route path="/builder" element={<Builder />} />
            <Route path="/instances" element={<Instances />} />
            <Route path="/profile" element={<Profile />} />
          </Routes>
        </div>
      </div>
    </>
  );
}

export default MainContainer;
