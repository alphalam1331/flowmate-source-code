import { useState } from "react";
import {LandingModal} from "./LandingModal"
import { ShowSignUp } from "./Signup";

function ShowLandingModal () {
    const [show, setShow] = useState(false);

    const closeModalHandler = () => setShow(false);
  
    return (
      <div >
        { show ? <div onClick={closeModalHandler} className="back-drop"></div> : null }
        <button onClick={() => setShow(true)} className="btn-openModal"
        style={{ 
        flexDirection:"row",
          width: "240px",
          height: "126px",
          border:" 1px solid black",
          background: "#535353",
          color: "White",
          fontSize:"3rem",
          margin: "2rem",
          padding: "0.5rem 1.8rem",
          outline: "none",
          cursor: "pointer",
          boxShadow: "0 20px 6px -6px black"
        }}>Sign Up</button>
        <ShowSignUp show={show} close={closeModalHandler} />
      </div>
    );
      }
  export default ShowLandingModal;