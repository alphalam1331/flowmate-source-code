import React, {
  useState,
  useRef,
  useEffect,
  DragEventHandler,
  ChangeEventHandler,
  useMemo,
} from "react";
import ColorSelectorNode from "./cards/jacknode";
import custom1 from "./cards/custom_node1";
import ReactFlow, {
  MiniMap,
  Controls,
  Background,
  ReactFlowProvider,
  Connection,
  Edge,
  OnLoadParams,
  ControlButton,
} from "react-flow-renderer";
import { useDispatch, useSelector } from "react-redux";
import buttonEdge from "./cards/buttonEdge";
import customEdge1 from "./cards/custom_edge1";
import sendEmailCard from "./cards/sendEmailCard";
import {
  addEdgeAction,
  clearAllElements,
  dropElement,
  inputDataAction,
  removeElement,
  updateAllElementAction,
} from "../redux/dnd/action";
import { RootState } from "../redux/state";
import {
  clearPanelAction,
  clickNodeAction,
} from "../redux/builderPanal/action";
import { store } from "../redux/store";
import styles from "./Flow2.module.css";
import { useLocation } from "react-router";
import {
  getAllWorkflowsByUserIDThunk,
  getWorkflowByIDThunk,
} from "../redux/workflows/thunk";
import { RiDeleteBin7Fill, RiTable2 } from "react-icons/ri";
import { postWithToken } from "../api";
import { WorkflowItem, Workflows } from "../redux/workflows/state";

let idNumber = 0;

const Flow2 = () => {
  let query = useQuery();
  const [workflowID, setWorkflowID] = useState<string | null>(null);
  const [workflowName, setWorkflowName] = useState<string>("");
  const [showCardSelection, setShowCardSelection] = useState<boolean>(true);

  // const jwtToken = useSelector((state: RootState) => state.auth.user?.token);
  const userID = useSelector((state: RootState) => state.auth.user?.payload.id);
  const dispatch = useDispatch();
  const cardElement = useSelector((state: RootState) => state.dnd);
  const panelData = useSelector((state: RootState) => state.buildPanel);
  const selectedNode = useSelector(
    (state: RootState) => state.buildPanel.selectedElement
  );
  const workflowList = useSelector(
    (state: RootState) => state.workflows.workflowList
  );
  const reactFlowWrapper = useRef<HTMLDivElement>(null);
  const [reactFlowInstance, setReactFlowInstance] = useState<OnLoadParams>(
    0 as any
  );
  const edgeTypes = {
    buttonEdge: buttonEdge,
    customEdge1: customEdge1,
  };

  const userId = useSelector((state: RootState) => state.auth.user?.payload.id);

  const getId = (): string => {
    idNumber = idNumber + 1;
    const id = new Date().getTime() + "_" + idNumber;
    return id;
  };

  const onConnect = (params: Edge | Connection) => {
    dispatch(addEdgeAction(params, getId(), "customEdge1"));
  };
  const removeCard = (event: React.MouseEvent<HTMLButtonElement>) => {
    dispatch(removeElement(event.currentTarget.name));
    dispatch(clearPanelAction());
  };

  const onLoad = async (reactFlowInstance: OnLoadParams) => {
    // reactFlowInstance.fitView();
    setReactFlowInstance(reactFlowInstance);
  };

  const onDragOver: DragEventHandler = (event) => {
    event.preventDefault();
    event.dataTransfer.dropEffect = "move";
  };

  const nodeTypes = {
    selectorNode: ColorSelectorNode,
    sendEmail: sendEmailCard,
    custom1: custom1,
  };

  
  useEffect(() => {
    if (query.get("wf")!=="") {
      setWorkflowID(query.get("wf"));
      //   "=====wf link different=====",
      //   " Now URL",
      //   window.location.href
      // );
    } else{
      dispatch(clearAllElements());

    }
  }, [query]);

  useEffect(() => {
    if (workflowID && userId) dispatch(getAllWorkflowsByUserIDThunk(userId));
    if (Boolean(workflowID) === false) {
      setWorkflowName("");
    }
  }, [workflowID]);

  useEffect(() => {
   
    let targetWorkflow: Workflows = workflowList.filter(
      (e: WorkflowItem) => e.id === Number(workflowID)
    );
 
    if (Boolean(workflowID) === true && targetWorkflow.length > 0) {
      const name = targetWorkflow[0].name;
      setWorkflowName(name);
    }
  }, [workflowList]);

  async function saveWorkflowChange() {
    let count = 0;
    const data = store.getState().dnd;
    let targetWorkflow: Workflows = workflowList.filter(
      (e: WorkflowItem) => e.id === Number(workflowID)
    );
    const name = targetWorkflow[0].name;
   
    if (!name || name === "") {
      alert("No workflow Name");
      return;
    }

    const cleanData = data.map((e: any) => {
      if ("position" in e && e.data.inputData.step === 0) {
        delete e.data.inputData.step;
        return e;
      } else {
        return e;
      }
    });

    const sortedData = cleanData.map((e: any) => {
      if (!("position" in e && e.data.inputData.dependencies.length === 0)) {
        return e;
      } else {
        count += 1;
        return {
          ...e,
          data: {
            ...e.data,
            inputData: {
              ...e.data.inputData,
              step: 0,
            },
          },
        };
      }
    });

    if (count > 1) {
      alert(
        "Only one interface is allowed, please integrate your whole workflow or remove the excessive interfaces"
      );
      return;
    } else if (count === 0) {
      alert(
        "Please release a node as an interface, otherwise dead loop is caused"
      );
      return;
    }
    console.log("check",sortedData)

    const jsonData = JSON.stringify(sortedData);
    const response = await postWithToken("/workflow/edit", {
      userID,
      workflowID,
      jsonData,
    }); 
    console.log(response)
    if (response.name) {
      alert("Workflow Updated");
    } else if (response.message) {
      alert(response.message);
    }
  }

  const saveWorkflow = async (event: React.MouseEvent<HTMLButtonElement>) => {
    let count = 0;
    const data = store.getState().dnd;

    const name = workflowName;
    if (!name || name === "") {
      alert("No workflow Name");
      return;
    }
    const sortedData = data.map((e: any) => {
      if (!("position" in e && e.data.inputData.dependencies.length === 0)) {
        return e;
      } else {
        count += 1;
        return {
          ...e,
          data: {
            ...e.data,
            inputData: {
              ...e.data.inputData,
              step: 0,
            },
          },
        };
      }
    });

    if (count > 1) {
      alert(
        "Only one interface is allowed, please integrate your whole workflow or remove the excessive interfaces"
      );
      return;
    } else if (count === 0) {
      alert(
        "Please release a node as an interface, otherwise dead loop is caused"
      );
      return;
    }
    console.log("check",sortedData)
    
    const jsonData = JSON.stringify(sortedData);

    const response = await postWithToken("/workflow/create", {
      userID,
      name,
      jsonData,
    });

    if (!isNaN(response)) {
      alert("Workflow Created");
    } else if (response.message) {
      alert(response.message);
    }
  };
  const onDragStart = (event: React.DragEvent, nodeType: string) => {
    if (event.dataTransfer) {
      event.dataTransfer.setData("application/reactflow", nodeType);
      event.dataTransfer.effectAllowed = "move";
    }
  };

  function sideBar() {
    return (
      <aside
        style={{
          position: "fixed",
          left: "90%",
          zIndex: "10000",
          width: "10%",
        }}
        hidden={showCardSelection}
      >
        <button
          style={{ background: "transparent", border: "0" }}
          onClick={() => setShowCardSelection(true)}
        >
          <div className={styles.cardIcon}>X</div>
        </button>
        <div className="description">
          You can drag these cards to the chart.
        </div>
        <div
          className={styles.yellowCard}
          onDragStart={(event) => onDragStart(event, "custom1")}
          draggable
        >
          <table className={styles.table}>
            <tbody className={styles.tbody}>
              <tr className={styles.tr}>
                <th scope="row" className={styles.th}>
                  Title:
                </th>
                <td className={styles.whiteColorBox}></td>
              </tr>
              <tr className={styles.tr}>
                <th scope="row" className={styles.th}>
                  Description:
                </th>
                <td className={styles.whiteColorBox}></td>
              </tr>
              <tr className={styles.tr}>
                <th scope="row" className={styles.th}>
                  Assign to:
                </th>
                <td className={styles.whiteColorBox}></td>
              </tr>
              <tr className={styles.tr}>
                <th scope="row" className={styles.th}>
                  Status:
                </th>
                <td className={styles.whiteColorBox}></td>
              </tr>
              <tr className={styles.tr}>
                <th scope="row" className={styles.th}>
                  Deadline:
                </th>
                <td className={styles.whiteColorBox}></td>
              </tr>
            </tbody>
          </table>
        </div>
      </aside>
    );
  }

  const onDrop = (event: React.DragEvent) => {
    event.preventDefault();
    if (!reactFlowWrapper.current) {
      return;
    }
    const reactFlowBounds = reactFlowWrapper.current.getBoundingClientRect();
    const position = reactFlowInstance.project({
      x: event.clientX - reactFlowBounds.left,
      y: event.clientY - reactFlowBounds.top,
    });

    dispatch(
      dropElement(event, position, getId(), removeCard, panelData, inputChange)
    );
  };
  const inputChange: ChangeEventHandler = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    if (
      selectedNode &&
      (event.target.name === "review" || event.target.name === "upload")
    ) {
      dispatch(
        inputDataAction(
          event.target.name,
          event.target.value,
          selectedNode,
          removeCard,
          inputChange,
          panelData
        )
      );
    } else if (selectedNode) {
      dispatch(
        inputDataAction(
          event.target.name,
          event.currentTarget.value,
          selectedNode,
          removeCard,
          inputChange,
          panelData
        )
      );

      dispatch(clickNodeAction(selectedNode));
    }
  };

  useEffect(() => {
    if (workflowID) {
      dispatch(getWorkflowByIDThunk(+workflowID, removeCard));
    } else {
      dispatch(clearAllElements());
      dispatch(clearPanelAction());
    }
  }, [workflowID]);

  useEffect(() => {
    if (store.getState().buildPanel.type === "clear") {
      return;
    }
    const elementState = store.getState().dnd;
    if (selectedNode) {
      const currentTargetNode = elementState.filter(
        (e) => e.id === selectedNode.id
      );
      dispatch(clickNodeAction(currentTargetNode[0]));
      dispatch(updateAllElementAction(cardElement));
    }
  }, [cardElement]);

  function saveButton() {
    if (Boolean(workflowID) === true) {
      return (
        <div style={{ width: "100%" }}>
          Workflow name:
          <input
            style={{ width: "150px", margin: "0px 5px" }}
            type="text"
            onChange={(e) => setWorkflowName(e.currentTarget.value)}
            value={workflowName}
          />
          <button
            className={styles.saveBtn}
            onClick={(event) => saveWorkflow(event)}
          >
            Create Workflow
          </button>
          <button
            className={styles.saveBtn}
            onClick={saveWorkflowChange}
          >
            Update Workflow
          </button>
        </div>
      );
    } else {
      return (
        <div style={{ width: "100%" }}>
          Workflow name:
          <input
            style={{ width: "150px", margin: "0px 5px" }}
            type="text"
            onChange={(e) => setWorkflowName(e.currentTarget.value)}
            value={workflowName}
          />
          <button
            className={styles.saveBtn}
            onClick={(event) => saveWorkflow(event)}
          >
            Create Workflow
          </button>
        </div>
      );
    }
  }

  return (
    <ReactFlowProvider>
      <div
        ref={reactFlowWrapper}
        className={styles.chart + " reactflow-wrapper "}
      >
        <ReactFlow
          elements={cardElement}
          onConnect={onConnect}
          onLoad={onLoad}
          edgeTypes={edgeTypes}
          snapToGrid={true}
          snapGrid={[15, 15]}
          onDrop={onDrop}
          onDragOver={onDragOver}
          nodeTypes={nodeTypes}
          elementsSelectable={true}
          defaultZoom={0.5}
          maxZoom={1.5}
          minZoom={0.01}
        >
          <MiniMap nodeColor={"black"} />
          <Controls>
            <ControlButton onClick={() => dispatch(clearAllElements())}>
              <RiDeleteBin7Fill />
            </ControlButton>
          </Controls>
          {/* <Controls /> */}
          <Background color="#aaa" gap={16} />
        </ReactFlow>
      </div>

      <div
        style={{
          display: "flex",
          justifyContent: "center",
          height: "2rem",
          position: "absolute",
          top: "1rem",
          left: "10%",
          right: "10%",
          zIndex: "10",
          fontSize: "1rem",
        }}
      >
        <div
          style={{
            width: "fit-content",
            display: "flex",
            justifyContent: "center",
            padding: "1rem",
            borderRadius: "3rem",
            background: "var(--primary)",
            color: "var(--text)",
            alignSelf: "center",
          }}
        >
          {saveButton()}
        </div>
        <div
          className={styles.tooltip}
          onDragStart={(event) => onDragStart(event, "custom1")}
          draggable
        >
          <RiTable2
            style={{ content: "fasfas", fontSize: "40", color: "white" }}
          />
          <span className={styles.tooltiptext}>
            Drag and drop me to the chart
          </span>
        </div>
      </div>
    </ReactFlowProvider>
  );
};

export default Flow2;

function useQuery() {
  const { search } = useLocation();
  return useMemo(() => new URLSearchParams(search), [search]);
}
