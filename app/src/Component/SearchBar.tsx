import styles from "./PageSection.module.css";
type Props = {
  header: string;
  className?:string
};
function SearchBar({ header, className }: Props) {
  return (
    <div className={styles.pageHeader + ' ' + className}>
      {header}
      <input type="text"></input>
      <input type="submit" value="Search" className="search-button"></input>
    </div>
  );
}

export default SearchBar;
