import './BuilderSideBar.css'

function BuilderSideBar() {
  
    const onDragStart = (event: React.DragEvent<HTMLDivElement>, nodeType: string) => {
        event.dataTransfer.setData('application/reactflow', nodeType);
        event.dataTransfer.effectAllowed = 'move';
      };
      
  return (
    <aside>
      <h1>Nodes</h1>
      <div onDragStart={(event) => onDragStart(event, 'input')} draggable>
        Input Node
      </div>
      <div onDragStart={(event) => onDragStart(event, 'default')} draggable>
        Default Node
      </div>
      <div onDragStart={(event) => onDragStart(event, 'output')} draggable>
        Output Node
      </div>
    </aside>
  )
}

export default BuilderSideBar
