import SignUpPage from '../Page/SignUpPage';

export const ShowSignUp = ({ show, close }) => {
  return (
    <div className="modal-wrapper"
      style={{
        transform: show ? 'translateY(-70vh)' : 'translateY(0vh)',
        opacity: show ? '1' : '0',
        background: "#fff",
        width: "100%",
        maxWidth: "800px",
        boxSizing: "border-box",
        display: "flex",
        flexDirection: "column",
        transition:" all .5s ease"
      }}
    >
      <div className="modal-header"style={{
        background:" #535353",
        color: "#e9e9e9",
        padding: "1rem",
        display:" flex",
        alignIitems: "center",
        justifyContent: "space-between",
      }}>
        <p>Sign Up</p>
        <span onClick={close} className="close-modal-btn"style={{
          cursor: "pointer",
          fontSize: "1.5rem"
        }}>x</span>
      </div>
      <div className="modal-content"
      style={{padding: "1.5rem 1rem"}}>
        <div className="modal-body"
        style={{marginBottom: "1rem"}}>
          <SignUpPage/>
        </div>
        {/* <div className="modal-footer">
          <button onClick={close} className="btn-cancel"style={{display: "block",
          fontSize: "1rem",
          marginLeft:"auto",
          padding:" 0.5rem 1.8rem",
          background: "#A35AFF",
          color: "#fff",
          border: "none",
          outline: "none",
          cursor: "pointer",
          fontSize: "1.5rem"}}>Close</button>
        </div> */}
      </div>
    </div>
  )
};