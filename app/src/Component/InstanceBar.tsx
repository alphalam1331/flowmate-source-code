import Col from "react-bootstrap/esm/Col";
import Row from "react-bootstrap/esm/Row";
import { IoMdArrowDropdown } from "react-icons/io";
import { GoPrimitiveDot } from "react-icons/go";
import { Histories, History, InstanceJSONData } from "../redux/workflows/state";
import styles from "./InstanceBar.module.css";
import { formatDate } from "../tools";
import { useEffect, useState } from "react";
import HistoryBar from "./HistoryBar";
import { getFetch } from "../api";
import ProgressBox from "./ProgressBox";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { getAllUsersInfoThunk } from "../redux/usersInfo/thunk";

type props = {
  id: number;
  name: string;
  created_at: Date;
  updated_at: Date;
  initiator: string;
  is_completed: boolean;
};

function InstanceBar({
  id,
  name,
  created_at,
  updated_at,
  initiator,
  is_completed,
}: props) {
  const dispatch = useDispatch();
  const [updated, setUpdated] = useState(false);
  const [showRunningStatus, setShowRunningStatus] = useState("none");
  const [showHistoriesHint, setShowHistoriesHint] = useState("none");
  const [showHistories, setShowHistories] = useState(false);

  const [instanceJSONData, setInstanceJSONData] = useState<InstanceJSONData>();
  const [histories, setHistories] = useState<Histories>();
  const [currentTasks, setCurrentTasks] = useState<Histories>();
  const userList = useSelector((state: RootState) => state.usersInfo.userList);

  useEffect(() => {
    getFetch(`/instance/json/${id}`).then((instance) => {
      setInstanceJSONData(instance);
      getFetch(`/instance/history/${id}`).then((histories) => {
        console.log({ histories });
        setHistories(histories);
      });
    });
    if (userList.length <= 0) dispatch(getAllUsersInfoThunk());
    setUpdated(false);
  }, [id, updated]);

  useEffect(() => {
    // console.log(userList);
    if (instanceJSONData && histories) {
      histories.forEach((record: History) => {
        const assignees: Array<string | number> = instanceJSONData?.filter(
          (task) => task.id === record.task_id
        )[0].assign_to;

        assignees.forEach((assigneeID, idx) => {
          // console.log({ assigneeID });
          const users = userList.filter(
            (userInfo) => userInfo.id === assigneeID
          );
          // console.log({ users });
          if (users.length === 1) assignees[idx] = users[0].username;
        });
        record.assign_to = assignees;
        // console.log (assignees)
      });
      // console.log("histories Kwith assignees: ", id, histories);

      // setHistories( historiesWithAssignees);
      const workInProgress = histories?.filter(
        (record: any) => record.completed_at === null
      );

      setCurrentTasks(workInProgress);
    }
  }, [instanceJSONData, histories]);

  const onShowStatus = (e: React.MouseEvent) => {
    e.preventDefault();
    setShowRunningStatus("block");
  };

  const onHideStatus = (e: React.MouseEvent) => {
    e.preventDefault();
    setShowRunningStatus("none");
  };

  const onShowHint = (e: React.MouseEvent) => {
    e.preventDefault();
    setShowHistoriesHint("block");
  };

  const onHideHint = (e: React.MouseEvent) => {
    e.preventDefault();
    setShowHistoriesHint("none");
  };
  return histories && currentTasks ? (
    <div className={styles.container}>
      <li id={`${id}`} className={styles.bar}>
        <GoPrimitiveDot
          className={is_completed ? styles.completed : styles.inProgress}
          onMouseEnter={(e) => onShowStatus(e)}
          onMouseLeave={(e) => onHideStatus(e)}
        ></GoPrimitiveDot>
        <div
          style={{ display: showRunningStatus }}
          className={styles.runningStatus}
        >
          {is_completed ? "Completed" : "Running"}
        </div>
        <Row
          className={
            showHistories
              ? styles.content
              : styles.content + " " + styles.bottomRadius
          }
        >
          <Col xs={3}>{name}</Col>
          <Col xs={2} className={styles.small}>
            <div>
              <span>{initiator}</span>
            </div>
            <div>
              at <span>{formatDate(created_at)}</span>
            </div>
          </Col>

          {is_completed ? (
            <Col xs={5} className={styles.small}>
              Completed
            </Col>
          ) : currentTasks.length === 1 ? (
            <>
              <Col
                xs={5}
                style={{ display: "flex", justifyContent: "space-evenly" }}
              >
                <div className={styles.small}>
                  <div>
                    <span>{currentTasks[0].task}</span>
                  </div>
                  <div>
                    Description: <span>{currentTasks[0].description}</span>
                  </div>
                </div>
                <div className={styles.small}>
                  <div>
                    Asignee:{" "}
                    <span>{currentTasks[0].assign_to?.join(", ")}</span>
                  </div>
                  <div>
                    Status: <span>{currentTasks[0].status}</span>
                  </div>
                </div>
              </Col>
            </>
          ) : currentTasks.length > 1 ? (
            <Col xs={5} className={styles.small}>
              More than one task are in progress. See History for more Detail
            </Col>
          ) : (
            ""
          )}
        </Row>
        <IoMdArrowDropdown
          className={styles.dropdownBtn}
          onClick={() => setShowHistories(!showHistories)}
          onMouseEnter={(e) => onShowHint(e)}
          onMouseLeave={(e) => onHideHint(e)}
        ></IoMdArrowDropdown>

        {/* hover card */}
        <div
          style={{ display: showHistoriesHint }}
          className={styles.showHistoriesHint}
        >
          Show Histories
        </div>
      </li>
      {showHistories ? (
        <ul className={styles.progressContainer}>
          {instanceJSONData?.map((task) => {
            const currentTasksID = [
              ...currentTasks.map((task) => task.task_id),
            ];
            return currentTasksID.includes(task.id) ? (
              <ProgressBox key={task.id} title={task.title} current={true} />
            ) : (
              <ProgressBox key={task.id} title={task.title} current={false} />
            );
          })}
        </ul>
      ) : (
        ""
      )}

      {showHistories ? (
        <ul className={styles.historiesContainer}>
          {histories?.map((history) => (
            <HistoryBar
              key={history.id}
              history={history}
              instanceJSONData={instanceJSONData!}
              onUpdate={setUpdated}
            />
          ))}
        </ul>
      ) : (
        ""
      )}
    </div>
  ) : (
    <div></div>
  );
}

export default InstanceBar;
