import { MouseEventHandler, ReactNode } from "react";
import { ArrowHeadType, Elements } from "react-flow-renderer";

import { WorkflowData } from "../redux/dnd/state";

// export interface initialElement extends Node, Edge {
//   emaimAdress:
// }

let idNumber = 0;
const id = (): string => {
  idNumber = idNumber + 1;
  const id = new Date().getTime() + "_" + idNumber;
  return id;
};

export const template_demo = (
  remove: MouseEventHandler,

): Elements<WorkflowData> => {
  const cardId: string = id();
  return [
    {
      id: cardId,
      type: "default",
      position: { x: 250, y: 0 },
      data: {
        inputData: {
          title: "",
          description: "",
          deadline: "",
          status: "",
          assign_to: "",
          dependencies: [],
          nextStage: [],
        },
        label: (
          <>
            <button name={cardId} onClick={remove}>
              x
            </button>
            <p>title: </p>
            <p>Description:</p>
            <p>DeadLine:</p>
            {/* <input id={action.id} type="text" onChange={action.inputData} /> */}
          </>
        ),
      },
      style: {
        color: "#333",
        border: "1px solid #222138",
        width: 280,
      },
    },
  ];
};

export const initialElement: Elements<WorkflowData> = [
  // {
  //   id: "1",
  //   type: "input",
  //   data: {
  //     label: <>Welcome to React Flow!</>,
  //   },
  //   position: { x: 250, y: 0 },
  // },
  // {
  //   id: "2",
  //   data: {
  //     label: <input type='text'></input>,
  //   },
  //   position: { x: 100, y: 100 },
  // },
  // {
  //   id: "3",
  //   data: {
  //     label: <>This one has a custom style</>,
  //   },
  //   position: { x: 400, y: 100 },
  //   style: {
  //     background: "#D6D5E6",
  //     color: "#333",
  //     border: "1px solid #222138",
  //     width: 180,
  //   },
  // },
  // {
  //   id: "4",
  //   position: { x: 250, y: 200 },
  //   data: {
  //     label: "Another default node",
  //   },
  // },
  // {
  //   id: "5",
  //   data: {
  //     label: "Node id: 5",
  //   },
  //   position: { x: 250, y: 325 },
  // },
  // {
  //   id: "6",
  //   type: "output",
  //   data: {
  //     label: (
  //       <>
  //         An <strong> output node</strong>
  //       </>
  //     ),
  //   },
  //   position: { x: 100, y: 480 },
  // },
  // {
  //   id: "7",
  //   type: "output",
  //   data: { label: "Another output node" },
  //   position: { x: 400, y: 450 },
  // },
  // { id: "e1-2", source: "1", target: "2", label: "this is an edge label" },
  // { id: "e1-3", source: "1", target: "3" },
  // {
  //   id: "e3-4",
  //   source: "3",
  //   target: "4",
  //   animated: true,
  //   label: "animated edge",
  // },
  // {
  //   id: "e4-5",
  //   source: "4",
  //   target: "5",
  //   arrowHeadType: ArrowHeadType.ArrowClosed,
  //   label: "edge with arrow head",
  // },
  // {
  //   id: "e5-6",
  //   source: "5",
  //   target: "6",
  //   type: "smoothstep",
  //   label: "smooth step edge",
  // },
  // {
  //   id: "e5-7",
  //   source: "5",
  //   target: "7",
  //   type: "step",
  //   style: { stroke: "#f6ab6c" },
  //   label: "a step edge",
  //   animated: true,
  //   labelStyle: { fill: "#f6ab6c", fontWeight: 700 },
  // },
];

export default initialElement;
