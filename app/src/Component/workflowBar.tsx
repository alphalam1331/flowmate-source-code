import { useState } from "react";
import Col from "react-bootstrap/esm/Col";
import Row from "react-bootstrap/esm/Row";
import { RiMore2Fill } from "react-icons/ri";
import { useSelector } from "react-redux";
import { postWithToken } from "../api";
import { RootState } from "../redux/state";
import { WorkflowItem } from "../redux/workflows/state";
import { formatDate } from "../tools";
import ConfirmationPopUp from "./ConfirmationPopUp";
import styles from "./workflowBar.module.css";
import WorkflowOptions from "./WorkflowOptions";
import { ToastContainer, toast } from "react-toastify";

function WorkflowBar({
  id,
  name,
  creator,
  created_at,
  updater,
  updated_at,
  running_instances,
}: WorkflowItem) {
  const [showEditMenu, setShowEditMenu] = useState(false);
  const [showPopUp, setShowPopUp] = useState(false);
  const userID = useSelector((state: RootState) => state.auth.user?.payload.id);

  const toggleMenu = () => {
    setShowEditMenu(!showEditMenu);
  };

  const togglePopUp = () => {
    setShowPopUp(!showPopUp);
  };

  const activateWorkflow = async () => {
    const res = await postWithToken("/workflow/activate", {
      userID,
      workflowID: id,
    });
  };
  
  return (
    <>
      <li id={String(id)} className={styles.bar}>
        <Row className={styles.content}>
          <Col xs={3}>{name}</Col>
          <Col xs={3} className={styles.small}>
            {
              <>
                <div>
                  <span>{creator}</span>
                </div>
                <div>
                  {" "}
                  at <span>{formatDate(created_at)}</span>
                </div>
              </>
            }
          </Col>
          <Col xs={3} className={styles.small}>
            {updated_at ? (
              <>
                <div>
                  <span>{updater}</span>
                </div>
                <div>
                  {" "}
                  at <span>{formatDate(updated_at)}</span>
                </div>
              </>
            ) : (
              <>
                <div>Has not been modified</div>
              </>
            )}
          </Col>
          <Col xs={2}>{running_instances ? running_instances : "0"}</Col>
        </Row>
        <RiMore2Fill className={styles.editBtn} onClick={toggleMenu} />
        {showEditMenu ? (
          <WorkflowOptions
            id={id}
            togglePopUp={togglePopUp}
            toggleMenu={toggleMenu}
          />
        ) : (
          ""
        )}
        {showPopUp ? (
          <ConfirmationPopUp
            name={name}
            onConfirm={() => {
              activateWorkflow();
              toast("good");
            }}
            togglePopUp={togglePopUp}
          />
        ) : (
          ""
        )}
      </li>
    </>
  );
}

export default WorkflowBar;
