import { Elements } from "react-flow-renderer";
import { WorkflowData } from "../../redux/dnd/state";
import { RootState } from "../../redux/state";
import { SingleUser, Users } from "../../redux/usersInfo/state";
import { Option } from "./custom_node1";

export function findNodeValue(
  name: string,
  elementId: string,
  elementsState: Elements<WorkflowData>
) {
  let selectedElement = elementsState.find((e: any) => e.id === elementId);
  // console.log("elementsState",elementsState,"elementId",elementId);
  
  let value = selectedElement?.data?.inputData[name];
  if(name === "to"){
    value = selectedElement?.data?.inputData.action.email[name];
  }
  return value;
}

export function sortAssignValue(
  name: string = "assign_to",
  elementId: string,
  elementsState: Elements<WorkflowData>,
  allUserInfo: Users
) {
  const assignIdArray = findNodeValue("assign_to", elementId, elementsState);
  const userList = allUserInfo.filter((e:SingleUser)=>assignIdArray.some((id:any)=>id === e.id)) 
  const sortedList:Option[]  = userList.map((e:SingleUser)=>{return {"label": e.name, "value": e.id}})
  return sortedList

}

export function sortEmailValue(
  name: string = "to",
  elementId: string,
  elementsState: Elements<WorkflowData>,
  allUserInfo: Users
) {
  const assignEmailArray = findNodeValue("to", elementId, elementsState);
  const userList = allUserInfo.filter((e:SingleUser)=>assignEmailArray.some((id:any)=>id === e.id)) 
  const sortedList:Option[]  = userList.map((e:SingleUser)=>{return {"label": e.email, "value": e.email}})
  return sortedList

}

export function findEdgeValue(
  name: string,
  elementId: string,
  elementsState: any
) {
  let selectedElement = elementsState.find((e: any) => e.id === elementId);
  let value = selectedElement?.data[name];
  return value;
}
