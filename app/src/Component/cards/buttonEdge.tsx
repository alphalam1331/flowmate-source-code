import React from "react";
import {
  Edge,
  getBezierPath,
  getEdgeCenter,
  getMarkerEnd,
  WrapEdgeProps,
} from "react-flow-renderer";

import { useDispatch, useSelector } from "react-redux";
import { clearPanelAction } from "../../redux/builderPanal/action";
import { removeElement } from "../../redux/dnd/action";
import { RootState } from "../../redux/state";
const foreignObjectSize = 60;

export default function CustomEdge({
  id,
  sourceX,
  sourceY,
  targetX,
  targetY,
  sourcePosition,
  targetPosition,
  style = {},
  data,
  arrowHeadType,
  markerEndId,
}: WrapEdgeProps) {
  const edgePath = getBezierPath({
    sourceX,
    sourceY,
    sourcePosition,
    targetX,
    targetY,
    targetPosition,
  });
  const markerEnd = getMarkerEnd(arrowHeadType, markerEndId);
  const [edgeCenterX, edgeCenterY] = getEdgeCenter({
    sourceX,
    sourceY,
    targetX,
    targetY,
  });
  const dispatch = useDispatch();

  const OnEdgeClick = (evt: React.MouseEvent<HTMLButtonElement>) => {
    evt.stopPropagation();

    dispatch(clearPanelAction());
    dispatch(removeElement(evt.currentTarget.name));
  };
  return (
    <>
      <path
        id={id}
        style={style}
        className="react-flow__edge-path"
        d={edgePath}
        markerEnd={markerEnd}
      />
      <foreignObject
        width={"10%"}
        height={"20%"}
        style={{}}
        x={edgeCenterX - foreignObjectSize}
        y={edgeCenterY - foreignObjectSize}
        className="edgebutton-foreignobject"
        requiredExtensions="http://www.w3.org/1999/xhtml"
      >
        <div style={{ color: "white", background: "grey" }}>
          <button
            className="edgebutton"
            name={id}
            onClick={(event) => OnEdgeClick(event)}
          >
            ×
          </button>
          <div>{data.label}</div>
          <p>ggggggggggggggggggggggg</p>
        </div>
        <div></div>
      </foreignObject>
    </>
  );
}
