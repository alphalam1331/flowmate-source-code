import React, { memo, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RiDeleteBin6Line, RiEditBoxLine } from "react-icons/ri";
import { Handle, NodeComponentProps, Position } from "react-flow-renderer";
import styles from "./custom1.module.css";
import { removeElement, updateNode } from "../../redux/dnd/action";
import { RootState } from "../../redux/state";
import { clearPanelAction } from "../../redux/builderPanal/action";
import { MultiSelect } from "react-multi-select-component";
import { getAllUsersInfoThunk } from "../../redux/usersInfo/thunk";
import { SingleUser } from "../../redux/usersInfo/state";
import {
  findNodeValue,
  sortAssignValue,
  sortEmailValue,
} from "./getElementValue";

export interface Option {
  value: any;
  label: string;
  key?: string;
  disabled?: boolean;
}

export default memo(
  ({ id, data, isConnectable, style }: NodeComponentProps) => {
    const dispatch = useDispatch();
    // const panelData = useSelector((state: RootState) => state.buildPanel);
    const elements = useSelector((state: RootState) => state.dnd);
    const allUserInfo = useSelector(
      (state: RootState) => state.usersInfo.userList
    );
    let userList: Array<{ label: string; value: number }> | [];
    let emailList: Array<{ label: string; value: string }> | [];

    if (allUserInfo.length > 0) {
      userList = allUserInfo.map((e: SingleUser) => {
        return { label: e.name, value: e.id };
      });
      emailList = allUserInfo.map((e: SingleUser) => {
        return { label: e.email, value: e.email };
      });
    } else {
      userList = [];
      emailList = [];
    }

    const removeCard = (event: React.MouseEvent<HTMLButtonElement>) => {
      dispatch(removeElement(event.currentTarget.name));
      dispatch(clearPanelAction());
    };

    const [selectedUser, setSelectedUser] = useState<Option[]>([]);
    const [selectedEmail, setSelectedEmail] = useState<Option[]>([]);
    const [expandEmailInput, setExpandEmailInput] = useState(true);

    const nodeId = id;
    useEffect(() => {
      if (elements.length === 0) {
        return;
      }
      dispatch(getAllUsersInfoThunk());

      setSelectedUser(
        sortAssignValue("assign_to", nodeId, elements, allUserInfo)
      );
      setSelectedEmail(sortEmailValue("to", nodeId, elements, allUserInfo));
    }, []);
    useEffect(() => {
      if (elements.length === 0) return;

      setSelectedUser(
        sortAssignValue("assign_to", nodeId, elements, allUserInfo)
      );
      setSelectedEmail(sortEmailValue("to", nodeId, elements, allUserInfo));
    }, [allUserInfo]);

    return (
      <div>
        <div style={{ backgroundColor: data.inputData.background_color }}>
          <div className={styles.nodeHeader} style={{ display: "flex", justifyContent: "space-between" }}>
            <button
              className={styles.delete_button}
              name={nodeId}
              onClick={(e) => removeCard(e)}
            >
              <RiDeleteBin6Line/>
            </button>
            <input
              style={{ width: "10%" }}
              type="color"
              name="background_color"
              defaultValue="#e4e81e"
              onChange={(e) =>
                dispatch(
                  updateNode(e.target.name, e.currentTarget.value, nodeId)
                )
              }
            />
          </div>

          <table className={styles.table}>
            <tbody className={styles.yellow_tbody}>
              <tr className={styles.yellow_tr}>
                <th scope="row" className={styles.th}>
                  Title:
                </th>
                <td className={styles.td}>
                  <input
                    className={styles.text_input}
                    onChange={(e) =>
                      dispatch(
                        updateNode(e.target.name, e.currentTarget.value, nodeId)
                      )
                    }
                    value={findNodeValue("title", nodeId, elements)}
                    name="title"
                    type="text"
                  />
                </td>
              </tr>
              <tr className={styles.yellow_tr}>
                <th scope="row" className={styles.th}>
                  Description:
                </th>
                <td className={styles.td}>
                  <textarea
                    className={styles.textarea}
                    onChange={(e) =>
                      dispatch(
                        updateNode(e.target.name, e.currentTarget.value, nodeId)
                      )
                    }
                    name="description"
                    id=""
                    cols={9}
                    rows={4}
                    maxLength={100}
                    value={findNodeValue("description", nodeId, elements)}
                  ></textarea>
                </td>
              </tr>
              <tr className={styles.yellow_tr}>
                <th scope="row" className={styles.th}>
                  Status:
                </th>
                <td className={styles.td}>
                  <input
                    className={styles.text_input}
                    onChange={(e) =>
                      dispatch(
                        updateNode(e.target.name, e.currentTarget.value, nodeId)
                      )
                    }
                    name="status"
                    type="text"
                    value={findNodeValue("status", nodeId, elements)}
                  />
                </td>
              </tr>
              <tr className={styles.yellow_tr}>
                <th scope="row" className={styles.th}>
                  Assign to:
                </th>
                <td>
                  <div className={styles.multiSelect_container}>
                    <MultiSelect
                      className={styles.text_input}
                      options={userList}
                      value={selectedUser}
                      onChange={(value: any) => {
                        dispatch(updateNode("assign_to", value, nodeId));

                        setSelectedUser(value);
                      }}
                      labelledBy="Select"
                    />
                  </div>
                </td>
              </tr>
              <tr className={styles.yellow_tr}>
                <th scope="row" className={styles.th}>
                  Deadline:
                </th>
                <td className={styles.td}>
                  <input
                    className={styles.text_input}
                    onChange={(e) =>
                      dispatch(
                        updateNode(e.target.name, e.currentTarget.value, nodeId)
                      )
                    }
                    name="deadline"
                    type="datetime-local"
                    value={findNodeValue("deadline", nodeId, elements)}
                  />
                </td>
              </tr>
              <tr className={styles.yellow_tr}>
                <th scope="row" className={styles.th}>
                  Email reminder:
                </th>
                <td style={{ display: "flex", justifyContent: "center" }}>
                  <button
                    style={{
                      padding: "0",
                      border: "0",
                      textAlign: "center",
                      background: "transparent",
                    }}
                    onClick={() =>
                      setExpandEmailInput(
                        expandEmailInput === true ? false : true
                      )
                    }
                  >
                    <RiEditBoxLine
                      style={{ margin: "5", paddingTop: "4", fontSize: "20" }}
                    ></RiEditBoxLine>
                  </button>
                </td>
              </tr>
            </tbody>
          </table>
          <div className={styles.emailInput} hidden={expandEmailInput}>
            <table style={{ width: "100%" }}>
              <tbody className={styles.yellow_tbody}>
                <tr className={styles.yellow_tr}>
                  <th scope="row" className={styles.th}>
                    To:
                  </th>
                  <td>
                    <div className={styles.multiSelect_container}>
                      <MultiSelect
                        className={styles.text_input}
                        options={emailList}
                        value={selectedEmail}
                        onChange={(value: any) => {
                          dispatch(updateNode("to", value, nodeId));

                          setSelectedEmail(value);
                        }}
                        labelledBy="Select"
                      />
                    </div>
                  </td>
                </tr>
                <tr className={styles.yellow_tr}>
                  <th scope="row" className={styles.th}>
                    Subject:
                  </th>
                  <td>
                    <input
                      className={styles.text_input}
                      type="text"
                      name="subject"
                      onChange={(e) =>
                        dispatch(
                          updateNode(
                            e.target.name,
                            e.currentTarget.value,
                            nodeId
                          )
                        )
                      }
                      value={findNodeValue("subject", nodeId, elements)}
                    />
                  </td>
                </tr>
                <tr className={styles.yellow_tr}>
                  <th scope="row" className={styles.th}>
                    Content:
                  </th>
                  <td className={styles.td}>
                    <textarea
                      className={styles.textarea}
                      onChange={(e) =>
                        dispatch(
                          updateNode(
                            e.target.name,
                            e.currentTarget.value,
                            nodeId
                          )
                        )
                      }
                      name="content"
                      id=""
                      cols={14}
                      rows={14}
                      maxLength={100}
                      value={findNodeValue("content", nodeId, elements)}
                    ></textarea>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>

        <Handle
          className={styles.yellow_targetHandle}
          type="target"
          position={Position.Top}
          id="a"
          // style={{ background: "#555" }}
          isConnectable={isConnectable}
        />
        <Handle
          className={styles.yellow_sourceHandle}
          type="source"
          position={Position.Right}
          id="b"
          isConnectable={isConnectable}
        />
        <Handle
          className={styles.yellow_sourceHandle}
          type="source"
          position={Position.Bottom}
          id="c"
          isConnectable={isConnectable}
        />
        <Handle
          className={styles.yellow_sourceHandle}
          type="source"
          position={Position.Left}
          id="d"
          isConnectable={isConnectable}
        />
      </div>
    );
  }
);
