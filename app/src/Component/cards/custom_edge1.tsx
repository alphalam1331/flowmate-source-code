import React, { useEffect, useState } from "react";
import {
  getBezierPath,
  getEdgeCenter,
  getMarkerEnd,
  getSmoothStepPath,
  Position,
  WrapEdgeProps,
} from "react-flow-renderer";
import { RiCloseFill } from "react-icons/ri";
import { useDispatch, useSelector } from "react-redux";
import { clearPanelAction } from "../../redux/builderPanal/action";
import { removeElement, updateEdge } from "../../redux/dnd/action";
import { RootState } from "../../redux/state";
import { findEdgeValue } from "./getElementValue";

// interface GetSmoothStepPathParams {
//   sourceX: number;
//   sourceY: number;
//   sourcePosition?: Position;
//   targetX: number;
//   targetY: number;
//   targetPosition?: Position;
//   borderRadius?: number;
//   centerX?: number;
//   centerY?: number;
// }

export default function CustomEdge1({
  id,
  sourceX,
  sourceY,
  targetX,
  targetY,
  sourcePosition,
  targetPosition,
  style = {},
  data,
  arrowHeadType,
  markerEndId,
}: WrapEdgeProps) {
  const markerEnd = getMarkerEnd(arrowHeadType, markerEndId);
  const [edgeCenterX, edgeCenterY] = getEdgeCenter({
    sourceX,
    sourceY,
    targetX,
    targetY,
  });
  const dispatch = useDispatch();
  const elements = useSelector((state: RootState) => state.dnd);
  const edgeId = id;

  let centerY = edgeCenterY;
  let foreignObjectSize = 60;
  let foreignObjectSizeX = edgeCenterX - foreignObjectSize * 1.8;
  let foreignObjectSizeY = edgeCenterY - foreignObjectSize * 0.5;
  let finalPath = `M ${sourceX},${sourceY} L${targetX},${targetY}`;

  if (targetY < sourceY && sourcePosition === "right") {
    centerY = edgeCenterY;
    const moveRight = 100;
    const moveUp = -80;

    finalPath = `M ${sourceX},${sourceY} h ${moveRight} V ${
      targetY + moveUp
    } H${targetX} L ${targetX},${targetY}`;
    foreignObjectSizeY = centerY + moveUp;
    foreignObjectSizeX = sourceX + moveRight;
  } else if (targetY > sourceY && sourcePosition === "right") {
    const moveRight = 100;
    const moveUp = -80;
    finalPath = `M ${sourceX},${sourceY} h ${moveRight} V ${
      targetY + moveUp
    } H${targetX} L ${targetX},${targetY}`;
    foreignObjectSizeY = (sourceY + targetY) / 2 + moveUp;
    foreignObjectSizeX = sourceX + moveRight;
  }

  if (targetY < sourceY && sourcePosition === "left") {
    centerY = edgeCenterY;
    const moveLeft = -100;
    const moveUp = -80;
    finalPath = `M ${sourceX},${sourceY} h ${moveLeft} V ${
      targetY + moveUp
    } H${targetX} L ${targetX},${targetY}`;
    foreignObjectSizeY = centerY + moveUp;
    foreignObjectSizeX = sourceX + moveLeft * 2.85;
  } else if (targetY > sourceY && sourcePosition === "left") {
    const moveLeft = -100;
    const moveUp = -80;
    finalPath = `M ${sourceX},${sourceY} h ${moveLeft} V ${
      targetY + moveUp
    } H${targetX} L ${targetX},${targetY}`;
    foreignObjectSizeY = (sourceY + targetY) / 2 + moveUp;
    foreignObjectSizeX = sourceX + moveLeft * 2.85;
  }

  if (targetY < sourceY && targetX > sourceX && sourcePosition === "bottom") {
    centerY = edgeCenterY;
    const moveLeft = -100;
    const moveRight = 500;
    const moveUp = -50;
    const moveDown = 80;
    finalPath = `M ${sourceX},${sourceY} v ${moveDown} h ${moveRight} V ${
      targetY + moveUp
    } H${targetX} L ${targetX},${targetY}`;
    foreignObjectSizeY = centerY + moveUp;
    foreignObjectSizeX = sourceX + moveRight - 100;
  } else if (
    targetY < sourceY &&
    targetX < sourceX &&
    sourcePosition === "bottom"
  ) {
    centerY = edgeCenterY;
    const moveLeft = -500;
    const moveRight = 500;
    const moveUp = -50;
    const moveDown = 80;
    finalPath = `M ${sourceX},${sourceY} v ${moveDown} h ${moveLeft} V ${
      targetY + moveUp
    } H${targetX} L ${targetX},${targetY}`;
    foreignObjectSizeY = centerY + moveUp;
    foreignObjectSizeX = sourceX + moveLeft - 100;
  }
  const OnEdgeClick = (evt: React.MouseEvent<HTMLButtonElement>) => {
    evt.stopPropagation();

    dispatch(clearPanelAction());
    dispatch(removeElement(evt.currentTarget.name));
  };

  const edgePath = getBezierPath({
    sourceX,
    sourceY,
    sourcePosition,
    targetX,
    targetY,
    targetPosition,
    centerY,
  });

  const test = getSmoothStepPath({
    sourceX,
    sourceY,
    sourcePosition: Position.Top, // optional
    targetX,
    targetY,
    targetPosition: Position.Top, // optional
    // borderRadius, // optional
    // centerX , // optional
    centerX: 10,
    centerY, // optional
  });

  const [selection, setSelection] = useState("review");
  useEffect(() => {
    const element: any = elements.find((e) => e.id === edgeId);

    if (element && element.data[selection]) {
      const value = element.data[selection];
      dispatch(updateEdge(selection, value, edgeId));
    }
  }, [selection]);
  function actionSelection() {
    if (selection === "review") {
      return (
        <select
          style={{ width: "100%" }}
          name="review"
          value={findEdgeValue("review", edgeId, elements)}
          onChange={(e) =>
            dispatch(updateEdge(e.target.name, e.target.value, edgeId))
          }
        >
          <option value="approve">Approve</option>
          <option value="reject">Reject</option>
        </select>
      );
    } else if (selection === "upload") {
      return (
        <select
          name="upload"
          onChange={(e) =>
            dispatch(updateEdge(e.target.name, e.target.value, edgeId))
          }
          defaultValue="false"
        >
          <option value="true">Yes</option>
          {/* <option value="false">No</option> */}
        </select>
      );
    }
  }
  return (
    <>
      <path
        id={id}
        style={{ stroke: data.stroke_color, strokeWidth: style.strokeWidth }}
        className="react-flow__edge-path"
        d={finalPath}
        markerEnd={markerEnd}
      />
      <foreignObject
        width={"17%"}
        height={"20%"}
        style={{}}
        x={foreignObjectSizeX}
        y={foreignObjectSizeY}
        className="edgebutton-foreignobject"
        requiredExtensions="http://www.w3.org/1999/xhtml"
      >
        <div
          style={{
            color: "white",
            backgroundColor: data.stroke_color,
            zIndex: "10000",
          }}
        >
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <button
              style={{ padding: 0, width: "8%", height: "4%" }}
              className="edgebutton"
              name={id}
              onClick={(event) => OnEdgeClick(event)}
            >
              <RiCloseFill />
            </button>

            <input
              name="stroke_color"
              type="color"
              value={findEdgeValue("stroke_color", edgeId, elements)}
              onChange={(e) =>
                dispatch(
                  updateEdge(e.target.name, e.currentTarget.value, edgeId)
                )
              }
            />
          </div>

          <table style={{ width: "100%" }}>
            <tbody>
              <tr>
                <th>Action:</th>
                <td>
                  <div style={{ display: "flex", width: "100%" }}>
                    <div>
                      <select
                        name=""
                        id=""
                        onChange={(e) => setSelection(e.target.value)}
                      >
                        <option value="review">Review</option>
                        <option value="upload">Upload</option>
                      </select>
                    </div>

                    <div>{actionSelection()}</div>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </foreignObject>
    </>
  );
}
