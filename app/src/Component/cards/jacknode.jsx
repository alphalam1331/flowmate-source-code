import React, { memo, useState } from 'react';

import { Handle } from 'react-flow-renderer';



export default memo(({ data, isConnectable,style }) => {
  

  return (
    <div style={style}>
    
      <div>
      {data.label}
      </div>
    

      <Handle
        type="target"
        position="top"
        id="a"

        style={{ background: '#555'}}
        isConnectable={isConnectable}
      />
      <Handle
        type="source"
        position="bottom"
        id="b"
        style={{ background: '#555', }}
        isConnectable={isConnectable}
      />
      <Handle
        type="source"
        position="right"
        id="c"
        style={{ background: '#555', }}
        isConnectable={isConnectable}
      />
      <Handle
        type="source"
        position="left"
        id="d"
        style={{ background: '#555',}}
        isConnectable={isConnectable}
      />
    </div>
  );
});