import { Button, ButtonGroup } from "react-bootstrap";
import styles from "./ConfirmationPopUp.module.css";

function ConfirmationPopUp(props: {
    name:string;
  onConfirm: () => void;
  togglePopUp: () => void;
}) {
  return (
    <div className={styles.overlay}>
      <div className={styles.card}>
        <div className={styles.msg}>Do you want to activate {props.name}?</div>
        <ButtonGroup className={styles.options}>
          <Button
            className={styles.confirm}
            onClick={() => {
              props.onConfirm();
              props.togglePopUp();
            }}
          >
            Confirm
          </Button>
          <Button className={styles.cancel} onClick={props.togglePopUp}>
            Cancel
          </Button>
        </ButtonGroup>
      </div>
    </div>
  );
}

export default ConfirmationPopUp;
