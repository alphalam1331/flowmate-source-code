export default [
  {
    id: "1646464977584_1",
    type: "default",
    position: {
      x: 765,
      y: 300,
    },
    data: {
      inputData: {
        title: "1",
        description: "1",
        deadline: "N/A",
        status: "testing 1",
        assign_to: "6",
        dependencies: [],
        nextStage: [
          {
            connectedEdgeId: "1646464988719_3",
            targetId: "1646464984524_2",
            triggerEvent: {
              review: "approve",
              input: false,
            },
          },
        ],
      },
      label: {
          children: [
            {
              name: "1646464977584_1",
              children: "x",
            },
            {
              children: ["Title:", "1"],
            },
            {
              children: ["Description:", "1"],
            },
            {
              children: ["Assign to:", "6"],
            },
            {
              children: ["Status:", "testing 1"],
            },
            {
              children: ["Deadline:", "N/A"],
            },
          ],
        },
      },
    },
    // style: {
    //   color: "#333",
    //   border: "1px solid #222138",
    //   width: 280,
    // },
  // },
];
