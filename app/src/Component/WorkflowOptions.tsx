import { Link } from "react-router-dom";
import { post } from "../api";
import styles from "./WorkflowOptions.module.css";

function WorkflowOptions(props: {
  id: number;
  toggleMenu: () => void;
  togglePopUp: () => void;
}) {
  return (
    <div className={styles.menu}>
      <Link to={`/main/builder?wf=${props.id}`} className={styles.option}>
        View
      </Link>
      <div
        className={styles.option}
        onClick={() => {
          props.togglePopUp();
          props.toggleMenu();
        }}
      >
        Activate
      </div>
    </div>
  );
}

export default WorkflowOptions;
