import styles from "./DropDown.module.css";

function UserDropDown() {

  return (
    <div className={styles.user}>
      <div>Profile</div>
      <div>Setting</div>
      <div>About Us</div>
      <div className={styles.logOut}>Log Out</div>
    </div>
  );
}

export default UserDropDown;
