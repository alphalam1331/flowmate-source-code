import styles from "./PageSection.module.css";

type Props = {
  header?: boolean;
  headerText: string;
  children: any;
  style?: Object;
};

function PageSection({ headerText, children, style, header = true }: Props) {
  return (
    <section  style={style}>
      {header ? <div className={styles.pageHeader}>{headerText}</div> : ""}
      {children}
    </section>
  );
}

export default PageSection;
