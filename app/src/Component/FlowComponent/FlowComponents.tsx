import styles from './ShowFlowComponent.module.css'
import {BsChevronDoubleDown, BsChevronDoubleUp} from "react-icons/bs"
import {SiGmail} from "react-icons/si"
import{MdOutlineMarkEmailRead} from"react-icons/md"
import 'bootstrap/dist/css/bootstrap.css';
import { useContext, useState } from 'react';
import { Accordion, AccordionContext, useAccordionButton, Card, Dropdown, DropdownButton } from 'react-bootstrap';
import FlowActions from './FlowActions';


function ContextAwareToggle({ children, eventKey, callback }:any) {
    const { activeEventKey } = useContext(AccordionContext);
  
    const decoratedOnClick = useAccordionButton(
      eventKey,
      () => callback && callback(eventKey),
    );
  
    const isCurrentEventKey = activeEventKey === eventKey;
  
    return (
       
      <><BsChevronDoubleDown
        // className={styles.}
        type="button"
        style={{ display: isCurrentEventKey ? 'none' : 'block' }}
        onClick={decoratedOnClick}
      >
        {children}
      </BsChevronDoubleDown><BsChevronDoubleUp type="button"
        style={{ display: isCurrentEventKey ? 'block' : 'none' }}
        onClick={decoratedOnClick}>{children}
        </BsChevronDoubleUp></>
    
    );

  }
  function Example(props:{name:string, type:string}) {
    const nodeName = ["Read Email","Write Email","Store Email"]
    return (
      <>
      <Dropdown> 
        <Dropdown.Toggle id="dropdown-button-white-example1" variant=""style={{width:"300px",display:"flex",flexDirection:"row",justifyContent:"space-between"}}>{props.type=='email' && <SiGmail/>}<div>{props.name} : {props.type}</div>
        
        {/* <ContextAwareToggle eventKey="0"></ContextAwareToggle> */}
        </Dropdown.Toggle>
    
        <Dropdown.Menu variant="white" >
        {nodeName.map((num)=>(
        <FlowActions key={num} name={" "+num}/>
      ))}
          <Dropdown.Item href="#/action-1" >
       
          </Dropdown.Item>
          <Dropdown.Item href="#/action-2">Another action</Dropdown.Item>
          
        </Dropdown.Menu>
      </Dropdown>
    
      
    </>
      
    );
  }
  export default Example
//   <Dropdown style={{ height:'55px', width:'300px'}}>
//   <Dropdown.Toggle className={styles.nodeName}>{props.type=='email' && <SiGmail/>}<div>{props.name} : {props.type}</div>
//     <ContextAwareToggle eventKey="0"></ContextAwareToggle>
//   </Dropdown.Toggle>
  
//   <Dropdown.Menu>
//     <Dropdown.Item eventKey="0" className={styles.actionName}>
//       {nodeName.map((num)=>(
//         <FlowActions key={num} name={" "+num}/>
//       ))}
//    </Dropdown.Item>
//   </Dropdown.Menu>

// </Dropdown>

