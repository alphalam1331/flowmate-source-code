import { Col, Row } from "react-bootstrap"
import { MdOutlineMarkEmailRead } from "react-icons/md"
import styles from "./ShowFlowComponent.module.css"
function FlowActions(props: {name:string}){
    return (
    <Row >
    <Col><MdOutlineMarkEmailRead /></Col> 
    <Col><div className={styles.actionName}>{props.name}</div></Col>
   </Row>)
}

export default FlowActions
