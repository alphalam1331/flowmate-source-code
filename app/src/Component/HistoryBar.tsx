import { useState } from "react";
import Col from "react-bootstrap/esm/Col";
import Row from "react-bootstrap/esm/Row";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import {
  History,
  InstanceJSONData,
  TaskJSONData,
} from "../redux/workflows/state";
import {
  getAllRunningInstancesByUserIDThunk,
  updateInstanceThunk,
} from "../redux/workflows/thunk";
import { formatDate } from "../tools";
import styles from "./HistoryBar.module.css";

type props = {
  history: History;
  instanceJSONData: InstanceJSONData;
  onUpdate: (updated: boolean) => void;
};

function HistoryBar({ history, instanceJSONData, onUpdate }: props) {
  const {
    id,
    workflow_instance_id,
    task_id,
    task,
    status,
    start_at,
    action,
    completed_at,
    assign_to,
  } = history;

  const dispatch = useDispatch();
  const jwtToken = useSelector((state: RootState) => state.auth.user?.token);
  const userID = useSelector((state: RootState) => state.auth.user?.payload.id);

  // console.log ({id})
  const [taskJSONData, setTaskJSONData] = useState<TaskJSONData>(() => {
    // console.log ({instanceJSONData})
    const taskJSON = instanceJSONData.filter(
      (taskJSON) => taskJSON.id === task_id
    );
    return taskJSON[0];
  });

  const { nextStage } = taskJSONData;

  const actionSet = new Set(
    nextStage.map((stage) => {
      for (let key in stage.triggerEvent) {
        switch (key) {
          case "upload":
            return key;
          case "review":
            return stage.triggerEvent[key];
        }
      }
    })
  );

  const handleUpdate = async (msg: string) => {
    if (jwtToken && userID)
      await dispatch(
        updateInstanceThunk(jwtToken, userID, workflow_instance_id, id, msg)
      );
    dispatch(getAllRunningInstancesByUserIDThunk(userID!));
  };

  const actionList = Array.from(actionSet);

  return (
    <li
      style={{ display: "inline" }}
      className={styles.enableFocus}
      id={`${id}`}
    >
      {completed_at ? (
        <Row className={styles.bar}>
          <Col xs={3}>
            {task} : {status}
          </Col>
          <Col xs={2} className={styles.small}>
            <div>
              Started at: <div>{formatDate(start_at)}</div>
            </div>
          </Col>
          <Col xs={2} className={styles.small}>
            <div>
              Completed at: <div>{formatDate(completed_at)}</div>
            </div>
          </Col>
          <Col xs={4} className={styles.small}>
            {action ? (
              <div>
                Action:
                <div>
                  {action.toLocaleUpperCase()}
                  {assign_to ? (assign_to.length > 0 ? ': '+assign_to.join(', ') : "") : ""}
                </div>
              </div>
            ) : (
              ""
            )}
          </Col>
        </Row>
      ) : (
        <Row className={styles.bar}>
          <Col xs={3}>
            {task} : {status}
          </Col>
          <Col xs={2} className={styles.small}>
            
            <div>
              Started at: <div>{formatDate(start_at)}</div>
            </div>
          </Col>
          <Col xs={2} className={styles.small}>
          {assign_to ? (
              assign_to.length > 0 ? (
                <div>
                  Assignees:<div>{assign_to?.join(", ")}</div>
                </div>
              ) : (
                ""
              )
            ) : (
              ""
            )}  
            </Col>

          <Col xs={4} className={styles.actionContainer}>
            {actionList.map((action) => (
              <button
                key={actionList.indexOf(action)}
                onClick={() => {
                  handleUpdate(`${action}`);
                  onUpdate(true);
                }}
              >
                {action?.toLocaleUpperCase()}
              </button>
            ))}
          </Col>
        </Row>
      )}
    </li>
  );
}

export default HistoryBar;
