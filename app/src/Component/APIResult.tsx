import { APIResultType } from '../redux/auth/state'

export function APIResult(props: { result: APIResultType }) {
  const { result } = props
  return (
    <>
  
      {result.type !== 'fail' ? null : (
        <div style={{justifyContent: 'center'}}>
        
            {result.message}
        
        </div>
      )}
    </>
  )
}