// import { useState,useRef, useEffect  } from 'react';
// import styles from './Flow.module.css'
// import {Navigation} from 'react-minimal-side-navigation';
// import ColorSelectorNode from './cards/jacknode'
// import { SiGmail } from "react-icons/si";
// import{IoIosNotificationsOutline} from "react-icons/io";
// import{AiOutlineControl} from "react-icons/ai"
// import {IoNotifications}from "react-icons/io5";
// import {AiFillControl}from "react-icons/ai";
// import {SiMicrosoftteams}from "react-icons/si";
// import ReactFlow, {
//   removeElements,
//   addEdge,
//   MiniMap,
//   Controls,
//   Background,
//   ReactFlowProvider,
// } from 'react-flow-renderer';

// import initialElements from './initialElements';
// import sendEmailCard from './cards/sendEmailCard'
// let id = 0;
// const getId = () => `dndnode_${id++}`;


// const Flow = () => {
//   const [elements, setElements] = useState(initialElements);
//   const onElementsRemove = (elementsToRemove) =>
//     setElements((els) => removeElements(elementsToRemove, els)); 
//   const onConnect = (params) => setElements((els) => addEdge(params, els));
//   console.log('elements:', elements);
//   const reactFlowWrapper = useRef(null);
//   const [reactFlowInstance, setReactFlowInstance] = useState(null);
//   const [nodeInputData, setNodeInputData] = useState("");
//   console.log('jacNodeInput',)

//   const onLoad = (reactFlowInstance) => {
//     console.log('flow loaded:', reactFlowInstance);
//     reactFlowInstance.fitView();
//     setReactFlowInstance(reactFlowInstance);
//   };

//   const onDragOver = (event) => {
//     event.preventDefault();
//     event.dataTransfer.dropEffect = 'move';
//   };

//   const checkID = (event) => {
//     // console.log("check id: ",event.target.id,elements, event)
//     console.log("check id event: ", event.target.value)
//     setNodeInputData(event.target.value)
//     setElements((els) =>
//       els.map((el) => {
//         if (el.id === event.target.id) {
//           // it's important that you create a new object here
//           // in order to notify react flow about the change
//           el.inputData = event.target.value
//         }

//         return el;
//       })
//     );


//   }
//   useEffect(() => {
//     console.log('Useffect element: ', elements)
//     console.log('Useffect nodeinputdata: ', nodeInputData)
//   }, [nodeInputData])

//   const nodeTypes = {
//     selectorNode: ColorSelectorNode,
//     sendEmail: sendEmailCard
//   };

//   function sideBar() {
//     const onDragStart = (event, nodeType) => {
//       event.dataTransfer.setData('application/reactflow', nodeType);
//       event.dataTransfer.effectAllowed = 'move';
//     };
//     return (
//       <aside>
//         <div className="description">You can drag these nodes to the pane on the right.</div>
//         <div className={styles.dndnode_input} onDragStart={(event) => onDragStart(event, 'input')} draggable>
//           Input Node
//         </div>
//         <div className={styles.dndnode_default} onDragStart={(event) => onDragStart(event, 'default')} draggable>
//           Default Node
//         </div>
//         <div className={styles.dndnode_ouput} onDragStart={(event) => onDragStart(event, 'output')} draggable>
//           Output Node
//         </div>
//         <>
//           <div className={styles.dndnode_custom} onDragStart={(event) => onDragStart(event, 'selectorNode')} draggable>
//             Receive Email
//           </div>
//           <div className={styles.dndnode_send} onDragStart={(event) => onDragStart(event, 'sendEmail')} draggable>
//             Send Email
//           </div>
//         </>
//       </aside>
//     );
//   };

//   const onDrop = (event) => {
//     event.preventDefault();

//     const reactFlowBounds = reactFlowWrapper.current.getBoundingClientRect();
//     const type = event.dataTransfer.getData('application/reactflow');
//     console.log('onDrop->event.dataTransfer', event.dataTransfer, type)
//     const position = reactFlowInstance.project({
//       x: event.clientX - reactFlowBounds.left,
//       y: event.clientY - reactFlowBounds.top,
//     });
//     const id = getId()
//     switch (type) {
//       case 'input':
//         setElements((es) => es.concat(
//           {
//             id: id,
//             type: type,
//             position,
//             data: {
//               label: <>
//                  This is a input card{getId()}
//                 <input id={id} type="text" onChange={checkID} />
//               </>
//             },
//             style: {
            
//               color: '#333',
//               border: '1px solid #222138',
//               width: 280,
//             },
//             inputData: ""
//           }
//         ));
//         break;
//       case 'default':
//         setElements((es) => es.concat(
//           {
//             id: id,
//             type: type,
//             position,
//             data: {
//               label: <>
//                 This is a default card{getId()}
//                 <input id={id} type="text" onChange={checkID} />
//               </>
//             },
//             style: {
            
//               color: '#333',
//               border: '1px solid #222138',
//               width: 280,
//             },
//             inputData: ""
//           }
//         ));
//         break
//       case 'output':
//         setElements((es) => es.concat(
//           {
//             id: id,
//             type: type,
//             position,
//             data: {
//               label: <>
//                 This is a output card{getId()}
//                 <input id={id} type="text" onChange={checkID} />
//               </>
//             },
//             style: {
            
//               color: '#333',
//               border: '1px solid #222138',
//               width: 280,
//             },
//             inputData: ""
//           }
//         ));
//         break;
//         case 'selectorNode':
//           setElements((es) => es.concat(
//             {
//               id: id,
//               type: type,
//               position,
//               data: {
//                 label: <div >
//                   Receive Email {getId()}
//                   <div>
//                   <div style={{display:'flex',justifyContent:'space-between', margin:'1px'}}>
//                     <div>From:</div>
//                     <input id={id} style={{border: '0.2px solid #222138'}} type="text" name='sender' onChange={checkID}/>
//                   </div>
//                   </div>
                  
//                 </div>
//               },
//               style: {
//                 padding:'2px',
//                background: 'white',
//                 border: '1px solid #ffd900',
//                 width: 280,
//               },
//               inputData: ""
//             }
//           ));
//           break;
//           case 'sendEmail':
//             setElements((es) => es.concat(
//               {
//                 id: id,
//                 type: type,
//                 position,
//                 data: {
//                   label: <div >
//                     Send Email {getId()}
//                     <div>
//                     <div style={{display:'flex',justifyContent:'space-between', margin:'1px'}}>
//                       <div>To:</div>
//                       <input id={id} style={{border: '0.2px solid #222138'}} type="text" name='receiver' onChange={checkID}/>
//                     </div>
//                     <div style={{display:'flex',justifyContent:'space-between', margin:'1px'}}>
//                       <div>Title:</div>
//                       <input id={id} type="text" style={{border: '0.2px solid #222138'}} name='title' onChange={checkID}/>
//                     </div>
//                     <div style={{display:'flex',justifyContent:'space-between', margin:'1px'}}>
//                       <div>Body:</div>
//                       <textarea name="" id="" cols="19" rows="3"></textarea>
//                     </div>
//                     </div>
                    
//                   </div>
//                 },
//                 style: {
//                   padding:'2px',
//                  background: 'white',
//                   border: '1px solid #00ff00',
//                   width: 280,
//                 },
//                 inputData: ""
//               }
//             ));
//             break;
//       default:
//         setElements((es) => es.concat(
//           {
//             id: id,
//             type: type,
//             position,
//             data: {
//               label: <>
//                 This is Error
//               </>
//             },
//             style: {
            
//               color: '#333',
//               border: '1px solid #222138',
//               width: 280,
//             },
//             inputData: ""
//           }
//         ));
//     }
//     // const newNode = {
//     //   id: id,
//     //   type: type,
//     //   position,
//     //   data: {
//     //     label: <>
//     //       Send Email id{getId()}
//     //       <input id={id} type="text" onChange={checkID} />
//     //     </>
//     //   },
//     //   style: {
      
//     //     color: '#333',
//     //     border: '1px solid #222138',
//     //     width: 280,
//     //   },
//     //   inputData: ""
//     // };

//     // setElements((es) => es.concat(newNode));
//   };

//   return (
//     <ReactFlowProvider>
//   <div className="reactflow-wrapper" ref={reactFlowWrapper} className={styles.chart}>
//     <ReactFlow
//       elements={elements}
//       onElementsRemove={onElementsRemove}
//       onConnect={onConnect}
//       onLoad={onLoad}
//       snapToGrid={true}
//       snapGrid={[15, 15]}
//       onDrop={onDrop}
//       onDragOver={onDragOver}
//       nodeTypes={nodeTypes}
//       elementsSelectable={true}
//     >
//       <MiniMap
//         nodeStrokeColor={(n) => {
//           if (n.style?.background) return n.style.background;
//           if (n.type === 'input') return '#0041d0';
//           if (n.type === 'output') return '#ff0072';
//           if (n.type === 'default') return '#1a192b';

//           return '#eee';
//         }}
//         nodeColor={(n) => {
//           if (n.style?.background) return n.style.background;

//           return '#fff';
//         }}
//         nodeBorderRadius={2}
//       />
      
//       <Controls />
//       <Background color="#aaa" gap={16} />
      
//     </ReactFlow>
    
//     </div>
//     <div className={styles.navigation}> 
//     <Navigation 
//     // you can use your own router's api to get pathname
//     activeItemId="/Gmail/Notifications/Control/Teams"
//     onSelect={({itemId}) => {
//       // maybe push to the route
//     }}
//     items={[
//       {
//         title: 'Gmail',
//         itemId: '/Gmail',
//         elemBefore: () => <SiGmail/>,
//         subNav: [
         
//           {
//             title: 'Send Email',
//             itemId: '/Gmail/Send Email',
//           },
//           {title:sideBar()},
//           {
//             title: 'Store Email',
//             itemId: '/Gmail/Store Email',
//           },
        
//         ],
//       },
      
//       {
//         title: 'Notifications',
//         itemId: '/Notifications',
//         elemBefore: () => <IoNotifications/>,
//         subNav: [
//           {
//             title: 'Send me an email notification',
//             itemId: '/Notifications/Send me an email notification',
//           },
//         ],
//       },{
//         title: 'Control',
//         itemId: '/Control',
//         elemBefore: () => <AiFillControl/>,
//         subNav: [
//           {
//             title: 'Condition',
//             itemId: '/Control/Condition',
//           },  
//         ],
//       },
//       {
//         title: 'Teams',
//         itemId: '/Teams',
//         elemBefore: () => <SiMicrosoftteams/>,
//         subNav: [
//           {
//             title: 'Create a Teams meeting',
//             itemId: '/Teams/Create a Teams meeting',
//           },  
//         ],
//       },
//     ]}
//   />
//     </div>
    
//     </ReactFlowProvider>
//   );
// };

// export default Flow;