import styles from "./ProgressBox.module.css";

function ProgressBox(props: { title: string, current:boolean}) {
  return (
    <div className={styles.box}>
      <div className={props.current?styles.current: styles.notCurrent}>{props.title}</div>
    </div>
  );
}

export default ProgressBox;
