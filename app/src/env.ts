
function check(key: string) {
  const value = process.env[key]
  if (!value) {
    throw new Error(`no ${key}`)
  }
  return value
}

export default {
  REACT_APP_API_SERVER: check('REACT_APP_API_SERVER')
}