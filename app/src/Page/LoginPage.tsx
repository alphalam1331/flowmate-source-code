import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { APIResult } from "../Component/APIResult";
import { loginWithPasswordThunk } from "../redux/auth/thunk";
import { RootState } from "../redux/state";
import styles from "./Login.module.css";

const LoginPage: React.FC = () => {
  const result = useSelector((state: RootState) => state.auth.loginResult);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [submitted, setSubmitted] = useState(false);

  // const [valid, setValid] = useState(
  //   user.username ==0,
  // )
  //  function validMsg(e:{target:{

  //  }})
  const [user, setUser] = useState({
    username: "",
    password: "",
  });

  function handleChange(e: { target: { name: any; value: any } }) {
    const { name, value } = e.target;
    setUser((user) => ({ ...user, [name]: value }));
  }

  async function handleSubmit() {
    setSubmitted(true);
    if (user.username && user.password) {
      dispatch(loginWithPasswordThunk(user));
    }
  }
  // dispatch(setLoginResultAction({ type: 'success', token: "abc" }))
  useEffect(() => {
    console.log ({result})
    if (result.type === "success") navigate("/main");
  }, [result]);


  return (
    <div className={styles.formContainer}>
        <div className={styles.header}>Log In</div>
      <form
        className={styles.formInputContainer}
        onSubmit={(e) => {
          e.preventDefault();
          handleSubmit();
        }}
      >

        <div className={styles.formGroup}>
          <div className={styles.inputLabel}>User Name</div>
          <input
            type="text"
            name="username"
            value={user.username}
            onChange={handleChange}
            className={
              (submitted && !user.username ? " is-invalid" : "")
            }
          />
          {submitted && !user.username && (
            <div className="invalid-feedback">User Name is required</div>
          )}
        </div>
        <div className={styles.formGroup}>
          <div className={styles.inputLabel}>Password</div>
          <input
            type="password"
            name="password"
            value={user.password}
            onChange={handleChange}
            className={
              styles.formInput +
              "form-control" +
              (submitted && !user.password ? " is-invalid" : "")
            }
          />
          {submitted && !user.password && (
            <div className="invalid-feedback">Password is required</div>
          )}
        </div>
        <div
          className="form-group"
          style={{
            padding: "4px",
            display: "flex",
            justifyContent: "center",
          }}
        >
          {/* <div className="custom-control custom-checkbox">
              <input type="checkbox" className="custom-control-input" id="customCheck1" />
              <label className="custom-control-label" htmlFor="customCheck1">Remember me</label>
          </div> */}

          <button type="submit" className={styles.submitButton}>
            Submit
          </button>
        </div>
        {/* <button type="button" className="btn btn-success btn-block">Login via Google</button> <button type="button" className="btn btn-light btn-Light btn-outline"><Link to="/Home">Go to Home Page</Link></button>
      <p className="forgot-password text-right">
          Forgot <a href="#">password?</a>
      </p> */}
        <APIResult result={result} />
      </form>
    </div>
  );
};
export default LoginPage;
