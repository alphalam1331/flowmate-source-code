import "react-minimal-side-navigation/lib/ReactMinimalSideNavigation.css";
import Flow2 from "../Component/Flow2";
import { useSelector } from "react-redux";
import { RootState } from "../redux/state";
import styles from "./Builder.module.css";

function Builder() {
  const isSuperAdmin = useSelector(
    (state: RootState) => state.auth.user?.payload.superAdmin
  );

  return (
    <>
      {isSuperAdmin ? (
        <div className={styles.builderContainer}>
          <div className={styles.flowContainer}>
            <Flow2></Flow2>
          </div>
        </div>
      ) : (
        <div className={styles.msg}>
          <div>Creating workflow can only be performed by Super Admin.</div>
        </div>
      )}
    </>
  );
}

export default Builder;
