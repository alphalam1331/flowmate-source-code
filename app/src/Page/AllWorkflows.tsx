import "../App.css";
import styles from "./AllWorkflows.module.css";
import PageSection from "../Component/PageSection";
import WorkflowBar from "../Component/workflowBar";
import { Col, Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../redux/state";
import { useEffect } from "react";
import { getAllWorkflowsByUserIDThunk } from "../redux/workflows/thunk";
import { WorkflowItem } from "../redux/workflows/state";
import { RiAddLine } from "react-icons/ri";
import { Link } from "react-router-dom";

function AllWorkflows() {
  const dispatch = useDispatch();

  const isSuperAdmin = useSelector(
    (state: RootState) => state.auth.user?.payload.superAdmin
  );
  const userID = useSelector((state: RootState) => state.auth.user?.payload.id);
  let workflows = useSelector(
    (state: RootState) => state.workflows.workflowList
  );

  useEffect(() => {
    if (isSuperAdmin && userID) dispatch(getAllWorkflowsByUserIDThunk(userID));
  }, [isSuperAdmin, userID, dispatch]);

  // let arr = Array.from(Array(20).keys());//

  return (
    <>
      <PageSection headerText="Your Workflows">
        <div className={styles.titleCol}>
          <Row>
            <Col xs={3}>Workflows</Col>
            <Col xs={3}>Created By</Col>
            <Col xs={3}>Updated By</Col>
            <Col xs={2}>Running Instances</Col>
          </Row>
        </div>
        <hr></hr>
        {isSuperAdmin ? (
          workflows.length > 0 ? (
            <ul className={styles.scrollDiv}>
              {workflows.map((workflow: WorkflowItem) => (
                <WorkflowBar
                  key={workflow.id}
                  id={workflow.id}
                  name={workflow.name}
                  creator={workflow.creator}
                  created_at={workflow.created_at}
                  updater={workflow.updater}
                  updated_at={workflow.updated_at}
                  running_instances={workflow.running_instances}
                />
              ))}
            </ul>
          ) : (
            <div className={styles.msg}>
              <div>Create Your Workflow</div>
              <div>
                <Link to="/main/builder">
                  <RiAddLine className={styles.msgBtn} />
                </Link>
              </div>
            </div>
          )
        ) : (
          <div className={styles.msg}>
          <div>Workflows are only accessed by Super Admin.</div>
        </div>
        )}

        {/* <div className={styles.moreBtn}>
          <RiMoreFill />
        </div> */}
      </PageSection>
    </>
  );
}

export default AllWorkflows;
