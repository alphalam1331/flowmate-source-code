import { useEffect, useMemo, useState } from "react";
import { Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import PageSection from "../Component/PageSection";
import { setEditProfileThunk, setLoadProfileThunk } from "../redux/auth/thunk";
import { RootState } from "../redux/state";
import { getAllUsersInfoThunk } from "../redux/usersInfo/thunk";
import styles from "./Profile.module.css";

function Profile() {
  // const userLogin =  useSelector((state:RootState)=>(state.auth.user))
  const userInfo = useSelector((state: RootState) => state.auth.userInfo);
  const userList = useSelector((state: RootState) =>
    state.usersInfo.userList.filter(
      (user) => user.username !== userInfo?.username
    )
  );
  // console.log(userInfo);
  const dispatch = useDispatch();
  const [updateInfo, setUpdateInfo] = useState({
    username: userInfo?.username,
    title: userInfo?.title,
    name: userInfo?.name,
    email: userInfo?.email,
    password: userInfo?.hashed_password,
  });
  const [isEdit, setIsEdit] = useState(false);
  const [submitted, setSubmitted] = useState(false);

  console.log("info2", updateInfo);
  function handleChange(e: { currentTarget: { name: any; value: any } }) {
    const { name, value } = e.currentTarget;
    console.log(isEdit);
    console.log({ name }, { value });
    console.log("updateinfo1", updateInfo);
    // console.log("testing",e.currentTarget)
    // const newInfo = {...updateInfo, name:value}
    // console.log(newInfo)
    // setUpdateInfo({ ...updateInfo, updateInfo[name]: value })
    setUpdateInfo({ ...updateInfo, [name]: value });

    console.log(setUpdateInfo);
  }
  function handleSubmit() {
    setSubmitted(true);
    console.log("updateInfo", updateInfo);
    dispatch(setEditProfileThunk(updateInfo));
  }

  useEffect(() => {
    dispatch(setLoadProfileThunk());
    dispatch(getAllUsersInfoThunk());
  }, []);

  useEffect(() => {
    setUpdateInfo({
      username: userInfo!.username,
      title: userInfo!.title,
      name: userInfo!.name,
      email: userInfo!.email,
      password: userInfo!.hashed_password,
    });
  }, [userInfo]);

  const [uploadAvatar, setUploadAvatar] = useState();
  // useEffect(()=>{
  //   dispatch(setEditProfileThunk(user))

  // },[]);

  return (
    <>
      <Row className={styles.profileCardContainer}>
        <form className={styles.card}>
          <div className={styles.userInfo}>
            <label className={styles.userLabel}>User Name:</label>
            <input
              value={isEdit === true ? updateInfo.username : userInfo!.username}
              onChange={(e) => handleChange(e)}
              name="username"
            ></input>
          </div>
          <div className={styles.userInfo}>
            <label className={styles.userLabel}>Change Password:</label>
            <input
            placeholder="Change Password"
            type={'password'}
              value={
                isEdit === true
                  ? updateInfo.password
                  : userInfo!.hashed_password
              }
              onChange={(e) => handleChange(e)}
              name="password"
            ></input>
          </div>
          <div className={styles.userInfo}>
            <label className={styles.userLabel}> Name: </label>
            <input
              value={isEdit === true ? updateInfo.name : updateInfo.name}
              onChange={(e) => handleChange(e)}
              name="name"
            ></input>
          </div>
          <div className={styles.userInfo}>
            <label className={styles.userLabel}>Email: </label>
            <input
              value={isEdit === true ? updateInfo.email : userInfo!.email}
              onChange={(e) => handleChange(e)}
              name="email"
            ></input>
          </div>

          <div className={styles.buttonContainer}>
            <button
              className={styles.submitButton}
              onClick={(e) => {
                e.preventDefault();
                isEdit === true ? setIsEdit(false) : setIsEdit(true);
              }}
            >
              {isEdit === true ? "Cancel" : "Edit Profile"}
            </button>
            <button
              className={styles.submitButton}
              type="submit"
              onClick={(e) => {
                e.preventDefault();
                handleSubmit();
              }}
            >
              Save Change
            </button>
          </div>
          <div className="EditButton" >
            {/* <div style={{
                    padding:"4px",
                    display:"flex",
                    justifyContent: 'center'}}>
          <button className={styles.submitButton} 
           onClick={ (e)=> {
             e.preventDefault();
             isEdit===true ? setIsEdit(false) : setIsEdit(true) }}>
            {isEdit ===true ? "Cancel" : "Edit Profile" }
                    </button></div>
                    
                    </div>
                    <div className="SaveButton" >
            <div style={{
                    padding:"4px",
                    display:"flex",
                    justifyContent: 'center'}}>
          <button className={styles.submitButton+submitted} type="submit"
          onClick={ (e)=> {
             e.preventDefault()
             handleSubmit()}}>
                        Save Change
                    </button></div> */}
                    
                    </div>
       
      </form>
  
      </Row>
      <PageSection header={true} headerText="Your Colleagues" style={{position: "absolute",bottom:" 10%",marginLeft:"30px"}} >
        <ul className={styles.colleaguesBar}>
          {userList.map((user, idx) => (
            <ColleagueCard
              key={idx}
              name={user.name}
              path={`//joeschmoe.io/api/v1/${user.id}`}
            />
          ))}
        </ul>
      </PageSection>
    </>
  );
}

export default Profile;

function ColleagueCard(props: { name: string; path: string }) {
  return (
    <>
      <li className={styles.colleaguesCard}>
        <img className={styles.avatar} src={props.path} />
        <div className={styles.colleagueName}>{props.name}</div>
      </li>
    </>
  );
}
