import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { APIResult } from "../Component/APIResult";
import { registerThunk } from "../redux/auth/thunk";
import { RootState } from "../redux/state";
import { useNavigate } from "react-router";
import styles from "./SignUp.module.css";
const SignUpPage: React.FC = () => {
  const result = useSelector((state: RootState) => state.auth.registerResult);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  
  if (result.type === "success") navigate("/main");

  const [user, setUser] = useState({
    username: "",
    title: "Mr.",
    name: "",
    superAdmin: 0,
    email: "",
    password: "",
    confirmPassword: "",
  });
  const [submitted, setSubmitted] = useState(false);
  function handleChange(e: { target: { name: any; value: any } }) {
    const { name, value } = e.target;
    setUser((user) => ({ ...user, [name]: value }));
  }
  function handleSubmit() {
    setSubmitted(true);
  //   let upperCase = /(?=.*?[A-Z])/
  //   let lowerCase = /(?=.*?[a-z])/
  //   if ( !upperCase.test(user.password)){
  //     return  "Password must contain Upper Case" 
  //   }
  //   if (!lowerCase.test(user.password)) {
  //     return "Password must contain Lower Case"
  //   }
  //   if (user.password.length<5) {
  //   return "Password length must be at least 5 characters"
  //   }
  //   if (user.password.length<8) {
  //     return "Password length must within 8 characters"
  // }
  // else if
  if
    (
      user.username &&
      user.email &&
      user.title &&
      user.password &&
      user.confirmPassword &&
      user.password == user.confirmPassword
    ) {
      dispatch(registerThunk(user));
    }
  }
  useEffect(() => {
    if (result.type === "success") navigate("/main");
  }, [result]);

  return (
    <div className={styles.formContainer}>
      <div className={styles.header}>Sign Up</div>
      <form
        className={styles.formInputContainer}
        onSubmit={(e) => {
          e.preventDefault();
          handleSubmit();
        }}
      >
        <div className={styles.formGroup}>
          <div className={styles.inputLabel}>User Name</div>
          <input
            type="text"
            name="username"
            value={user.username}
            onChange={handleChange}
            className={
              styles.formInput +
              "form-control" +
              (submitted && !user.username ? " is-invalid" : "")
            }
          />
          {submitted && !user.username && (
            <div className="invalid-feedback">User Name is required</div>
          )}
        </div>
        <div className={styles.formGroup}>
          <div className={styles.inputLabel}>Title</div>
          <select
            defaultValue="Mr."
            name="title"
            value={user.title}
            onChange={handleChange}
            className={
              styles.formInput +
              "form-control" +
              (submitted && !user.title ? " is-invalid" : "")
            }
          >
            <option value="Mr.">Mr.</option>
            <option value="Mrs.">Mrs.</option>
          </select>
          {submitted && !user.title && (
            <div className="invalid-feedback">Title is required</div>
          )}
        </div>
        <div className={styles.formGroup}>
          <div className={styles.inputLabel}>Name</div>
          <input
            type="text"
            name="name"
            value={user.name}
            onChange={handleChange}
            className={
              styles.formInput +
              "form-control" +
              (submitted && !user.name ? " is-invalid" : "")
            }
          />
          {submitted && !user.name && (
            <div className="invalid-feedback">Name is required</div>
          )}
        </div>
        {/* <div className={styles.formGroup}>
          <div className={styles.inputLabel}>Super Admin</div>
          <input
            // style={{ width:"5px", height:"5px",borderRadius:"5px"}}
            type="checkbox"
            name="superAdmin"
            checked={user.superAdmin ? true : false}
            value={user.superAdmin ? 1 : 0}
            onChange={handleChange}
            className={
              "form-control" +
              (submitted && !user.superAdmin ? " is-invalid" : "")
            }
          />
        </div> */}
        <div className={styles.formGroup}>
          <div className={styles.inputLabel}>Email address</div>
          <input
            type="email"
            name="email"
            value={user.email}
            onChange={handleChange}
            className={
              styles.formInput +
              "form-control" +
              (submitted && !user.email ? " is-invalid" : "")
            }
          />
          {submitted && !user.email && (
            <div className="invalid-feedback">Email address is required</div>
          )}
        </div>
        <div className={styles.formGroup}>
          <div className={styles.inputLabel}>Password</div>
          <input
            type="password"
            name="password"
            value={user.password}
            onChange={handleChange}
            className={
              styles.formInput +
              "form-control" +
              (submitted && !user.password ? " is-invalid" : "")
            }
          />
          {submitted && !user.password && (
            <div className="invalid-feedback">Password is required</div>
          )}
        </div>
        <div className={styles.formGroup}>
          <div className={styles.inputLabel}>Confirm Password</div>
          <input
            type="password"
            name="confirmPassword"
            value={user.confirmPassword}
            onChange={handleChange}
            className={
              styles.formInput +
              "form-control" +
              (submitted && user.password !== user.confirmPassword
                ? " is-invalid"
                : "")
            }
          />
          {submitted && user.password !== user.confirmPassword && (
            <div className="invalid-feedback">Password not matched</div>
          )}
        </div>
        <div
          className="form-group"
          style={{
            padding: "4px",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <button className={styles.submitButton}>
            {/* {result && <span className="spinner-border spinner-border-sm mr-1"></span>} */}
            Submit
          </button>
        </div>

        <APIResult result={result} />
      </form>
    </div>
  );
};
export default SignUpPage;
