// import { ToastContainer } from "react-toastify";
import { Col, Row } from "react-bootstrap";
import styles from "./Instances.module.css";
import PageSection from "../Component/PageSection";
// import SearchBar from "../Component/SearchBar";
import InstanceBar from "../Component/InstanceBar";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getAllRunningInstancesByUserIDThunk } from "../redux/workflows/thunk";
import { RootState } from "../redux/state";
import { Link } from "react-router-dom";
import { VscCompassActive } from "react-icons/vsc";

function Instances() {
  const dispatch = useDispatch();
  const instances = useSelector(
    (state: RootState) => state.workflows.instanceList
  );
  const userId = useSelector((state: RootState) => state.auth.user?.payload.id);
  let numOfCompletedTasks;

  useEffect(() => {
    if (userId) dispatch(getAllRunningInstancesByUserIDThunk(userId));
    numOfCompletedTasks = instances.filter(
      (instance) => instance.is_completed
    ).length;
  }, [userId, dispatch]);


  return (
    <>
      {/* <ToastContainer /> */}

      <PageSection header={true} headerText="Running Instances">
        <div className={styles.titleCol}>
          <Row>
            <Col xs={3}>
              Instances{" "}
              <span>
                ({numOfCompletedTasks ? numOfCompletedTasks : "0"}/
                {instances.length})
              </span>
            </Col>
            <Col xs={2}>Activated By</Col>
            <Col xs={5}>Current Task</Col>
          </Row>
        </div>
        <hr></hr>
        {instances.length > 0 ? (
          <ul className={styles.instancesContainer}>
            {instances.map((instance) => (
              <InstanceBar
                key={instance.id}
                id={instance.id}
                name={instance.name}
                created_at={instance.created_at}
                updated_at={instance.updated_at}
                initiator={instance.initiator}
                is_completed={instance.is_completed}
              />
            ))}
          </ul>
        ) : (
          <div className={styles.msg}>
            <div>Activate your Workflow</div>
            <div>
              <Link to="/main/">
                <VscCompassActive className={styles.msgBtn} />
              </Link>
            </div>
          </div>
        )}
      </PageSection>
    </>
  );
}

export default Instances;
