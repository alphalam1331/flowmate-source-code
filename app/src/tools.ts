import { format } from "date-fns";

export function formatDate(date: any) {
  // console.log ('before', date)
  date = new Date(date);
  // console.log ('Date:', date)
  let formattedDate = format(date, "do LLL, yyyy - HH:mm:ss");
  String(formattedDate);
  // console.log ('after formatted', formattedDate)
  return formattedDate;
}