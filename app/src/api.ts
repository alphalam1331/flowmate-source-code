import env from "./env";

const { REACT_APP_API_SERVER } = env;

async function handleResponse(resP: Promise<Response>) {
  try {
    let res = await resP;
    let json = await res.json();
    return json;
  } catch (error) {
    return { error: String(error) };
  }
}

export async function getFetch(url: string) {
  let res = fetch(REACT_APP_API_SERVER + url);
  return handleResponse(res);
}

export async function getFetchWithToken(url: string) {
  let res = fetch(REACT_APP_API_SERVER + url, {
    headers: {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    },
  });
  return handleResponse(res);
}

export async function getFetchUserInfo(url:string){
  let res = fetch(REACT_APP_API_SERVER + url,{
    method: 'GET',
    headers:{
        "Authorization":`Bearer ${localStorage.getItem('token')}`
    }
  })
      return handleResponse(res)
    }
    


export function post(url: string, body?: any) {
  let resP = fetch(REACT_APP_API_SERVER + url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  });
  return handleResponse(resP);
}

export async function postWithToken(
  url: string,
  body?: any
) {
const token = localStorage.getItem("token")

  if (!token) {
    return { error: "This API is not available to guest" };
  }
  let resP = fetch(REACT_APP_API_SERVER + url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    },
    body: JSON.stringify(body),
  });
  return handleResponse(resP);
}
