// import 'bootstrap/dist/css/bootstrap.min.css'
import styles from "./Trail.module.css";
// import ShowLandingModal from "./Component/ShowLandingModal";
// import ShowSignUpModal from "./Component/ShowSignUpModal";
import React, { MouseEventHandler, useState } from "react";
import SignUpPage from "./Page/SignUpPage";
import LoginPage from "./Page/LoginPage";

function Trail() {
  const [showLogIn, setShowLogIn] = useState(false);
  const [showSignUp, setShowSignUp] = useState(false);

  return (
    <div className={styles.container}>
      <button
        className={styles.logIn}
        onClick={() => {
          setShowLogIn(!showLogIn);
          if (showSignUp) setShowSignUp(!showSignUp);
        }}
      >
        Log In
      </button>
      <button
        className={styles.signUp}
        onClick={() => {
          if (showLogIn) setShowSignUp(!showLogIn);
          setShowSignUp(!showSignUp);
        }}
      >
        Sign Up
      </button>

      {showLogIn ? (
        <div
          className={styles.formContainer}
          onClick={(e: React.MouseEvent<Element>) => {
            e.stopPropagation();
            if (
              (e.target as Element).classList.contains(styles.formContainer)
            ) {
              setShowLogIn(false);
              setShowSignUp(false);
            }
          }}
        >
          <LoginPage />{" "}
        </div>
      ) : (
        ""
      )}
      {showSignUp ? (
        <div
          className={styles.formContainer}
          onClick={(e: React.MouseEvent<HTMLDivElement>) => {
            e.stopPropagation();
            if (
              (e.target as Element).classList.contains(styles.formContainer)
            ) {
              setShowLogIn(false);
              setShowSignUp(false);
            }
          }}
        >
          <SignUpPage />
        </div>
      ) : (
        ""
      )}

      {/* <ShowLandingModal></ShowLandingModal>
        <ShowSignUpModal></ShowSignUpModal> */}

      {/* <div className="button-container"> */}
      {/* <Button className="Login-button" onClick={handleshowLogIn}>
       Login
      </Button> */}

      {/* <Modal show={showLogIn} onHide={handleshowLogIn}>
        <Modal.Header closeButton>
          <Modal.Title>Login</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <LoginPage/>
        </Modal.Body>
      </Modal> */}

      {/* <Button className="SignUp-button" onClick={handleShowSignUp}>
      Sign Up
      </Button> */}
      {/* <Modal show={showSignUp} onHide={handleShowSignUp}>
        <Modal.Header closeButton>
          <Modal.Title>Sign Up</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <SignUpPage/>
  </Modal.Body>
      </Modal> */}
      {/* </div> */}
    </div>
  );
}

export default Trail;
