set -e -x

root="$PWD"
server="flowmate-server"
server_root="~/flowmate-server"
image_name="flowmate-server"
image_tag="production"
image="$image_name:$image_tag"

cd "$root/flowmate-server"

yarn install
# yarn test 
docker build . -t "$image"

cd "$root"
docker save "$image" \
  | zstd -c - \
  | ssh $server "unzstd - | docker load"

scp docker.env "$server:$server_root"
scp docker-compose.yml "$server:$server_root"
ssh "$server" "cd $server_root && docker-compose up -d"


