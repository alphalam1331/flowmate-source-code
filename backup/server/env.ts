import { config } from 'dotenv'

config()
function check(key: string) {
    const value = process.env[key]
    if (!value) {
        throw new Error(`no ${key}`)
    }
    return value
}

export default {
    // host: check('HOST'),
    port: check('PORT'),
    database: {
        host: check('POSTGRES_HOST'),
        port: +check('POSTGRES_PORT'),
        database: check('POSTGRES_DB'),
        user: check('POSTGRES_USER'),
        password: check('POSTGRES_PASSWORD'),
    },
    nodeEnv: check('NODE_ENV'),
    jwtSecret: check('JWT_SECRET')
}
