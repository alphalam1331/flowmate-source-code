import { UserService } from "./userSevice";
import { WorkflowService } from "./workflowService";
import { FileSystemService } from "./fileSystemService";

import { knex } from './knex'

export const userService = new UserService(knex)
export const fileSystemService = new FileSystemService()
export const workflowService = new WorkflowService(knex, fileSystemService, userService)