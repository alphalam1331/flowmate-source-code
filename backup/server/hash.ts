import { hash, compare } from 'bcryptjs'

const ROUND = 12

export async function hashPassword(password: string) {
    const hashed_password = await hash(password, ROUND)
    return hashed_password
}

export async function comparePassword(pair: { password: string, hashed_password: string }) {
    const isMatch = await compare(pair.password, pair.hashed_password)
    return isMatch
}