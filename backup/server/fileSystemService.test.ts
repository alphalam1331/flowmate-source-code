import {FileSystemService} from './fileSystemService'


describe ('testing file service', ()=>{
    let fileService: FileSystemService
    
    beforeAll(()=>{
        fileService= new FileSystemService()
    })

    test('save, read workflow ', ()=>{
        const content = '{"cate": "workflow"}'
        const filename = fileService.saveWorkFlow(content)
        const fileContent = fileService.readWorkFlow(filename)
        expect(fileContent).toBe(content)
    })

    // test('save, read template', ()=>{
    //     const content = '{"cate": "template"}'
    //     const filename = fileService.saveTemplate(content)
    //     // const fileContent = fileService.readTemplate(filename)
    //     expect(fileContent).toBe(content)
    // })

    test.only('read instance 1 file detail', ()=>{
        fileService.readInstanceDetail('1646737848423A1MhIix3bXLE4I0ky6PgZu3BFVGbpc48+.json')
    })
})