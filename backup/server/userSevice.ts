import { Knex } from 'knex';
import { comparePassword, hashPassword } from './hash';
import { createJWT } from './jwt';
import { NewUserForm, LoginForm } from './model';
import { HTTPError } from './apiHelper'

export class UserService {
  constructor(private knex: Knex) { }

  table = () => { return this.knex('users') }

  createUser = async (newUserForm: NewUserForm) => {
    const { username, title, name, password, email, superAdmin } = newUserForm
    let row = await this.table().select().where({ username })
    if (row.length > 0) {
      throw new HTTPError('Failed to insert new user: Username already exists.', 400);
    }
    
    row = await this.table().select().where({ email })
    if (row.length > 0) {
      throw new HTTPError('Failed to insert new user: Email already exists.', 400);
    }

    row = await this.table().insert({
      username
      , title
      , name
      , hashed_password: await hashPassword(password)
      , email
      , super_admin: superAdmin
    }).returning('id')
    const id = row[0].id

    const JWToken = createJWT({ id, username, superAdmin })
    console.log('user service: JWT: ', JWToken)
    return JWToken
  }

  logIn = async (loginForm: LoginForm) => {
    const { username, password } = loginForm
    const row = (await this.table().select().where({ username: username }))[0]
    if (!row) {
      throw new HTTPError(`${username} does not exist.`, 400)
    }

    const isMatch = comparePassword({
      password: password,
      hashed_password: row.hashed_password
    })
    if (!isMatch) {
      throw new HTTPError(`Wrong password.`, 400)
    }

    const jwt = createJWT({ id: row.id, username, superAdmin:row.super_admin })
    return jwt
  }

  getUsersInfo = async (userID:any)=>{
    let users;
    if (userID === 0) {
      users = await this.table().select('id', 'username', 'title', 'name', 'email')
    } else {
      users = await this.table().select('id', 'username', 'title', 'name', 'email').where({id:userID})
    }
    return users
  }

  checkSuperAdminByUserID = async (userID: number) => {
    const row = (await this.table().select('super_admin').where({id:userID}))[0]
    const superAdminRight = row['super_admin']
    return superAdminRight
  }




}