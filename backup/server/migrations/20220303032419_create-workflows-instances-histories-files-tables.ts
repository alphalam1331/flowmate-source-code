import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTableIfNotExists('workflow_instances', (t) => {
        t.increments('id')
        t.integer('workflow_id').notNullable().references('workflows.id')
        t.string('name', 255).notNullable()
        t.integer('initiator_id').notNullable().references('users.id')
        t.timestamp('created_at').defaultTo(knex.fn.now(0));
        t.string('filename', 255).unique().notNullable()
        t.timestamp('updated_at').defaultTo(knex.fn.now(0));
        t.boolean('is_completed').defaultTo(false);
    })

    
    await knex.schema.createTableIfNotExists('workflow_histories', (t) => {
        t.increments('id')
        t.integer('workflow_instance_id').notNullable().references('workflow_instances.id')
        t.string('task_id').notNullable()
        t.string('task', 255).notNullable()
        t.string('status', 255).notNullable()
        t.string('description', 255).notNullable()
        t.timestamp('start_at').defaultTo(knex.fn.now(0));
        t.integer('action_id').references('actions.id')
        t.timestamp('completed_at');

    })
    await knex.schema.createTableIfNotExists('pic', (t) => {
        t.increments('id')
        t.integer('user_id').references('users.id')
        t.integer('workflow_instance_id').references('workflow_instances.id')
    })

}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTableIfExists('pic')
    await knex.schema.dropTableIfExists('workflow_histories')
    await knex.schema.dropTableIfExists('workflow_instances')
}

