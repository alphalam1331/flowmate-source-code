import Knex from 'knex'

const knexConfigs = require('./knexfile');
const knexConfig = knexConfigs['test']

export const knex = Knex(knexConfig)