import { join } from "path";
import { readFileSync, writeFileSync } from "fs";
import { FileClasses, InstanceJSONData } from "./model";
import { HTTPError } from "./apiHelper";

export class FileSystemService {
  private instanceDirectory = join(".", "asset", "workflow_instances");
  private workflowsDirectory = join(".", "asset", "workflows");
  // private templatesDirectory = join(".", "asset", "templates");

  constructor() {}

  saveInstance = (jsonData: string) => {
    const randomFilename = this.toSave(
      this.instanceDirectory,
      jsonData,
      ".json"
    );
    return randomFilename;
  };
  saveWorkFlow = (jsonData: string) => {
    const randomFilename = this.toSave(
      this.workflowsDirectory,
      jsonData,
      ".json"
    );
    return randomFilename;
  };


  toSave = (directory: string, data: string, ext: string) => {
    const randomFilename = `${this.genRandomFilename()}+${ext}`;
    const path = join(directory, randomFilename);
    try {
      writeFileSync(path, data);
    } catch (error) {
      throw new HTTPError(`Failed to write file. Error: ${error}`, 500);
    }
    return randomFilename;
  };

  readInstanceDetail = (instance_filename: string, id?: number):InstanceJSONData => {
    let jsonData: Array<any> = this.readInstance(instance_filename);
    let taskList = []

    for(let item of jsonData){
      if(item.data.inputData){
        const task = item.data.inputData
        task.id = item.id
        taskList.push(task)
      }
    }
    taskList.filter((task) => task !== null);
    console.log(taskList);
    return taskList;
  };

  readInstance = (instance_filename: string) => {
    const jsonData = this.toRead("instance", instance_filename);
    return jsonData;
  };

  readWorkFlow = (workflow_filename: string) => {
    const jsonData = this.toRead("workflow", workflow_filename);
    return jsonData;
  };


  toRead = (fileClass: FileClasses, filename: string) => {
    const path = this.getFullPath(fileClass, filename);
    const fileContent = readFileSync(path, "utf-8");
    return JSON.parse(fileContent);
  };

  getFullPath = (fileClass: FileClasses, filename: string) => {
    let directory;
    switch (fileClass) {
      case "instance":
        directory = this.instanceDirectory;
        break;
      case "workflow":
        directory = this.workflowsDirectory;
        break;

    }
    const path = join(directory, filename);
    return path;
  };

  private genRandomFilename = (): string => {
    const p1 = String(Date.now());
    let chars =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    let name = p1;
    console.log("initial length: ", name.length);
    while (name.length < 45) {
      name += chars[Math.floor(Math.random() * chars.length - 1)];
    }
    return name;
  };
}
