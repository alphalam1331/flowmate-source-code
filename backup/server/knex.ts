import Knex from 'knex'

const knexConfigs = require('./knexfile');
const knexConfig = knexConfigs['development']

export const knex = Knex(knexConfig)