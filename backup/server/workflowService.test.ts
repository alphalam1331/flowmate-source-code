import { FileSystemService } from "./fileSystemService";
import { knex } from "./knex";
import { UserService } from "./userSevice";
import { WorkflowService } from "./workflowService";

describe("testing workflow service", () => {
  let workflowService: WorkflowService;
  let fileSystemService: FileSystemService;
  let userService: UserService;
  beforeAll(() => {
    fileSystemService = new FileSystemService();
    userService = new UserService(knex);
    workflowService = new WorkflowService(knex, fileSystemService, userService);
  });
  afterAll(() => {
    knex.destroy();
  });

  describe("create workflow", () => {
    test("result: successful", async () => {
      let workflowID = await workflowService.createWorkflow({
        name: "jest testing create workflow",
        userID: 1,
        jsonData: `{
                    "compilerOptions": {
                        "strict": true,
                        "outDir": "dist",
                        "module": "commonjs",
                        "target": "es5",
                        "lib": [
                            "es6",
                            "dom"
                        ],
                        "sourceMap": true,
                        "allowJs": true,
                        "jsx": "react",
                        "esModuleInterop": true,
                        "moduleResolution": "node",
                        "noImplicitReturns": true,
                        "noImplicitThis": true,
                        "noImplicitAny": true,
                        "strictNullChecks": true,
                        "suppressImplicitAnyIndexErrors": true,
                        "noUnusedLocals": true
                    },
                    "exclude": [
                        "node_modules",
                        "build",
                        "scripts",
                        "index.js"
                    ],
                    "files": [
                        "server.ts"
                    ],
                }`,
      });
      expect(workflowID).toBeDefined();
      expect(workflowID).toBeGreaterThan(5);
    });
  });

  describe("load Workflows and Instances", () => {
    test(` userID: 2 has workflows: 1, 2`, async () => {
      const workflows = await workflowService.getAllWorkflows(2);
      console.log(workflows);
      let workflowsID = [...workflows.map((workflow) => workflow.id)];
      expect(workflowsID).toEqual(expect.arrayContaining([1, 2]));
    });
  });

  describe("edit workflow", () => {
    test("userID: 4 should be rejected in editing workflowID: 4 since he is not super admin", async () => {
      await expect(
        workflowService.editWorkflow(4, 4, '{"content": "come reject me"}')
      ).rejects.toThrow(
        "Unauthorized Action: Editing Workflow can only be performed by super admin and authorised user."
      );
    });
    test("userID: 1 should successfully edit workflowID: 5", async () => {
      let name = await workflowService.editWorkflow(
        1,
        5,
        '{"content": "You can\'t stop me"}'
      );
      expect(name).toBe("testing5");
    });
  });

  describe("activate workflow", () => {
    test("userID: 4 should be rejected in activating workflowID since he is not super admin", async () => {
      let id = await expect(
        workflowService.activateWorkflow(4, 2)
      ).rejects.toThrow(
        "Unauthorized Action: Activating Workflow can only be performed by super admin and authorised user."
      );

      expect(id).toBeUndefined();
    });
    test("userID: 1 should successfully activate workflowID: 1", async () => {
      const id = await workflowService.activateWorkflow(1, 1);
      expect(id).toBeGreaterThanOrEqual(6);
    });
  });
  describe("update workflow", () => {
    test.only("userID: 1 should successfully update workflowID: 1", async () => {
      // for (let i = 0; i < 5; i++)
        await workflowService.updateInstance(1, 7, 27, "approve");
      // expect(id).toBeGreaterThanOrEqual(6)
    });
  });
});
