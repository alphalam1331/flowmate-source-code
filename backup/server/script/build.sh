set -e -x


if [ $# == 1 ]; 
then
    tag = $1
else
    read -p "tag: " tag
fi

docker build ./ -t flowmate:$tag