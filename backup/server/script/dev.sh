set -e -x

./script/build.sh

docker stop flowmate-dev || true
docker rm flowmate-dev || true

docker run -it --name flowmate-dev -p 8100:8100 flowmate:dev
