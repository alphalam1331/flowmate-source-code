import express, {Request,Response} from 'express'
import env from './env'
import { WorkflowController } from './workflowController'
import { UserController } from './userController'
import { userService, workflowService } from './services'

import cors from 'cors'
let app = express()

const userController = new UserController(userService)
const workflowController = new WorkflowController(workflowService)

app.use(cors())
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

app.use(userController.router)
app.use(workflowController.router)

app.get('/', (req,res) => {
  res.end('hello world from express inside docker with postgres')
})


app.post('/save', (req: Request, res: Response) => {
  let time = Date.now()
  console.log(time, req.body[0].data.label.props)
  res.send({ some:req.body})
})

let port = env.port

app.listen(port, () => {
  console.log('listening on port', port)
})
