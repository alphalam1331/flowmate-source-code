import { Router, Request } from "express";
import { WorkflowService } from "./workflowService";
import { routeHandlerWrapper } from "./apiHelper";

export class WorkflowController {
  router = Router();
  constructor(private workflowService: WorkflowService) {
    this.router.get(
      "/workflow/json/:workflowID",
      routeHandlerWrapper(this.getWorkFlowJSONDataByID)
    );
    this.router.get(
      "/workflow/all/:userID",
      routeHandlerWrapper(this.getAllWorkflows)
    );
    this.router.get(
      "/instance/:userID",
      routeHandlerWrapper(this.getAllRunningInstancesByUserID)
    );
    this.router.get(
      "/instance/json/:instanceID",
      routeHandlerWrapper(this.getInstanceDetailJSONDataByID)
    );
    this.router.get(
      "/history/:instanceID",
      routeHandlerWrapper(this.getHistoriesByInstanceID)
    );
    this.router.post(
      "/instance/update",
      routeHandlerWrapper(this.updateInstance)
    );
    this.router.post(
      "/workflow/activate",
      routeHandlerWrapper(this.activateWorkflow)
    );
    this.router.post(
      "/workflow/create",
      routeHandlerWrapper(this.createWorkflows)
    );
    this.router.post("/workflow/edit", routeHandlerWrapper(this.editWorkflows));
  }

  getAllWorkflows = async (req: Request) => {
    const { userID } = req.params;
    const workflows = await this.workflowService.getAllWorkflows(+userID);

    return workflows;
  };

  getWorkFlowJSONDataByID = async (req: Request) => {
    const { workflowID } = req.params;
    let jsonData = await this.workflowService.getWorkFlowJSONDataByID(
      +workflowID
    );
    return jsonData;
  };

  getInstanceDetailJSONDataByID = async (req: Request) => {
    const { instanceID } = req.params;
    let jsonData = await this.workflowService.getInstanceDetailJSONDataByID(
      +instanceID
    );
    return jsonData;
  };

  getHistoriesByInstanceID = async (req: Request) => {
    const { instanceID } = req.params;
    let histories = await this.workflowService.getHistoriesByInstanceID(
      +instanceID
    );
    return histories;
  };

  createWorkflows = async (req: Request) => {
    const { userID, name, jsonData } = req.body;

    console.log({ userID });
    console.log({ name });
    console.log({ jsonData });
    const workflowID = await this.workflowService.createWorkflow({
      userID,
      name,
      jsonData,
    });
    //TODO: grant others right according to the front-end data
    return workflowID;
  };

  editWorkflows = async (req: Request) => {
    const { userID, workflowID, jsonData } = req.body;
    const name = await this.workflowService.editWorkflow(
      workflowID,
      userID,
      jsonData
    );
    return name;
  };

  activateWorkflow = async (req: Request) => {
    const { userID, workflowID } = req.body;
    console.log({ userID }, { workflowID });
    const instanceID = await this.workflowService.activateWorkflow(
      +userID,
      +workflowID
    );
    return instanceID;
  };

  updateInstance = async (req: Request) => {
    const { userID, workflowInstanceID, historyID, msg } = req.body;
    const result = await this.workflowService.updateInstance( userID, workflowInstanceID, historyID, msg );
    return result
  };

  getAllRunningInstancesByUserID = async (req: Request) => {
    const { userID } = req.params;
    const workflowInstances =
      await this.workflowService.getAllRunningInstancesByUserID(+userID);
    return workflowInstances;
  };
}
