import type { Knex } from "knex";
import env from "./env";
import test_env from "./test_env";
// Update with your config settings.

const config: { [key: string]: Knex.Config } = {
  development: {
    debug: true,
    client: "postgresql",
    connection: {
      database: env.database.database,
      user: env.database.user,
      password: env.database.password
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    },
  },
  test: {
    debug: true,
    client: "postgresql",
    connection: {
      database: test_env.database.testDatabase,
      user: test_env.database.testUser,
      password: test_env.database.testPassword
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    },
  },

  production: {
    client: "postgresql",
    connection: {
      database: env.database.database,
      user: env.database.user,
      password: env.database.password
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: "knex_migrations"
    }
  }

};

module.exports = config;
