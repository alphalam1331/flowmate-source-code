import { UserInfo } from "./model";
import { knex } from "./test_knex";
import { UserService } from "./userSevice";

describe('testing user services', () => {
    let userService: UserService;
    let rand_num: number;
    beforeAll(() => {
        userService = new UserService(knex)
        rand_num = Math.floor(Math.random() * 100000) + 1
    })
    afterAll(() => {
        knex.destroy()
    })

    it('should be defined', async () => {
        expect(userService.createUser).toBeDefined()
    })

    it('should be a function', async () => {
        expect(typeof userService.createUser).toBe("function")
    })

    describe('createUser', () => {
        test('createUser: Successful ', async () => {
            const token = await userService.createUser({
                username: `flowmate_superadmin${rand_num}`,
                title: `Mr.`,
                name: `Suen`,
                password: 'flowmate_superadmin_password',
                email: `superadmin${rand_num}@flowmate.com`,
                superAdmin: false
            })
            console.log(token)
            expect(token).toBeDefined()
        })

        test('createUser: duplicate user ', async () => {
            await expect(userService.createUser({
                username: `flowmate_superadmin${rand_num}`,
                title: `Mr.`,
                name: `Suen`,
                password: 'flowmate_superadmin_password',
                email: `superadmin${rand_num}@flowmate.com`,
                superAdmin: false
            })
            ).rejects.toThrow('Failed to insert new user: Username already exists.')
        })
    })

    describe('Login', () => {
        test('login: Successful ', async () => {
            const token = await userService.logIn(
                {
                    username: `flowmate_superadmin${rand_num}`,
                    password: 'flowmate_superadmin_password',
                }
            )
            console.log(token)
            expect(token).toBeDefined()
        })

        test('login: no such user', async () => {
            await expect(userService.logIn({
                username: 'soulmate_admin',
                password: 'soulmate_admin'
            })
            ).rejects.toThrow(`soulmate_admin does not exist.`)
        })
    })

    describe('getRole', () => {
        test('successully get the role of userID 1: super admin', async () => {
            const role = await userService.checkSuperAdminByUserID(1)
            expect(role).toBeTruthy()
        })
        test('failed to get the role of userID 2: super admin', async () => {
            const role = await userService.checkSuperAdminByUserID(2)
            expect(role).toBeFalsy()
        })
        test('failed to get the role of userID 3: super admin', async () => {
            const role = await userService.checkSuperAdminByUserID(3)
            expect(role).toBeFalsy()
        })
        test('failed to get the role of userID 41: super admin', async () => {
            const role = await userService.checkSuperAdminByUserID(4)
            expect(role).toBeFalsy()
        })
        test('failed to get the role of userID 5: super admin', async () => {
            const role = await userService.checkSuperAdminByUserID(5)
            expect(role).toBeFalsy()
        })
    })

    describe('get user(s) info', ()=>{
        test('get all users info', async ()=>{
            const usersList = await userService.getUsersInfo(0)
            let i:number=0;
            usersList.map((user:UserInfo)=>i++)
            expect(i).toBeGreaterThan(6)
        })
        test('get userID 4 info', async ()=>{
            const user = await userService.getUsersInfo(4)
            expect(user[0].username).toBe('123_Admin3')
        })
    })
})