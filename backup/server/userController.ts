import { Router, Request } from "express";
import { routeHandlerWrapper } from "./apiHelper";
import { getJWTPayload } from "./jwt";
import { NewUserForm, LoginForm } from "./model";
import { UserService } from "./userSevice";

export class UserController {
  router = Router();
  constructor(private userService: UserService) {
    this.router.get("/user", routeHandlerWrapper(this.getUsersInfo));
    this.router.post("/user/register", routeHandlerWrapper(this.register));
    this.router.post("/user/login", routeHandlerWrapper(this.logIn));
  }
  private register = async (req: Request) => {
    console.log(req.body);
    const newUserForm: NewUserForm = req.body;
    const jwt = await this.userService.createUser(newUserForm);
    return {token:jwt}
  };

  private logIn = async (req: Request) => {
    console.log("received login form");
    const loginForm: LoginForm = req.body;
    const jwt = await this.userService.logIn(loginForm);
    return {token:jwt};
  };
  private getUsersInfo = async (req: Request) => {
    let id: number;
    const { all } = req.query;
    if (all) {
      id = 0;
    } else {
      const payload = getJWTPayload(req);
      id = payload.id;
    }
    const result = await this.userService.getUsersInfo(id);
    return result;
  };
}
