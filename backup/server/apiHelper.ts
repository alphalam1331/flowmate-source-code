import { Request, Response } from "express"

export class HTTPError extends Error {
    constructor(public message: string, public status: number) {
        super()
    }
}

export function routeHandlerWrapper(routeHandler: (req: Request) => any) {
    return async (req: Request, res: Response) => {
        try {
            const result = await routeHandler(req)
            res.json(result)
        } catch (error) {
            if (error instanceof HTTPError) {
                res.status(error.status).json({ error: error.message })
            } else {
                res.status(500).json({ error: String(error) })
            }

        }
    }
}