import { knex } from "./test_knex";
import { UserService } from "./userSevice";
import {UserController}from'./userController'

describe('testing user controller', ()=>{
    let userService: UserService;
    let userController:UserController
    beforeAll(()=>{
        userService = new UserService(knex)
        userController= new UserController(userService)
    })

    it('should have been defined', ()=>{
        expect(userController.router).toBeDefined()
    })
    
})
