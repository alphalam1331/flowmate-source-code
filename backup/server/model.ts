const SECOND = 1000;
const MINUTE = 60 * SECOND;
const HOUR = 60 * MINUTE;
export const DAY = 24 * HOUR;

export type NewUserForm = {
  username: string;
  title: string;
  name: string;
  password: string;
  email: string;
  superAdmin: boolean;
};

export type LoginForm = {
  username: string;
  password: string;
};

export type JWTPayload = {
  id: number;
  username: string;
  superAdmin: boolean;
  exp_at: number;
};

export type UserInfo = {
  id: number;
  username: string;
  title: string;
  name: string;
  email?: string;
};

export type NewWorkflow = {
  userID: number;
  name: string;
  jsonData: string;
};

export type Workflow = {
  id: number;
  path: string;
  jsonData: string;
};

export type InstanceTask = {
  id?: number;
  workflowInstanceID?: number;
  taskID?: string;
  task?: string;
  status?: string;
  description?: string;
  actionID?: number;
};

export type FileClasses = "workflow" | "instance";

export type InstanceJSONData = Array<{
  id: string;
  title: string;
  description: string;
  deadline: string;
  status: string;
  assign_to: Array<number>;
  dependencies: Array<any>;
  nextStage: Array<{
    targetId: string;
    triggerEvent: { review: string; upload: string | boolean };
  }>;
}>;
