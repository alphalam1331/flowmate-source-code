set -e -x

sudo apt-get update
sudo apt-get upgrade

sudo apt --yes install docker.io
sudo apt --yes install zstd
sudo apt --yes install gzip
sudo apt --yes install docker-compose

sudo usermod -aG docker ubuntu