set -e -x
# stop and remove container
docker stop flowmate-server-production || true
docker rm flowmate-server-production || true

# remove image
docker rmi flowmate-server:1.0.1 || true
docker rmi flowmate-server:production|| true
docker rmi postgres:14|| true