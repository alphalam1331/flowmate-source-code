set -e -x

# stop and remove container
docker stop flowmate-server-production || true
docker rm flowmate-server-production || true

# remove image
docker rmi flowmate-server || true

#rebuild image
./scripts/docker-build.sh

#run the lastest image
docker run --env-file .env --name flowmate-server-production -p 8100:8100 flowmate-server:1.0.1 
