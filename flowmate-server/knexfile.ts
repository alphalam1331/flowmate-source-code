import env from './env';
import test_env from './src/test_env';
// Update with your config settings.

export default {
  development: {
    debug: true,
    client: 'postgresql',
    connection: {
      database: env.database.database,
      user: env.database.user,
      password: env.database.password,
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: 'src/database/migrations',
    },
    seeds: {
      directory: 'src/database/seeds',
    }
  },

  test: {
    debug: true,
    client: 'postgresql',
    connection: {
      database: test_env.database.testDatabase,
      user: test_env.database.testUser,
      password: test_env.database.testPassword,
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: 'src/database/migrations',
    },
    seeds: {
      directory: 'src/database/seeds',
    }
  },

  production: {
    client: 'postgresql',
    connection: {
      host: env.database.host,
      port: env.database.port,
      database: env.database.database,
      user: env.database.user,
      password: env.database.password,
    },
    pool: {
      min: 2,
      max: 10,
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: 'src/database/migrations',
    },
    seeds: {
      directory: 'src/database/seeds',
    }
  },
};
