import { config } from 'dotenv'

config()
function check(key: string) {
    const value = process.env[key]
    if (!value) {
        throw new Error(`no ${key}`)
    }
    return value
}

export default {
    database:{
        testDatabase: check('TEST_POSTGRES_DB'),
        testUser: check('TEST_POSTGRES_USER'),
        testPassword: check('TEST_POSTGRES_PASSWORD')
    }
}