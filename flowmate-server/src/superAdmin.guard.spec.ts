import { SuperAdminGuard } from './superAdmin.guard';

describe('RolesGuard', () => {
  it('should be defined', () => {
    expect(new SuperAdminGuard()).toBeDefined();
  });
});
