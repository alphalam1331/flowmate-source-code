import { Knex } from 'knex';
import { hashPassword } from '../../hash';

export async function seed(knex: Knex): Promise<void> {
  // Deletes ALL existing entries

  await knex('pic').del();
  await knex('workflow_histories').del();
  await knex('workflow_instances').del();
  await knex('workflows').del();
  await knex('users').del();
  await knex('actions').del();

  // Inserts seed entries
  await knex('actions').insert([
    { name: 'approve' },
    { name: 'reject' },
    { name: 'upload' },
  ]);

  await knex('users').insert([
    {
      username: 'Super_Admin',
      title: 'Mr.',
      name: 'Super_Admin',
      email: 'superadmin@123.com',
      hashed_password: await hashPassword('123'),
      super_admin: true,
    },

    {
      username: '123_Admin1',
      title: 'Mr.',
      name: '123_Admin',
      email: 'admin1@123.com',
      hashed_password: await hashPassword('123'),
    },
    {
      username: '123_Admin2',
      title: 'Mrs.',
      name: '123_Admin2',
      email: 'admin2@123.com',
      hashed_password: await hashPassword('123'),
    },
    {
      username: '123_Admin3',
      title: 'Mr.',
      name: '123_Admin3',
      email: 'admin3@123.com',
      hashed_password: await hashPassword('123'),
    },
    {
      username: '123_Operator1',
      title: 'Mr.',
      name: '123_Operator1',
      email: 'operator1@123.com',
      hashed_password: await hashPassword('123'),
    },
    {
      username: '123_Operator2',
      title: 'Mr.',
      name: '123_Operator2',
      email: 'operator2@123.com',
      hashed_password: await hashPassword('123'),
    },
    {
      username: '123_Operator3',
      title: 'Mrs.',
      name: '123_Operator3',
      email: 'operator3@123.com',
      hashed_password: await hashPassword('123'),
    },
    {
      username: '123_Operator4',
      title: 'Mr.',
      name: '123_Operator4',
      email: 'operator4@123.com',
      hashed_password: await hashPassword('123'),
    },
    {
      username: '123_Operator5',
      title: 'Mrs.',
      name: '123_Operator5',
      email: 'operator5@123.com',
      hashed_password: await hashPassword('123'),
    },
    {
      username: '123_Operator6',
      title: 'Mr.',
      name: '123_Operator6',
      email: 'operator6@123.com',
      hashed_password: await hashPassword('123'),
    },
  ]);
  // await knex("workflows").insert([
  //   { name: "testing1", filename: "testing1.json", created_by: 1 },
  //   { name: "testing2", filename: "testing2.json", created_by: 1 },
  //   { name: "testing3", filename: "testing3.json", created_by: 1 },
  //   { name: "testing4", filename: "testing4.json", created_by: 1 },
  //   { name: "testing5", filename: "testing5.json", created_by: 1 },
  //   { name: "testing6", filename: "testing6.json", created_by: 1 },
  // ]);

  // await knex("workflow_instances").insert([
  //   {
  //     workflow_id: 1,
  //     name: "testing1",
  //     initiator_id: 1,
  //     filename: "testinginstance1.json",
  //   }
  // {
  //     workflow_id: 2,
  //     name: "testing2",
  //     initiator_id: 1,
  //     filename: "testinginstance2.json",
  //     current_task_id: 1,
  //     current_task: "testing2-1",
  //     current_status: "Pending",
  //     current_assignee: "9",
  //   },
  //   {
  //     workflow_id: 3,
  //     name: "testing3",
  //     initiator_id: 1,
  //     filename: "testinginstance3.json",
  //     current_task_id: 1,
  //     current_task: "testing3-1",
  //     current_status: "Pending",
  //     current_assignee: "8",
  //   }
  // ]);

  // await knex("pic").insert([
  //   { user_id: 1, workflow_instance_id: 1 },
  //   { user_id: 1, workflow_instance_id: 2 },
  //   { user_id: 1, workflow_instance_id: 3 },
  //   { user_id: 2, workflow_instance_id: 1 },
  //   { user_id: 2, workflow_instance_id: 2 },
  //   { user_id: 3, workflow_instance_id: 3 },
  //   { user_id: 5, workflow_instance_id: 1 },
  //   { user_id: 6, workflow_instance_id: 2 },
  //   { user_id: 7, workflow_instance_id: 3 },
  //   { user_id: 10, workflow_instance_id: 1 },
  //   { user_id: 9, workflow_instance_id: 2 },
  //   { user_id: 8, workflow_instance_id: 3 },
  // ]);
}
