import { Knex } from "knex";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTableIfNotExists("users", (t) => {
    t.increments("id");
    t.string("username", 255).unique().notNullable();
    t.string("title", 10).notNullable();
    t.string("name", 255).notNullable();
    // t.string("email", 320).notNullable();
    t.string("email", 320).unique().notNullable();
    t.string("hashed_password", 255).notNullable();
    t.boolean("super_admin").notNullable().defaultTo(false);
  });

  await knex.schema.createTableIfNotExists("workflows", (t) => {
    t.increments("id");
    t.string("name", 255).notNullable();
    t.string("filename", 255).unique().notNullable();
    t.integer("created_by").references("users.id");
    t.timestamp("created_at").defaultTo(knex.fn.now(0));
    t.integer("updated_by").references("users.id");
    t.timestamp("updated_at");
  });

  await knex.schema.createTableIfNotExists("actions", (t) => {
    t.increments("id");
    t.string("name", 255);
  });
}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTableIfExists("actions");
  await knex.schema.dropTableIfExists("workflows");
  await knex.schema.dropTableIfExists("users");
}
