import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { updateInstanceDto } from 'src/models/workflow/dto/workflow.dto';
import { WorkflowService } from 'src/models/workflow/workflow.service';

@Controller('instance')
export class InstanceController {
  constructor(private workflowService: WorkflowService) {}

  @Get(':userID')
  async getAllRunningInstancesByUserID(@Param('userID') userID: string) {
    return await this.workflowService.getAllRunningInstancesByUserID(+userID);
  }

  @Get('json/:instanceID')
  async getInstanceDetailJSONDataByID(@Param('instanceID') instanceID: string) {
    return await this.workflowService.getInstanceDetailJSONDataByID(
      +instanceID,
    );
  }
  @Post('update')
  async updateInstance(@Body() updateInstance: updateInstanceDto) {
    return await this.workflowService.updateInstance(updateInstance);
  }
  @Get('history/:instanceID')
  async getHistoriesByInstanceID(@Param('instanceID') instanceID: string) {
    return await this.workflowService.getHistoriesByInstanceID(+instanceID);
  }
}
