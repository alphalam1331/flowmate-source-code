import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';
import env from 'env';

@Injectable()
export class HelperService {
  constructor(private readonly mailerService: MailerService) {}

  mail(receivers: Array<string>, subject: string, content: string): void {
    this.mailerService
      .sendMail({
        to: receivers, // list of receivers
        from: env.mail.user, // sender address
        subject, // Subject lineW
        text: content, // plaintext body
        // html: '<b>welcome</b>', // HTML body content
      })
      .then((success) => {
        console.log('Mail sent successfully: ', success);
      })
      .catch((err) => {
        console.log('Failed to send mail: ', err);
      });
  }
}
