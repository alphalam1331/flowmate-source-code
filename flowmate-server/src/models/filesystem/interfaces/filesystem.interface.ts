export type FileClasses = 'workflow' | 'instance';

export type InstanceJSONData = Array<{
  id: string;
  title: string;
  description: string;
  deadline: string;
  status: string;
  action:{
    email:{
      to:Array<string>,
      subject:string,
      content:string
    }
  }
  assign_to: Array<number>;
  dependencies: Array<any>;
  nextStage: Array<{
    targetId: string;
    triggerEvent: { review: 'approve' | 'reject' } | { upload: boolean };
  }>;
}>;
