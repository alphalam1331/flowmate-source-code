export class RegFormDto {
  readonly username: string;
  readonly title: string;
  readonly name: string;
  readonly password: string;
  readonly email: string;
  readonly superAdmin: boolean;
};

export class LoginFormDto {
  readonly username: string;
  readonly password: string;
}

export class UserInfoDto{
  readonly id:string;
  readonly username:string;
  readonly email:string;
  readonly title:string;
  readonly name:string;
}

export class EditUserFormDto {
  readonly username: string;
  readonly title: string;
  readonly name: string;
  readonly password: string;
  readonly email: string;
};

export class JWTDto {
  token:string
}