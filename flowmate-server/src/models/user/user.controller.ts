import { Body, Query, Get, Post, Controller, Req } from '@nestjs/common';
import { Request } from 'express';
import { getJWTPayload } from 'src/jwt';
import { EditUserFormDto, JWTDto, LoginFormDto, RegFormDto } from './dto/user.dto';
import { User } from './interfaces/user.interfaces';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('register')
  async register(@Body() regForm: RegFormDto):Promise<JWTDto> {
    const jwt = await this.userService.createUser(regForm);
    return { token: jwt };
  }

  @Post('login')
  async logIn(@Body() loginForm: LoginFormDto):Promise<JWTDto> {
    const jwt = await this.userService.logIn(loginForm);
    return { token: jwt };
  }

  @Get()
  async getUsersInfo(@Req() req: Request, @Query('all') all: string):Promise<User[]> {
    let id: number;
    if (all) id = 0;
    else {
      const payload = getJWTPayload(req);
      id = payload.id;
    }
    return await this.userService.getUsersInfo(id);
  }

  @Post('edit')
  async editUserInfo(@Req() req: Request, @Body() editForm: EditUserFormDto) {
    let id: number;

    const payload = getJWTPayload(req);
    id = payload.id;
    // console.log(editForm)
    const newUserInfo = await this.userService.editUserInfo(editForm, id);
    return newUserInfo;
  }
}
