import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { Knex } from 'nestjs-knex/dist/knex.interfaces';
import { comparePassword, hashPassword } from 'src/hash';
import { createJWT } from 'src/jwt';
import { EditUserFormDto, LoginFormDto, RegFormDto } from './dto/user.dto';
import { User } from './interfaces/user.interfaces';
import { InjectConnection } from 'nestjs-knex';

@Injectable()
export class UserService {
  constructor(@InjectConnection() private readonly knex: Knex) {}

  table = () => {
    return this.knex('users');
  };

  createUser = async (regForm: RegFormDto) => {
    const { username, title, name, password, email, superAdmin } = regForm;
    let row = await this.table().select().where({ username });
    if (row.length > 0)
      throw new HttpException(
        'Failed to insert new user: Username already exists.',
        HttpStatus.BAD_REQUEST,
      );

    row = await this.table().select().where({ email });

    if (row.length > 0)
      throw new HttpException(
        'Failed to insert new user: Email already exists.',
        HttpStatus.BAD_REQUEST,
      );

    row = await this.table()
      .insert({
        username,
        title,
        name,
        hashed_password: await hashPassword(password),
        email,
        super_admin: superAdmin,
      })
      .returning('id');
    const id = row[0].id;

    const JWToken = createJWT({ id, username, superAdmin });
    console.log('user service: JWT: ', JWToken);
    return JWToken;
  };

  logIn = async (loginForm: LoginFormDto) => {
    const { username, password } = loginForm;
    const row = (await this.table().select().where({ username: username }))[0];
    if (!row)
      throw new HttpException(
        `${username} does not exist.`,
        HttpStatus.BAD_REQUEST,
      );

    const isMatch = await comparePassword({
      password: password,
      hashed_password: row.hashed_password,
    });

    if (!isMatch)
      throw new HttpException(`Wrong password.`, HttpStatus.BAD_REQUEST);

    const jwt = createJWT({
      id: row.id,
      username,
      superAdmin: row.super_admin,
    });
    return jwt;
  };

  getUsersInfo = async (userID: number | null):Promise<User[]> => {
    let users: Array<User>;

    if (userID === 0) {
      users = await this.table().select(
        'id',
        'username',
        'title',
        'name',
        'email',
      );
    } else {
      users = await this.table()
        .select('id', 'username', 'title', 'name', 'email', 'super_admin')
        .where({ id: userID });
    }

    return users;
  };
  editUserInfo = async (editForm: EditUserFormDto, userID: number | null) => {
    console.log(editForm);

    if (editForm.password) {
      let updatePassword = await hashPassword(editForm.password);
      const editUserInfo = await this.knex.raw(
        /* sql */ `
    UPDATE users SET username =?, title=?, name=?,email=?,hashed_password=? WHERE id=?`,
        [
          editForm.username,
          editForm.title,
          editForm.name,
          editForm.email,
          updatePassword,
          userID,
        ],
      );
    } else {
      const editUserInfo = await this.knex.raw(
        /* sql */ `
    UPDATE users SET username =?, title=?, name=?,email=? WHERE id=?`,
        [
          editForm.username,
          editForm.title,
          editForm.name,
          editForm.email,
          userID,
        ],
      );
    }

    const newUserInfo = await this.getUsersInfo(userID);
    console.log('newinfo++++++++++++++++++++++++++', newUserInfo);
    return newUserInfo;
  };

  checkSuperAdminByUserID = async (userID: number) => {
    const row = (
      await this.table().select('super_admin').where({ id: userID })
    )[0];
    const superAdminRight = row['super_admin'];
    return superAdminRight;
  };
}
