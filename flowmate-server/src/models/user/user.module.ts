import { Module } from '@nestjs/common';
import { KnexModule } from 'nestjs-knex';
import { UserController } from './user.controller';
import { UserService } from './user.service';
import knexConfig from '../../../knexfile';

@Module({
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
