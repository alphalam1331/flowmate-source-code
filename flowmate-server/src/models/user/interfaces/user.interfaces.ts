export interface User {
  id?: number;
  username: string;
  password?: string;
  title: string;
  name: string;
  email: string;
  superAdmin: boolean;
}
