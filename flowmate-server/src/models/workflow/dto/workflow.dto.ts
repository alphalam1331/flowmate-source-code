export class NewWorkflowDto {
  userID: number;
  name: string;
  jsonData: string;
}

export class WorkflowDto {
  workflowID: number;
  userID: number;
  name: string;
  jsonData: string;
}

export class updateInstanceDto {
  userID: number;
  workflowInstanceID: number;
  historyID: number;
  msg: 'approve' | 'reject' | 'upload';
}
