import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { writeFileSync } from 'fs';
import { Knex } from 'knex';
import { InjectConnection } from 'nestjs-knex';
import { FileSystemService } from 'src/models/filesystem/filesystem.service';
import {
  FileClasses,
  InstanceJSONData,
} from 'src/models/filesystem/interfaces/filesystem.interface';
import { NewWorkflow, InstanceTask } from 'src/model';
import { UserService } from 'src/models/user/user.service';
import { HelperService } from '../helper/helper.service';

@Injectable()
export class WorkflowService {
  constructor(
    @InjectConnection() private readonly knex: Knex,
    private fileSystemService: FileSystemService,
    private userService: UserService,
    private helperService: HelperService,
  ) {}

  workflowsTable = () => this.knex('workflows');
  instancesTable = () => this.knex('workflow_instances');
  historiesTable = () => this.knex('workflow_histories');
  PICTable = () => this.knex('pic');

  getAllWorkflows = async (userID: number) => {
    await this.ensureSuperAdmin(userID, 'Get workflows');

    const creator =
      '(select username from users where users.id = workflows.created_by) as creator';
    const runningInstances =
      '(select count(*)from workflow_instances where workflow_instances.workflow_id = workflows.id) as running_instances';
    let workflows = await this.workflowsTable()
      .select(
        'workflows.id',
        'workflows.name',
        'workflows.filename',
        this.knex.raw(creator),
        'workflows.created_at',
        'workflows.updated_at',
        this.knex.raw(runningInstances),
      )
      .leftJoin(
        'workflow_instances',
        'workflow_instances.workflow_id',
        'workflows.id',
      )
      .groupBy('workflows.id')
      .orderBy('workflows.created_at', 'desc')
      .orderBy('workflows.id', 'asc');
    return workflows;
  };

  getWorkFlowJSONDataByID = async (workflowID: number): Promise<string> => {
    const jsonData = await this.getJSONData(workflowID, 'workflow');
    return jsonData;
  };

  getInstanceDetailJSONDataByID = async (
    InstanceID: number,
  ): Promise<string> => {
    const jsonData = await this.getJSONData(InstanceID, 'instance');
    return jsonData;
  };

  private getJSONData = async (
    id: number,
    fileClass: FileClasses,
  ): Promise<string> => {
    let table: () => Knex.QueryBuilder<any>;
    let readFunction: (filename: string) => any;

    switch (fileClass) {
      case 'workflow': {
        table = this.workflowsTable;
        readFunction = this.fileSystemService.readWorkFlow;
        break;
      }
      case 'instance': {
        table = this.instancesTable;
        readFunction = this.fileSystemService.readInstanceDetail;
        break;
      }
    }

    const rows = await table().select('filename').where({ id });
    const filename = rows[0].filename;
    const jsonData = readFunction(filename);
    return jsonData;
  };

  getAllRunningInstancesByUserID = async (userID: number) => {
    const wi = 'workflow_instances';

    const initiator = `(select name from users where users.id = ${wi}.initiator_id) as initiator`;

    let workflowInstances = await this.instancesTable()
      .select(
        `${wi}.id`,
        `${wi}.name`,
        this.knex.raw(initiator),
        `${wi}.created_at`,
        `${wi}.updated_at`,
        `${wi}.is_completed`,
      )
      .innerJoin('pic', `${wi}.id`, `pic.workflow_instance_id`)
      .innerJoin('users', 'users.id', 'pic.user_id')
      .where({ 'users.id': userID })
      .orderBy(`${wi}.is_completed`, 'asc')
      .orderBy(`${wi}.updated_at`, 'desc')
      .orderBy(`${wi}.created_at`, 'desc');

    // console.log(workflowInstances);

    return workflowInstances;
  };

  getHistoriesByInstanceID = async (instanceID: number) => {
    const action =
      '(select name from actions where id = workflow_histories.action_id)as action';
    const histories = await this.historiesTable()
      .select('*', this.knex.raw(action))
      .where({ workflow_instance_id: instanceID })
      // .innerJoin('actions', 'actions.id', 'workflow_histories.action_id')
      .orderBy('completed_at', 'desc')
      .orderBy('start_at', 'desc');
    return histories;
  };

  createWorkflow = async (newWorkflow: NewWorkflow): Promise<number> => {
    const { name, userID, jsonData } = newWorkflow;

    await this.ensureSuperAdmin(userID, 'Create Workflow');

    const row = await this.workflowsTable().select('name').where({ name });
    if (row.length > 0)
      throw new HttpException(
        `Failed to create new workflow: Workflow name:${name} already exists.`,
        HttpStatus.BAD_REQUEST,
      );

    const filename = this.fileSystemService.saveWorkFlow(jsonData);
    const result = await this.workflowsTable()
      .insert({
        name,
        filename,
        created_by: userID,
      })
      .returning('id');
    // console.log({ result }
    const workflowID = +result[0].id;

    return workflowID;
  };

  editWorkflow = async (workflow: {
    userID: number;
    workflowID: number;
    jsonData: string;
  }): Promise<string> => {
    const { userID, workflowID, jsonData } = workflow;
    //check permission
    await this.ensureSuperAdmin(userID, 'Editing Workflow');
    const filepath = await this.getWorkflowFilePath(+workflowID);

    try {
      writeFileSync(filepath, jsonData);
    } catch (error) {
      throw new HttpException(
        `Failed to update workflows: ${error}`,
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }

    const rows = await this.workflowsTable()
      .where({ id: workflowID })
      .update({ updated_by: userID, updated_at: this.knex.fn.now(0) })
      .returning('name');
    const { name } = rows[0];
    return name;
  };

  activateWorkflow = async (workflow: {
    userID: number;
    workflowID: number;
  }): Promise<number> => {
    const { userID, workflowID } = workflow;

    await this.ensureSuperAdmin(userID, 'Activating Workflow');

    let result = (
      await this.workflowsTable().select('name').where({ id: workflowID })
    )[0];

    if (!result)
      throw new HttpException('Workflow not found.', HttpStatus.NOT_FOUND);

    const { name } = result;

    result = (
      await this.instancesTable().count().where({ workflow_id: workflowID })
    )[0];

    const totalInstance = +result.count;

    const fileContent = await this.getWorkFlowJSONDataByID(workflowID);

    const newSavedInstanceFilename = this.fileSystemService.saveInstance(
      JSON.stringify(fileContent),
    );

    //read workflow detail
    const data = this.fileSystemService.readInstanceDetail(
      newSavedInstanceFilename,
    );

    //list all involved user
    let picList: Array<number> = [userID];

    data.forEach((task) => {
      task.assign_to.forEach((userID: string | number) =>
        picList.push(+userID),
      );
    });
    // console.log('before sanitised: ', picList);

    const picSet = new Set(picList);
    picList = Array.from(picSet);

    const initialStep = data[0];
    //create instance
    const rows = await this.instancesTable()
      .insert({
        workflow_id: workflowID,
        name: `${name}_${totalInstance + 1}`,
        initiator_id: userID,
        filename: newSavedInstanceFilename,
      })
      .returning('id');

    const workflowInstanceID = +rows[0].id;

    //grant access to all user involved
    for (let picID of picList) {
      await this.PICTable().insert({
        user_id: picID,
        workflow_instance_id: workflowInstanceID,
      });
    }

    //insert initial record with data read by fs
    await this.recordHistory({
      workflowInstanceID,
      taskID: initialStep.id,
      task: initialStep.title,
      status: initialStep.status,
      description: initialStep.description,
    });

    if (initialStep.action.email.to.length > 0) {
      const { to, subject, content } = initialStep.action.email;
      this.helperService.mail(to, subject, content);
    }

    return workflowInstanceID;
  };

  updateInstance = async (updateData: {
    userID: number;
    workflowInstanceID: number;
    historyID: number;
    msg: 'approve' | 'reject' | 'upload';
  }) => {
    const { userID, workflowInstanceID, historyID, msg } = updateData;

    //read the instance JSON file
    const filename = (
      await this.instancesTable()
        .select('filename')
        .where({ id: workflowInstanceID })
    )[0].filename;
    const taskListJSON = this.fileSystemService.readInstanceDetail(filename);

    //get the updating task info
    const row = (
      await this.historiesTable()
        .select(
          'task_id as currentTaskID',
          'completed_at as currentTaskCompletedAt',
        )
        .where({ id: historyID })
    )[0];

    const { currentTaskID, currentTaskCompletedAt } = row;

    //make sure the task have not been completed yet
    if (currentTaskCompletedAt) {
      throw new HttpException(
        `This Task has been already completed at: ${currentTaskCompletedAt}`,
        HttpStatus.BAD_REQUEST,
      );
    }

    //get all allowed actions of the task
    const requiredActions = this.getNextStagesRequiredActions(
      taskListJSON,
      currentTaskID,
    );
    //update the history record
    const actionID = (
      await this.knex('actions').select('id').where({ name: msg })
    )[0].id;

    await this.recordHistory({ id: historyID, actionID });

    //update instance
    await this.instancesTable()
      .where({ id: workflowInstanceID })
      .update({ created_at: this.knex.fn.now() });

    //get which is(are) next
    const nextStageID = requiredActions[msg];

    // console.log('----------------------------------------------');
    // console.log({ requiredActions });
    // console.log({ nextStageID });
    let allowProceedTasksID: Array<string> = [];

    for (let id of nextStageID) {
      //check dependencies of next stage
      const finish = await this.checkDependencies(
        workflowInstanceID,
        taskListJSON,
        id,
      );
      // console.log({ finish });
      if (finish) {
        allowProceedTasksID.push(id);
      }
    }
    //create record of next stage(s) if all dependencies are completed
    for (let nextID of allowProceedTasksID) {
      for (let task of taskListJSON) {
        if (task.id === nextID) {
          await this.recordHistory({
            workflowInstanceID,
            taskID: task.id,
            task: task.title,
            status: task.status,
            description: task.description,
          });
          //check if email notification is required at the next stages
          if (task.action.email.to.length > 0) {
            const { to, subject, content } = task.action.email;
            this.helperService.mail(to, subject, content);
          }
        }
      }
    }
    
    //check if next stage is the final stage
    try {
      for (let nextID of allowProceedTasksID) {
        this.getNextStagesRequiredActions(taskListJSON, nextID);
      }
    } catch (error) {
      const err = error.toString();
      if (err.includes('this is the last stage of the workflow.')) {
        await this.instancesTable()
          .where({ id: workflowInstanceID })
          .update({ created_at: this.knex.fn.now(), is_completed: true });
      }
    }
    return true;
  };

  recordHistory = async (instanceTask: InstanceTask) => {
    const {
      id,
      workflowInstanceID,
      taskID,
      task,
      status,
      description,
      actionID,
    } = instanceTask;

    if (!id) {
      await this.historiesTable().insert({
        workflow_instance_id: workflowInstanceID,
        task_id: taskID,
        task,
        status,
        description,
      });
    } else {
      await this.historiesTable()
        .where({ id })
        .update({ action_id: actionID, completed_at: this.knex.fn.now(0) });
    }
  };

  ensureSuperAdmin = async (
    userID: number,
    action: string,
  ): Promise<HttpException | true> => {
    const superAdminRight = await this.userService.checkSuperAdminByUserID(
      userID,
    );
    if (!superAdminRight)
      throw new HttpException(
        `Unauthorized Action: ${action} can only be performed by super admin and authorised user.`,
        HttpStatus.UNAUTHORIZED,
      );

    return true;
  };

  getWorkflowFilePath = async (workflowID: number) => {
    const rows = await this.workflowsTable()
      .select('filename')
      .where({ id: workflowID });
    const filename = rows[0].filename;
    const path = this.fileSystemService.getFullPath('workflow', filename);
    return path;
  };

  getNextStagesRequiredActions = (
    taskList: InstanceJSONData,
    current_task_id: string,
  ) => {
    const currentTask = taskList.filter((task) => task.id === current_task_id);
    // console.log('length of nextStage: ', currentTask[0].nextStage.length);

    if (currentTask[0].nextStage.length <= 0)
      throw new Error('this is the last stage of the workflow.');

    let requiredActions: {
      approve: Array<string>;
      reject: Array<string>;
      upload: Array<string>;
    } = { approve: [], reject: [], upload: [] };

    currentTask[0].nextStage.forEach((stage) => {
      let triggerEvent = Object.entries(stage.triggerEvent);
      // console.log({ triggerEvent });

      const [event, action] = triggerEvent[0];
      switch (event) {
        case 'review': {
          if (!requiredActions[action]) requiredActions[action] = [];
          requiredActions[action].push(stage.targetId);
          return;
        }
        case 'upload': {
          requiredActions[event].push(stage.targetId);
          break;
        }
        default:
          return;
      }
    });
    return requiredActions;
  };

  checkDependencies = async (
    workflowInstanceID: number,
    taskList: InstanceJSONData,
    taskId: string,
  ) => {
    const { dependencies } = taskList.filter((task) => task.id === taskId)[0];
    // console.log({ dependencies });

    if (dependencies.length > 0) {
      const dependenciesID = dependencies.map(
        (dependency) => dependency.sourceId,
      );
      // console.log({ dependenciesID });

      const rows = await this.historiesTable()
        .select()
        .where({
          workflow_instance_id: workflowInstanceID,
        })
        .whereIn('task_id', dependenciesID)
        .whereNull('completed_at');
      console.log('dependencies have not been done: ', rows);
      if (rows.length > 0) return false;
      else return true;
    }
    return true;
  };

  checkTaskCompletion = async (instanceID: number, taskID: string) => {
    const completionTime = (
      await this.historiesTable()
        .select('completed_at')
        .where({ workflow_instance_id: instanceID, task_id: taskID })
    )[0].completed_at;
    return completionTime;
  };
}
