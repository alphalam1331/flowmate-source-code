import { Body, Controller, Get, Param, Post, UseGuards } from '@nestjs/common';
import env from 'env';
import { SuperAdminGuard } from 'src/superAdmin.guard';
import { NewWorkflowDto, WorkflowDto } from './dto/workflow.dto';
import { WorkflowService } from './workflow.service';

@Controller('workflow')
@UseGuards(SuperAdminGuard)
export class WorkflowController {
  constructor(
    private workflowService: WorkflowService,
  ) {}

  @Post('create')
  async createWorkflows(@Body() newWorkflow: NewWorkflowDto) {
    return await this.workflowService.createWorkflow(newWorkflow);
  }

  @Post('edit')
  async editWorkflows(@Body() workflow: WorkflowDto) {
    const name = await this.workflowService.editWorkflow(workflow);
    console.log({ name })
    return { name };
  }

  @Post('activate')
  async activateWorkflow(
    @Body() workflow: Omit<WorkflowDto, 'name' | 'jsonData'>,
  ) {
    return await this.workflowService.activateWorkflow(workflow);
  }

  @Get('json/:workflowID')
  async getWorkFlowJSONDataByID(@Param('workflowID') workflowID: string) {
    return await this.workflowService.getWorkFlowJSONDataByID(+workflowID);
  }

  @Get('all/:userID')
  async getAllWorkflows(@Param('userID') userID: string) {
    return await this.workflowService.getAllWorkflows(+userID);
  }
}
