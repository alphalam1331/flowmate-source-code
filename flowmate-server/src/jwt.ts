import { encode, decode } from 'jwt-simple';
import { DAY, JWTPayload } from './model';
import { Bearer } from 'permit';
import { Request } from 'express';
import { jwtConfig } from 'jwt.config';
import { HttpException, HttpStatus } from '@nestjs/common';

export const createJWT = (user: Omit<JWTPayload, 'exp_at'>) => {
  const payload: JWTPayload = {
    id: user.id,
    username: user.username,
    superAdmin: user.superAdmin,
    exp_at: Date.now() + 90 * DAY,
  };
  const token = encode(payload, jwtConfig.jwtSecret);
  return token;
};

export const getJWTPayload = (req: Request): JWTPayload => {
  let token: string;
  try {
    token = permit.check(req);
  } catch (error) {
    throw new HttpException('Invalid Bearer Token', HttpStatus.BAD_REQUEST);
  }
  if (!token)
    throw new HttpException('No Token Provided', HttpStatus.FORBIDDEN);
  
  console.log('Getting JWT Payload. \n Token: ', token);
  const payload = decodeJWT(token);
  return payload;
};

const permit = new Bearer({
  query: 'access_token', 
});

export const decodeJWT = (token: string): JWTPayload => {
  let payload: JWTPayload;

  try {
    payload = decode(token, jwtConfig.jwtSecret);
  } catch (error) {
    throw new HttpException('Invalid Token', HttpStatus.FORBIDDEN);
  }
  if (payload.exp_at <= Date.now()) {
    throw new HttpException(
      `Token was expired at ${payload.exp_at}`,
      HttpStatus.FORBIDDEN,
    );
  }
  return payload;
};
