import { Module } from '@nestjs/common';
import { KnexModule } from 'nestjs-knex';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UserController } from './models/user/user.controller';
import { UserService } from './models/user/user.service';
import { WorkflowController } from './models/workflow/workflow.controller';
import knexConfig from '../knexfile';
import { WorkflowService } from './models/workflow/workflow.service';
import { FileSystemService } from './models/filesystem/filesystem.service';
import { InstanceController } from './models/instance/instance.controller';
import env from '../env';
import { MailerModule } from '@nestjs-modules/mailer/dist/mailer.module';
import { HelperService } from './models/helper/helper.service';

@Module({
  imports: [
    KnexModule.forRoot({
      config: knexConfig[env.nodeEnv],
    }),
    MailerModule.forRoot({
      transport: {
        host: 'outlook.com',
        port: 587,
        secure:false, // upgrade later with STARTTLS
        auth: {
          user: env.mail.user,
          pass: env.mail.pass,
        },
      },
      preview: true,
    }),
  ],
  controllers: [
    AppController,
    UserController,
    WorkflowController,
    InstanceController,
  ],
  providers: [
    AppService,
    UserService,
    WorkflowService,
    FileSystemService,
    HelperService,
  ],
})
export class AppModule {}

// transport: {
//   host: 'localhost',
//   port: env.port,
//   secure: false,
//   auth: {
//     user: env.mail.user,
//     pass: env.mail.pass,
//   },
// }),
